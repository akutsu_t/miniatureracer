#include "Character.h"
#include "Engine/Model.h"
#include "CarPhysic.h"
#include "Engine/BoxCollider.h"
#include "Wheel.h"
#include "CharacterAI.h"
#include "Engine/Audio.h"

//コンストラクタ
Character::Character(IGameObject * parent, std::string name) : IGameObject(parent, name), checkPoint_(0),
lap_(1), rank_(1), state_(CHARACTER_STATE_START), pPhysic_(nullptr), pAI_(nullptr)
{
}

//デストラクタ
Character::~Character()
{
}

//初期化
void Character::Initialize()
{
	//各クラスの生成
	pPhysic_ = new CarPhysic();					//物理演算
	pAI_ = new CharacterAI(this, pPhysic_);		//AI

	InitializeAddData();
}

//更新
void Character::Update()
{
	//現在の状態に応じて変化
	switch (state_)
	{
		//スタート合図前
		case CHARACTER_STATE_START:
			//何もしない
			break;

		//レース中とゴール後
		case CHARACTER_STATE_RACE:
		case CHARACTER_STATE_GOAL:
			//移動
			Move();

			//物理演算使用時
			if (pPhysic_ != nullptr)
			{
				//物理演算クラスの更新
				pPhysic_->Update(1 / g.fps);

				//演算後の位置・角度を取得
				position_ = pPhysic_->GetPosition();
				rotate_ = pPhysic_->GetRotate();

				//前方タイヤの角度を更新
				D3DXVECTOR3 rotate = pPhysic_->GetWheelRotate();
				pWheels_[WHEEL_FL]->SetRotate(rotate);
				pWheels_[WHEEL_FR]->SetRotate(rotate);
			}
			break;

		default:
			break;
	}
}

//描画
void Character::Draw()
{
}

//解放
void Character::Release()
{
	//継承先で追加された解放処理
	ReleaseAddData();
	
	SAFE_DELETE(pPhysic_);
	SAFE_DELETE(pAI_);
}


void Character::AddForce(D3DXVECTOR3 force)
{
	if (pPhysic_ == nullptr)
	{
		return;
	}

	pPhysic_->AddForce(force);
}


D3DXVECTOR3 Character::GetForce()
{
	if (pPhysic_ == nullptr)
	{
		return D3DXVECTOR3(0, 0, 0);
	}

	//力を取得
	D3DXVECTOR3 force = pPhysic_->GetForce();
	//ワールド座標に変換
	D3DXMATRIX matX, matY, matZ, mat;
	D3DXMatrixRotationX(&matX, D3DXToRadian(rotate_.x));
	D3DXMatrixRotationX(&matY, D3DXToRadian(rotate_.y));
	D3DXMatrixRotationX(&matZ, D3DXToRadian(rotate_.z));
	mat = matX * matY * matZ;
	D3DXVec3TransformCoord(&force, &force, &mat);
	return force;
}


void Character::OnCollision(IGameObject * pTarget, float length)
{
	//Boxに当たったとき
	if (pTarget->GetName() == "Box")
	{
		if (pPhysic_ != nullptr)
		{
			//速度からエンジンの回転数を再計算
			pPhysic_->CalcRpm();
		}

		if (pAI_ != nullptr)
		{
			//次のチェックポイントまでのベクトルを再計算
			pAI_->CalcNextPoint();

		}
	}

	//PlayerかNPCに当たったとき
	if (pTarget->GetName() == "Player" || pTarget->GetName() == "NPC")
	{
		//反射処理
		ReflectVsCharacter((Character*)pTarget);
	}
}

//周回数を1増やす
void Character::IncreaseLap()
{
		lap_++;
}

void Character::ReflectVsCharacter(Character * pCharacter)
{
	//相手の中心方向へのベクトルを求める
	D3DXVECTOR3 direction = pCharacter->GetPosition() - position_;
	//正規化
	D3DXVec3Normalize(&direction, &direction);
	//自分の現在の力を取得
	D3DXVECTOR3 force = pPhysic_->GetForce();
	//ワールド座標に変換
	D3DXMATRIX mat, matX, matY, matZ;
	D3DXMatrixRotationX(&matX, D3DXToRadian(rotate_.x));
	D3DXMatrixRotationY(&matY, D3DXToRadian(rotate_.y));
	D3DXMatrixRotationZ(&matZ, D3DXToRadian(rotate_.z));
	mat = matX * matY * matZ;
	D3DXVec3TransformCoord(&force, &force, &mat);
	//相手方向の力を算出
	float dot = D3DXVec3Dot(&direction, &force);
	direction *= fabs(dot);
	//めり込み距離を算出し、半分の距離相手を押し出す
	D3DXVECTOR3 pushBackVec = CalcPushBackVec(pCharacter);
	pCharacter->SetPosition(pCharacter->GetPosition() + pushBackVec * 0.5f);
	SetPosition(position_);
	//自分の力を相手に与える
	pCharacter->AddForce(direction);
	//与えた分自分の力から引く
	AddForce(-direction);
	
}

//レース開始
void Character::StartGame()
{
	state_ = CHARACTER_STATE_RACE;
}

//レース終了
void Character::EndGame()
{
	state_ = CHARACTER_STATE_GOAL;
}

//ステージ情報を設定
void Character::SetStage(Stage * pStage)
{
	pAI_->SetStage(pStage);
}

//次のチェックポイントまでのベクトルを計算する
void Character::CalcNextPoint()
{
	pAI_->CalcNextPoint();
}

void Character::SetVelocity(D3DXVECTOR3 vel)
{
	pPhysic_->SetVelocity(vel);
}

D3DXVECTOR3 & Character::GetVelocity()
{
	return pPhysic_->GetVelocity();
}

//アクセス関数
void Character::SetLap(int lap)
{
	lap_ = lap;
}

void Character::SetPosition(D3DXVECTOR3& position)
{
	position_ = position;

	//物理演算クラスを使用
	if (pPhysic_ != nullptr)
	{
		pPhysic_->SetPosition(position);
	}
}

void Character::AddPosition(D3DXVECTOR3 & addPosition)
{
	position_ += addPosition;

	//物理演算クラスを使用
	if (pPhysic_ != nullptr)
	{
		pPhysic_->AddPosition(addPosition);
	}
}
