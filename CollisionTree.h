#pragma once
#include <Windows.h>
#include <set>
#include <list>
#include <vector>
#include <list>
#include <map>
#include "Engine/IGameObject.h"

//前方宣言
template<class T>
class CollisionCell;			//衝突空間クラス

//線形4分木
namespace CollisionTree
{
	////////////////登録オブジェクトクラス//////////////
	template<class T>
	class ObjectForTree
	{
	private:
		CollisionCell<IGameObject>* pCell_;		//登録空間
		T* pObject_;							//判定対象のオブジェクト
		int id_;								//識別ID

	public:
		ObjectForTree* pPre_;			//前の登録オブジェクト
		ObjectForTree* pNext_;			//次の登録オブジェクト

		//コンストラクタ
		//引数：id	識別ID（初期値：0）
		ObjectForTree(int id = 0) : pCell_(nullptr), pObject_(nullptr),
			pPre_(nullptr), pNext_(nullptr), id_(id)
		{
		}

		//デストラクタ
		virtual ~ObjectForTree()
		{
		}

	public:
		//リストから離脱
		//戻り値：離脱成功ならtrue、失敗ならfalse
		bool Remove()
		{
			//すでに離脱しているなら処理終了
			if (!pCell_)
			{
				return false;
			}

			//自分が登録している空間に離脱することを通知
			if (!pCell_->OnRemove(this))
			{
				//離脱失敗
				return false;
			}

			//離脱処理
			//前後のオブジェクトをつなげる
			//前にオブジェクトがいる
			if (pPre_ != nullptr)
			{
				pPre_->pNext_ = pNext_;
			}
			//後ろにオブジェクトがいる
			if (pNext_ != nullptr)
			{
				pNext_->pPre_ = pPre_;
			}
			//前後のオブジェクト情報を初期化
			pPre_ = nullptr;
			pNext_ = nullptr;
			pCell_ = nullptr;

			//離脱成功
			return true;
		}
	};


	/////////////衝突空間クラス///////////////////
	template<class T>
	class CollisionCell
	{
	protected:
		T* pLatest_;			//先頭オブジェクトへのポインタ

	public:
		//コンストラクタ
		CollisionCell() : pLatest_(nullptr)
		{
		}

		//デストラクタ
		virtual ~CollisionCell()
		{
			//登録オブジェクトが存在するなら
			if (pLatest_ != nullptr)
			{
				//リンクをリセット
				ResetLink(pLatest_);
				pLatest_ = nullptr;
			}
		}

		//リンクを全てリセットする
		void ResetLink(T* pObject)
		{
			//次のオブジェクトがある
			if (pObject->pNext_ != nullptr)
			{
				ResetLink(pObject->pNext_);
			}
		}

		//オブジェクトを登録
		//引数：pObject	登録したいオブジェクト
		//戻り値：登録完了ならtrue,登録失敗ならfalse
		bool Push(T* pObject)
		{
			//無効オブジェクト
			if (pObject == nullptr)
			{
				//登録しない
				return false;
			}

			//二重で登録してないかチェック
			if (pObject->pCell_ == this)
			{
				//登録しない
				return false;
			}

			//すでにオブジェクトが登録されているか
			if (pLatest_ == nullptr)
			{
				//空間に新規登録
				pLatest_ = pObject;
			}
			else
			{
				//先頭オブジェクトを更新
				pObject->pNext_ = pLatest_;
				pLatest_->pPre_ = pObject;
				pLatest_ = pObject;
			}
			//オブジェクトに空間を登録
			pObject->pCell_ = this;
			
			//登録完了
			return true;
		}

		//先頭オブジェクトを取得
		T* GetFirstObject()
		{
			return pLatest_;
		}

		//削除されるオブジェクトをチェック
		bool OnRemove(T* pRemoveObject)
		{
			//最新登録オブジェクトか
			if (pLatest_ == pRemoveObject)
			{
				//最新を次のオブジェクトに変更
				if (pLatest_ != nullptr)
				{
					pLatest_ = pLatest_->pNext_;
				}
			}

			return true;
		}
	};


	/////////////////衝突リスト////////////////////
	//定数
	const int COLLISIONLIST_REALLOC_SIZE = 100;		//領域再確保時の増加サイズ
	template<class T>
	class CollisionList {
	private:
		T** root_;				//リストのルートポインタ
		size_t pos_;			//現在の位置
		size_t mallocSize_;		//確保済みのサイズ

	public:
		//コンストラクタ
		CollisionList() : root_(nullptr), pos_(0), mallocSize_(COLLISIONLIST_REALLOC_SIZE)
		{
			//領域確保
			root_ = (T**)malloc(sizeof(T*) * COLLISIONLIST_REALLOC_SIZE);
			int i = 0;
		}

		//デストラクタ
		~CollisionList()
		{
			Reflesh();
		}

		//現在の位置を取得
		size_t GetSize()
		{
			return pos_;
		}

		//ルートポインタを取得
		T** GetRoot()
		{
			return root_;
		}

		//位置をリセット
		void ResetPos()
		{
			pos_ = 0;
		}

		//リストに書き込む
		void wright(T* object1, T* object2)
		{
			//現在位置が確保サイズ以上なら再確保
			if (pos_ >= mallocSize_)
			{
				root_ = (T**)realloc(root_, sizeof(T*) * (mallocSize_ * COLLISIONLIST_REALLOC_SIZE));
				mallocSize_ += COLLISIONLIST_REALLOC_SIZE;
			}
			//書き込み
			root_[pos_++] = object1;
			root_[pos_++] = object2;
		}

		//領域を解放
		void Reflesh()
		{
			if (root_)
			{
				free(root_);
			}
		}
	};

	/////////////線分4分木空間管理クラス//////////////////
	//定数
	const int CLINER4TREEMANAGER_MAXLEVEL = 9;			//空間の最大レベル
	template<class T>
	class CLiner4TreeManager
	{
	protected:
		CollisionCell<T>** ppCellAry_;							//線形空間のポインタ配列
		unsigned int iPow_[CLINER4TREEMANAGER_MAXLEVEL + 1];	//べき乗数値配列
		float width_;					//領域のX軸幅
		float height_;					//領域のY軸幅
		float left_;					//領域の左側（X軸最小値）
		float top_;						//領域の上側（Y軸最小値）
		float unitWidth_;				//最小レベル空間の幅単位
		float unitHeight_;				//最小レベル空間の高さ単位
		DWORD cellNum_;					//空間の数
		unsigned int uiLevel_;			//最下位レベル
		CollisionList<T>* pColList_;	//衝突リスト

	public:
		//コンストラクタ
		CLiner4TreeManager() : ppCellAry_(nullptr), width_(0.0f), height_(0.0f), left_(0.0f),
			top_(0.0f), unitWidth_(0.0f), unitHeight_(0.0f), cellNum_(0), uiLevel_(0)
		{
			//各レベルでの空間数を算出
			iPow_[0] = 1;
			for (int i = 1; i < CLINER4TREEMANAGER_MAXLEVEL + 1; i++)
			{
				iPow_[i] = iPow_[i - 1] * 4;
			}
		}

		//デストラクタ
		virtual ~CLiner4TreeManager()
		{
			AllClear();
		}


		//線形4分木配列を構築する
		//引数：level 最高レベル, left X軸最小値, top Y軸最小値, right X軸最大値, bottom Y軸最大値
		//戻り値：構築成功ならtrue、失敗ならfalse
		bool Init(unsigned int level, float left, float top, float right, float bottom)
		{
			//最高レベル以上の空間は作れない
			if (level >= CLINER4TREEMANAGER_MAXLEVEL)
			{
				//構築失敗
				return false;
			}

			//level0（0基点）の配列作成
			cellNum_ = (iPow_[level + 1] - 1 / 3);
			ppCellAry_ = new CollisionCell<T>* [cellNum_];
			//中身を0で初期化
			ZeroMemory(ppCellAry_, sizeof(CollisionCell<T>*)*cellNum_);
			
			//領域を登録
			left_ = left;
			top_ = top;
			width_ = right - left;
			height_ = bottom - top;
			unitWidth_ = width_ / (1 << level);
			unitHeight_ = width_ / (1 << level);
			uiLevel_ = level;

			//衝突リストを生成
			pColList_ = new CollisionList<T>;

			//構築成功
			return true;
		}

		//オブジェクトを登録する
		bool Regist(float left, float top, float right, float bottom, IGameObject* pObject)
		{
			//オブジェクトの境界範囲から登録モートン番号を算出
			DWORD element = GetMortonNumber(left, top, right, bottom);
			//総空間数以内の場合のみ登録
			if (element < cellNum_)
			{
				//空間が無い場合は新規作成
				if (!ppCellAry_[element])
				{
					CreateNewCell(element);
				}
				//空間にオブジェクトを登録
				return ppCellAry_[element]->Push(pObject);
			}

			//登録失敗
			return false;
		}

		//衝突判定リストを作成する
		//引数：ppColList	衝突判定リスト
		//戻り値：衝突判定リストのサイズ
		DWORD GetAllCollisionList(CollisionList<T>** ppColList)
		{
			//リストを初期化
			pColList_->ResetPos();

			//ルート空間の存在をチェック
			if (ppCellAry_[0] == nullptr)
			{
				//空間が存在していない
				return 0;
			}

			//ルート空間を処理
			std::list<T*> colStac;
			GetCollisionList(0, colStac);

			//衝突判定リストを更新
			*ppColList = pColList_;

			//衝突リストのサイズを返す
			return (DWORD)pColList_->GetSize();

		}

		//衝突リストのルートポインタを取得
		//戻り値：衝突リストのルートポインタ
		T** GetRoot()
		{
			return pColList_->GetRoot();
		}

		//全ての空間情報を削除
		void AllClear()
		{
			//線形空間のポインタ配列を開放
			for (DWORD i = 0; i < cellNum_; i++)
			{
				if (ppCellAry_[i] != nullptr)
				{
					delete ppCellAry_[i];
				}
			}
			delete ppCellAry_;
		}


	protected:
		//空間内で衝突リストを作成
		//引数：element	空間番号, colStac	衝突可能性のあるオブジェクトのスタック
		//戻り値：作成終了 true
		bool GetCollisionList(DWORD element, std::list<T*> &colStac)
		{
			auto it = colStac.begin();
			//空間内のオブジェクト同士の衝突リストを作成
			//先頭オブジェクトを取得
			T* pObject1 = ppCellAry_[element]->GetFirstObject();
			//空間内のオブジェクトをすべて順に調べる
			while (pObject1 != 0)
			{
				//次のオブジェクトを取得
				T* pObject2 = pObject1->pNext_;
				while (pObject2 != 0)
				{
					//衝突リストに追加
					pColList_->wright(pObject1, pObject2);
					//さらに次のオブジェクトに変更
					pObject2 = pObject2->pNext_;
				}

				//衝突スタックとの衝突リストを作成
				for (it = colStac.begin(); it != colStac.end(); it++)
				{
					//衝突リストに追加
					pColList_->wright(pObject1, *it);
				}
				//次のオブジェクトに変更
				pObject1 = pObject1->pNext_;
			}

			bool childFlag = false;		//子空間が存在するか
			//子空間に移動
			DWORD objectNum = 0;		//スタックに登録済みの空間内のオブジェクト数
			DWORD nextElement;
			//子空間全てを調べる
			for (DWORD i = 0; i < 4; i++)
			{
				//子空間の空間番号を算出
				nextElement = element * 4 + 1 + i;
				//存在する空間かチェック
				if (nextElement < cellNum_ && ppCellAry_[nextElement])
				{
					//最初に子空間を見つけたときのみ登録オブジェクトをスタックに追加
					if (!childFlag)
					{
						//空間内の全登録オブジェクトをスタックに追加
						pObject1 = ppCellAry_[element]->GetFirstObject();
						while (pObject1 != 0)
						{
							colStac.push_back(pObject1);
							objectNum++;
							pObject1 = pObject1->pNext_;
						}
					}
					//子空間を発見
					childFlag = true;
					//子空間からさらに調べる
					GetCollisionList(nextElement, colStac);
				}
			}

			//スタックからオブジェクトを外す
			if (childFlag)
			{
				for (DWORD i = 0; i < objectNum; i++)
				{
					colStac.pop_back();
				}
			}

			//作成完了
			return true;
		}

		//空間を生成
		bool CreateNewCell(DWORD element)
		{
			//指定要素番号に空間が存在しない場合
			while (!ppCellAry_[element])
			{
				//指定要素番号に空間を新規作成
				ppCellAry_[element] = new CollisionCell<T>;

				//親空間に移動
				element = (element - 1) >> 2;
				//総空間数以上ならループ終了
				if (element >= cellNum_)
				{
					break;
				}
			}

			//生成完了
			return true;
		}
	
		//座標から空間番号を算出
		DWORD GetMortonNumber(float left, float top, float right, float bottom)
		{
			//最小レベルにおける各軸位置を算出
			DWORD leftTop = GetPointElement(left, top);
			DWORD rightBottom = GetPointElement(right, bottom);

			//空間番号の排他的論理和から所属レベルを算出
			DWORD def = leftTop ^ rightBottom;
			unsigned int hiLevel = 0;
			//最下位レベルまで2ビットずつ調べる
			for (unsigned int i = 0; i < uiLevel_; i++)
			{
				//2ビットずつ確認
				DWORD check = (def >> (i * 2)) & 0x3;
				//0以外ならそのシフト数をひとまず採用
				if (check != 0)
				{
					hiLevel = i + 1;
				}
			}
			//所属空間内の番号を算出
			DWORD spaceNum = rightBottom >> (hiLevel * 2);
			//上位レベル空間の要素数を求める
			DWORD addNum = (iPow_[uiLevel_ - hiLevel] - 1) / 3;
			spaceNum += addNum;

			//総空間数以内か確認
			if (spaceNum > cellNum_)
			{
				//範囲外のため失敗
				return 0xffffffff;
			}

			//求めた空間番号
			return spaceNum;

		}

		//ビット分割関数
		//引数：num 分割したい数
		//戻り値：1ビット間隔で分割された数
		DWORD BitSeparate32(DWORD num)
		{
			num = (num | (num << 8)) & 0x00ff00ff;
			num = (num | (num << 4)) & 0x0f0f0f0f;
			num = (num | (num << 2)) & 0x33333333;
			num = (num | (num << 1)) & 0x55555555;
			return num;
		}

		//2Dモートン空間番号を算出
		//引数：x x座標, y y座標
		//戻り値：2Dモートン空間番号
		WORD Get2DMortonNumber(WORD x, WORD y)
		{
			return (WORD)(BitSeparate32(x) | (BitSeparate32(y) << 1));
		}

		//座標を線形4分木要素番号に変換
		DWORD GetPointElement(float posX, float posY)
		{
			//x,yそれぞれ何番目の空間か求める
			WORD x = (WORD)((posX - left_) / unitWidth_);
			WORD y = (WORD)((posY - top_) / unitHeight_);
			//求めた位置をもとに空間番号を算出
			return Get2DMortonNumber(x, y);
		}
	};
}	//end namespace CollisionTree