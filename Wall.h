#pragma once

#include "Engine/IGameObject.h"


//壁を管理するクラス
class Wall : public IGameObject
{
public:
	//コンストラクタ
	Wall(IGameObject* parent);


	//デストラクタ
	~Wall();


	//初期化
	void Initialize() override;


	//更新
	void Update() override;


	//描画
	void Draw() override;


	//解放
	void Release() override;

};