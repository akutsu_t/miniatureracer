#include "PlayScene.h"
#include "Stage.h"
#include "Player.h"
#include "Box.h"
#include "Judge.h"
#include "Engine/Camera.h"
#include "Engine/Model.h"
#include "NPC.h"
#include "BackGround.h"
#include "Engine/Audio.h"

//定数
const int CHARACTER_TOTAL = 6;										//全キャラクター数

//コンストラクタ
PlayScene::PlayScene(IGameObject * parent)
	: IGameObject(parent, "PlayScene"), pStage_(nullptr), pJudge_(nullptr),
	pPlayer_(nullptr)
{
}

//初期化
void PlayScene::Initialize()
{
	//各オブジェクトを生成
	pStage_ = CreateGameObject<Stage>(this);			//ステージ
	pJudge_ = CreateGameObject<Judge>(this);			//審判
	pJudge_->SetStage(pStage_);							//ステージ情報を審判に渡す
	CreateCharacter(CHARACTER_TOTAL);					//全キャラクターを生成

	//背景
	CreateGameObject<BackGround>(this);

	//BGM再生
	Audio::Play("raceBgm");
}

//更新
void PlayScene::Update()
{
}

//描画
void PlayScene::Draw()
{
}

//解放
void PlayScene::Release()
{
	//BGM停止
	Audio::Stop("raceBgm");
}

void PlayScene::CreateCharacter(int total)
{
	//生成するNPCの数
	int createNpcCount = total - 1;

	//NPC生成
	const std::string name = "start";
	//ステージのモデル番号を取得
	int hStageModel = pStage_->GetModelHandle();
	for (int i = 0; i < createNpcCount; i++)
	{
		NPC* pNpc = CreateGameObject<NPC>(this);
		//スタート位置を取得し、反映
		D3DXVECTOR3 pos = Model::GetBonePosition(hStageModel, name + std::to_string(i));
		pNpc->SetPosition(pos);
		pNpc->SetStage(pStage_);					//ステージ情報を渡す
		pJudge_->RegistCharacter(pNpc);
	}

	//プレイヤー生成
	pPlayer_ = CreateGameObject<Player>(this);
	//スタート位置を取得し、反映
	D3DXVECTOR3 pos = Model::GetBonePosition(hStageModel, name + std::to_string(createNpcCount));
	pPlayer_->SetPosition(pos);
	pPlayer_->SetStage(pStage_);					//ステージ情報を渡す
	pJudge_->RegistCharacter(pPlayer_);

	pJudge_->CompleteRegistration();				//登録完了通知
}

