#include "Random.h"

Random::Random()
{
}

Random::~Random()
{
}

void Random::Initialize()
{
	mt_.seed(random_());
}

int Random::GetRandom(int min, int max)
{
	//指定された範囲内の数をランダムで返す
	return mt_() % (max + 1 - min) + min;
}

void Random::Release()
{
}
