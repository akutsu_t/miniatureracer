#pragma once

#include "Engine/IGameObject.h"

//画像
enum SPEED_PICT {
	SPEED_PICT_NUMBER_0,
	SPEED_PICT_NUMBER_1,
	SPEED_PICT_NUMBER_2,
	SPEED_PICT_NUMBER_3,
	SPEED_PICT_NUMBER_4,
	SPEED_PICT_NUMBER_5,
	SPEED_PICT_NUMBER_6,
	SPEED_PICT_NUMBER_7,
	SPEED_PICT_NUMBER_8,
	SPEED_PICT_NUMBER_9,
	SPEED_PICT_WINDOW,
	SPEED_PICT_MAX
};

//Speedometerを管理するクラス
class Speedometer : public IGameObject
{
	int hPict_[SPEED_PICT_MAX];				//画像番号
	std::string pictName_[SPEED_PICT_MAX];	//画像名
	int speed_;								//速度

	int onePlaceNum_;							//一の位の表示画像番号
	int tenPlaceNum_;							//十の位の表示画像番号
	int hundredPlaceNum_;						//百の位の表示画像番号
	D3DXVECTOR3 numberPos_;						//数値表示位置
	D3DXVECTOR2 numberSize_;					//数字サイズ

	bool isDraw_;							//描画するかどうか
public:
	//コンストラクタ
	Speedometer(IGameObject* parent);

	//デストラクタ
	~Speedometer();

	//初期化
	void Initialize() override;

	//更新
	void Update() override;

	//描画
	void Draw() override;

	//解放
	void Release() override;

	//速度を描画
	void DrawSpeed();

	//アクセス関数
	void SetSpeed(int speed) { speed_ = speed; }
	void SetIsDraw(bool isDraw) { isDraw_ = isDraw; }

};