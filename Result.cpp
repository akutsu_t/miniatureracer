#include "Result.h"
#include "Engine/Image.h"
#include "Engine/Input.h"

//定数
const D3DXVECTOR3 RESULT_TIME_POS = D3DXVECTOR3(350, 330, 0);		//時間表示位置
const D3DXVECTOR3 RESULT_RANK_POS = D3DXVECTOR3(500, 520, 0);		//順位表示位置

//コンストラクタ
Result::Result(IGameObject * parent)
	:IGameObject(parent, "Result"), isDisplay_(false), minute_(0), second_(0), microSecond_(0),
	rank_(1)
{
}

//デストラクタ
Result::~Result()
{
}

//初期化
void Result::Initialize()
{
	//画像番号の初期化
	InitHPict();

	//画像データをロード
	LoadPict();

	//数字サイズを取得
	numberSize_ = Image::GetTextureSize(hTimePict_[RESULT_TIME_PICT_NUMBER_0]);
}

//更新
void Result::Update()
{
	//表示中に[Enter]キーが押された
	if (isDisplay_ && Input::IsKeyDown(DIK_RETURN))
	{
		//タイトルシーンへ
		SceneManager::ChangeScene(SCENE_ID_TITLE);
	}
}

//描画
void Result::Draw()
{
	//表示するかどうか
	if (isDisplay_)
	{
		//ウィンドウ描画
		Image::SetPosition(hResultPict_[RESULT_PICT_WINDOW], position_);
		Image::Draw(hResultPict_[RESULT_PICT_WINDOW]);

		//時間描画
		DrawTime();

		//順位描画
		Image::SetPosition(hRankPict_[rank_], RESULT_RANK_POS);
		Image::Draw(hRankPict_[rank_]);
	}
}

//解放
void Result::Release()
{
}

//リザルト画面を表示
void Result::ShowResult()
{
	isDisplay_ = true;
}

//時間を描画
void Result::DrawTime()
{
	int number;						//描画する数字
	D3DXVECTOR3 position;			//描画位置

	// 分
	// 十の位
	number = minute_ / 10;
	position = position_ + RESULT_TIME_POS;
	Image::SetPosition(hTimePict_[number], position);
	Image::Draw(hTimePict_[number]);
	// 一の位
	number = minute_ % 10;
	position.x += numberSize_.x;
	Image::SetPosition(hTimePict_[number], position);
	Image::Draw(hTimePict_[number]);

	// :
	position.x += numberSize_.x;
	Image::SetPosition(hTimePict_[RESULT_TIME_PICT_COLON], position);
	Image::Draw(hTimePict_[RESULT_TIME_PICT_COLON]);

	// 秒
	// 十の位
	number = second_ / 10;
	position.x += numberSize_.x;
	Image::SetPosition(hTimePict_[number], position);
	Image::Draw(hTimePict_[number]);
	// 一の位
	number = second_ % 10;
	position.x += numberSize_.x;
	Image::SetPosition(hTimePict_[number], position);
	Image::Draw(hTimePict_[number]);

	// :
	position.x += numberSize_.x;
	Image::SetPosition(hTimePict_[RESULT_TIME_PICT_COLON], position);
	Image::Draw(hTimePict_[RESULT_TIME_PICT_COLON]);

	// 1/100秒
	//十の位
	number = microSecond_ / 10;
	position.x += numberSize_.x;
	Image::SetPosition(hTimePict_[number], position);
	Image::Draw(hTimePict_[number]);
	//一の位
	number = microSecond_ % 10;
	position.x += numberSize_.x;
	Image::SetPosition(hTimePict_[number], position);
	Image::Draw(hTimePict_[number]);
}

//画像番号の初期化
void Result::InitHPict()
{
	//時間表示用
	for (int i = 0; i < RESULT_TIME_PICT_MAX; i++)
	{
		hTimePict_[i] = -1;
	}

	//順位表示用
	for (int i = 0; i < RESULT_RANK_PICT_MAX; i++)
	{
		hRankPict_[i] = -1;
	}

	//リザルト表示用
	for (int i = 0; i < RESULT_PICT_MAX; i++)
	{
		hResultPict_[i] = -1;
	}
}

//画像データをロード
void Result::LoadPict()
{
	//準備
	//時間
	timePictName_[RESULT_TIME_PICT_NUMBER_0] = "0";
	timePictName_[RESULT_TIME_PICT_NUMBER_1] = "1";
	timePictName_[RESULT_TIME_PICT_NUMBER_2] = "2";
	timePictName_[RESULT_TIME_PICT_NUMBER_3] = "3";
	timePictName_[RESULT_TIME_PICT_NUMBER_4] = "4";
	timePictName_[RESULT_TIME_PICT_NUMBER_5] = "5";
	timePictName_[RESULT_TIME_PICT_NUMBER_6] = "6";
	timePictName_[RESULT_TIME_PICT_NUMBER_7] = "7";
	timePictName_[RESULT_TIME_PICT_NUMBER_8] = "8";
	timePictName_[RESULT_TIME_PICT_NUMBER_9] = "9";
	timePictName_[RESULT_TIME_PICT_COLON] = "colon";
	//順位
	rankPictName_[RESULT_RANK_PICT_0] = "0";
	rankPictName_[RESULT_RANK_PICT_1] = "1st";
	rankPictName_[RESULT_RANK_PICT_2] = "2nd";
	rankPictName_[RESULT_RANK_PICT_3] = "3rd";
	rankPictName_[RESULT_RANK_PICT_4] = "4th";
	rankPictName_[RESULT_RANK_PICT_5] = "5th";
	rankPictName_[RESULT_RANK_PICT_6] = "6th";
	//リザルト
	resultPictName_[RESULT_PICT_WINDOW_NONE] = "ResultWindowNone";
	resultPictName_[RESULT_PICT_WINDOW] = "ResultWindow";

	//画像データのロード
	//時間
	for (int i = 0; i < RESULT_TIME_PICT_MAX; i++)
	{
		hTimePict_[i] = Image::Load("Data/Image/Result/Time/" + timePictName_[i] + ".png");
		assert(hTimePict_[i] >= 0);
	}
	//順位
	for (int i = 0; i < RESULT_RANK_PICT_MAX; i++)
	{
		hRankPict_[i] = Image::Load("Data/Image/Result/Rank/" + rankPictName_[i] + ".png");
		assert(hRankPict_[i] >= 0);
	}
	//リザルト
	for (int i = 0; i < RESULT_PICT_MAX; i++)
	{
		hResultPict_[i] = Image::Load("Data/Image/Result/" + resultPictName_[i] + ".png");
		assert(hResultPict_[i] >= 0);
	}
}

//時間を設定
void Result::SetTime(int minute, int second, int microSecond)
{
	minute_ = minute;
	second_ = second;
	microSecond_ = microSecond;
}
