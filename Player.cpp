#include "Player.h"
#include "Engine/Model.h"
#include "Engine/Input.h"
#include "CarPhysic.h"
#include "Engine/BoxCollider.h"
#include "UI.h"
#include "Wheel.h"
#include "CharacterAI.h"
#include "Engine/Camera.h"

//定数
const D3DXVECTOR3 COLLIDER_SIZE = D3DXVECTOR3(2.5f, 2.0f, 4.0f);	//当たり判定のサイズ
const D3DXVECTOR3 COLLIDER_CENTER = D3DXVECTOR3(0, 1.0f, 0);		//当たり判定の中心位置
const int TURN_HANDLE_SPEED = 15;									//ハンドルを回す速度
const D3DXVECTOR3 CAMERA_POSITION = D3DXVECTOR3(0, 3, -7);			//メインカメラの位置
const D3DXVECTOR3 CAMERA_TARGET = D3DXVECTOR3(0, 3, 0);				//メインカメラの焦点

//コンストラクタ
Player::Player(IGameObject * parent)
	: Character(parent, "Player"), pCamera_(nullptr), pUI_(nullptr)
{
}

//デストラクタ
Player::~Player()
{
}

void Player::InitializeAddData()
{
	//エフェクト生成
	LPD3DXBUFFER err = 0;		//エラー用
	if (FAILED(D3DXCreateEffectFromFile(Direct3D::pDevice_, "CommonShader.hlsl",
		NULL, NULL, D3DXSHADER_DEBUG, NULL, &pEffect_, &err)))
	{
		//失敗時	親ウィンドウ	表示内容			ウィンドウバー		ボタン
		MessageBox(NULL, (char*)err->GetBufferPointer(), "シェーダエラー", MB_OK);
	}
	assert(pEffect_ != nullptr);

	//モデルデータのロード
	hModel_ = Model::Load("Data/Car_Blue.fbx", pEffect_);
	assert(hModel_ >= 0);

	//箱型コライダーをつける
	SetBoxCollider(COLLIDER_CENTER, COLLIDER_SIZE, worldMatrix_);

	//UIクラスを生成
	pUI_ = CreateGameObject<UI>(this);

	//指定位置にタイヤを生成
	pWheels_[WHEEL_FL] = CreateGameObject<Wheel>(this);
	pWheels_[WHEEL_FL]->SetPosition(Model::GetBonePosition(hModel_, "wheel_FL"));
	pWheels_[WHEEL_FR] = CreateGameObject<Wheel>(this);
	pWheels_[WHEEL_FR]->SetPosition(Model::GetBonePosition(hModel_, "wheel_FR"));
	pWheels_[WHEEL_RL] = CreateGameObject<Wheel>(this);
	pWheels_[WHEEL_RL]->SetPosition(Model::GetBonePosition(hModel_, "wheel_RL"));
	pWheels_[WHEEL_RR] = CreateGameObject<Wheel>(this);
	pWheels_[WHEEL_RR]->SetPosition(Model::GetBonePosition(hModel_, "wheel_RR"));

	//カメラを生成
	pCamera_ = CreateGameObject<Camera>(this);
	//位置、焦点を設定
	D3DXVECTOR3 cameraPos = CAMERA_POSITION;
	pCamera_->SetPosition(cameraPos);
	//焦点設定
	D3DXVECTOR3 cameraTarget = CAMERA_TARGET;
	pCamera_->SetTarget(cameraTarget);
}


void Player::Draw()
{
	//行列を設定
	Model::SetMatrix(hModel_, worldMatrix_);
	
	//シェーダー
	PassShaderInfo();

	//pEffectを使って描画
	pEffect_->Begin(NULL, 0);

	//パス設定
	pEffect_->BeginPass(0);

	//描画
	Model::Draw(hModel_);

	//パス終了
	pEffect_->EndPass();
	pEffect_->End();

}

//解放
void Player::ReleaseAddData()
{
}

//移動
void Player::Move()
{
	//ゴール後はAIを使用
	if (state_ == CHARACTER_STATE_GOAL)
	{
		pAI_->drive();
	}
	else
	{
		//入力受付
		Input();
	}
}

//入力
void Player::Input()
{

	//加速（前進）
	if (Input::IsKey(DIK_K))
	{
		pPhysic_->MoveFront();
	}

	//減速（後退）
	if (Input::IsKey(DIK_J))
	{
		pPhysic_->MoveBack();
	}

	//右旋回
	if (Input::IsKey(DIK_D))
	{
		pPhysic_->TurnHandle(TURN_HANDLE_SPEED);
	}

	//左旋回
	if (Input::IsKey(DIK_A))
	{
		pPhysic_->TurnHandle(-TURN_HANDLE_SPEED);
	}
}

//レース開始
void Player::StartGame()
{
	state_ = CHARACTER_STATE_RACE;
	pUI_->StartCount();
}

//レース終了
void Player::EndGame()
{
	state_ = CHARACTER_STATE_GOAL;
	pUI_->EndCount();
}

//リザルトを表示
void Player::ShowResult()
{
	pUI_->ShowResult();
}

