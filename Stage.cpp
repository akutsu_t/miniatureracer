#include "Stage.h"
#include "Engine/Model.h"
#include "Box.h"
#include "CheckPoint.h"
#include "Wall.h"
#include "Engine/Camera.h"

//定数
const int WALL_MAX = 39;			//壁の総数
const int CHECK_MAX = 21;			//チェックポイントの総数

//コンストラクタ
Stage::Stage(IGameObject * parent)
	:IGameObject(parent, "Stage"), wallMax_(WALL_MAX), checkMax_(CHECK_MAX)
{
}

//デストラクタ
Stage::~Stage()
{
}

//初期化
void Stage::Initialize()
{
	//エフェクト生成
	LPD3DXBUFFER err = 0;		//エラー用
	if (FAILED(D3DXCreateEffectFromFile(Direct3D::pDevice_, "CommonShader.hlsl",
		NULL, NULL, D3DXSHADER_DEBUG, NULL, &pEffect_, &err)))
	{
		//失敗時	親ウィンドウ	表示内容			ウィンドウバー		ボタン
		MessageBox(NULL, (char*)err->GetBufferPointer(), "シェーダエラー", MB_OK);
		err->Release();
	}

	//ステージモデルのロード
	hModel_ = Model::Load("Data/Stage.fbx", pEffect_);
	assert(hModel_ >= 0);

	//コース内の壁を全て生成
	CreateWall(wallMax_);
	//コース内のチェックポイントを生成
	CreateCheckPoint(checkMax_);

	CreateGameObject<Camera>(this);
}

//更新
void Stage::Update()
{
}

//描画
void Stage::Draw()
{
	//行列を設定
	Model::SetMatrix(hModel_, worldMatrix_);

	//シェーダー
	PassShaderInfo();

	//pEffectを使って描画
	pEffect_->Begin(NULL, 0);

	//パス設定
	pEffect_->BeginPass(0);

	//描画
	Model::Draw(hModel_);

	//パス終了
	pEffect_->EndPass();
	pEffect_->End();
}

//解放
void Stage::Release()
{
}

//コース内の壁を生成
void Stage::CreateWall(int wallMaxNum)
{
	const std::string name = "wall";
	for (int i = 0; i < wallMaxNum; i++)
	{
		//壁を生成
		Box* pBox = CreateGameObject<Box>(this);
		//ボーンの各情報を取得し、反映
		//位置
		D3DXVECTOR3 pos = Model::GetBonePosition(hModel_, name + std::to_string(i));
		pBox->SetPosition(pos);
		//角度
		D3DXVECTOR3 rotate = Model::GetBoneRotate(hModel_, name + std::to_string(i));
		pBox->SetRotate(rotate);
		//拡大率
		D3DXVECTOR3 scale = Model::GetBoneScale(hModel_, name + std::to_string(i));
		pBox->SetScale(scale);
	}

	CreateGameObject<Wall>(this);
}

//コース内のチェックポイントを生成
void Stage::CreateCheckPoint(int checkMaxNum)
{
	const std::string name = "checkPoint";
	for (int i = 0; i < checkMaxNum; i++)
	{
		//チェックポイントを生成
		CheckPoint* pCheckPoint = CreateGameObject<CheckPoint>(this);
		//ボーンの各情報を取得し、反映
		//位置
		D3DXVECTOR3 pos = Model::GetBonePosition(hModel_, name + std::to_string(i));
		pCheckPoint->SetPosition(pos);
		//角度
		D3DXVECTOR3 rotate = Model::GetBoneRotate(hModel_, name + std::to_string(i));
		pCheckPoint->SetRotate(rotate);
		//拡大率
		D3DXVECTOR3 scale = Model::GetBoneScale(hModel_, name + std::to_string(i));
		pCheckPoint->SetScale(scale);
	}
}

//任意のボーンの位置を返す
D3DXVECTOR3 Stage::GetBonePosition(const std::string & boneName)
{
	//ボーンの位置を取得
	D3DXVECTOR3 pos = Model::GetBonePosition(hModel_, boneName);
	return pos;
}
