#pragma once
#include "Engine/IGameObject.h"

//地面を管理するクラス
class Stage : public IGameObject
{
	int wallMax_;			//壁の最大数
	int checkMax_;			//チェックポイントの最大数
	int hStartLine_;		//スタートラインのモデル番号

public:
	//コンストラクタ
	Stage(IGameObject* parent);

	//デストラクタ
	~Stage();

	//初期化
	void Initialize() override;

	//更新
	void Update() override;

	//描画
	void Draw() override;

	//解放
	void Release() override;

	//コース内の壁を生成
	//引数：wallMaxNum 壁の最大数
	void CreateWall(int wallMaxNum);

	//コース内のチェックポイントを生成
	//引数：checkMaxNum チェックポイントの最大数
	void CreateCheckPoint(int checkMaxNum);

	//任意の名前のボーン位置を返す
	//引数：boneName ボーンの名前
	//戻り値：ボーンの位置ベクトル
	D3DXVECTOR3 GetBonePosition(const std::string & boneName);

	//アクセス関数
	int GetWallMax() { return wallMax_; }
	int GetCheckMax() { return checkMax_; }
};