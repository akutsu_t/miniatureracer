#pragma once

#include "Engine/global.h"

//画像
enum TITLE_PICT {
	TITLE_PICT_TITLE,			//タイトル画像
	TITLE_PICT_LOAD,			//ロード画像
	TITLE_PICT_MAX
};

//タイトルシーンを管理するクラス
class TitleScene : public IGameObject
{
	int hPict_[TITLE_PICT_MAX];			//画像番号
	TITLE_PICT pict_;							//表示画像

public:
	//コンストラクタ
	//引数：parent  親オブジェクト（SceneManager）
	TitleScene(IGameObject* parent);

	//初期化
	void Initialize() override;

	//更新
	void Update() override;

	//描画
	void Draw() override;

	//解放
	void Release() override;
};