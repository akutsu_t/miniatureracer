#pragma once
#include <d3dx9.h>

//前方宣言
class Character;
class CarPhysic;
class Random;
class Stage;

//キャラクターのAIを管理するクラス
class CharacterAI
{
	Character* pCharacter_;			//このAIを使うキャラクター
	CarPhysic* pPhysic_;			//キャラクターの物理演算クラス
	Stage* pStage_;					//ステージクラス
	Random* pRandom_;				//乱数生成クラス

	float yellowZone_;				//アクセルを緩め始める基準
	float redZone_;					//ブレーキをかけ始める基準
	D3DXVECTOR3 nextCheckPos_;		//次の目標地点へのベクトル
	float turnHandleSpeed_;			//ハンドルの回転速度
public:
	//コンストラクタ
	//引数：Character　所持するキャラクター
	//引数：pPhysic　キャラクターが使用している物理演算クラス
	//引数：pStage　現在のステージクラス
	CharacterAI(Character* pCharacter, CarPhysic* pPhysic);

	//デストラクタ
	~CharacterAI();

	//運転する
	void drive();

	//次の目標地点までのベクトルを求める
	void CalcNextPoint();

	//ステージ情報を設定
	void SetStage(Stage* pStage);

};

