#include "UI.h"
#include "TimeBoard.h"
#include "Speedometer.h"
#include "RankBoard.h"
#include "Player.h"
#include "Result.h"
#include "Engine/Global.h"

//コンストラクタ
UI::UI(IGameObject * parent) : IGameObject(parent, "UI"), pTimeBoard_(nullptr), pSpeedometer_(nullptr),
	pRankBoard_(nullptr), pPlayer_((Player*)parent), pResult_(nullptr)
{
}

//デストラクタ
UI::~UI()
{
}

//初期化
void UI::Initialize()
{
	//各表示クラスを生成
	pTimeBoard_ = CreateGameObject<TimeBoard>(this);
	pSpeedometer_ = CreateGameObject<Speedometer>(this);
	pRankBoard_ = CreateGameObject<RankBoard>(this);
	pResult_ = CreateGameObject<Result>(this);
}

//更新
void UI::Update()
{
	//各UIの数値を更新
	//順位
	pRankBoard_->SetRank(pPlayer_->GetRank());
	//速度
	D3DXVECTOR3 speed = pPlayer_->GetVelocity();
	pSpeedometer_->SetSpeed((int)D3DXVec3Length(&speed));
	//最大周回数を設定
	pTimeBoard_->SetLapMax(pPlayer_->GetLapMax());
	//周回数
	pTimeBoard_->SetLap(pPlayer_->GetLap());
}

//描画
void UI::Draw()
{
	//fps表示
	OutputDebugString((std::to_string(g.fps) + "\n").c_str());
}

//解放
void UI::Release()
{
}

//速度表示クラスに速度を渡す
void UI::SetSpped(float speed)
{
	pSpeedometer_->SetSpeed((int)speed);
}

//タイム計測開始
void UI::StartCount()
{
	pTimeBoard_->StartCount();
}

//タイム計測終了
void UI::EndCount()
{
	pTimeBoard_->EndCount();
}

//リザルトを表示
void UI::ShowResult()
{
	//各情報を伝える
	int minute = pTimeBoard_->GetMinute();
	int second = pTimeBoard_->GetSecond();
	int microSecond = pTimeBoard_->GetMicroSecond();
	pResult_->SetTime(minute, second, microSecond);
	pResult_->SetRank(pRankBoard_->GetRank());

	//表示
	pResult_->ShowResult();

	//他のUIを非表示にする
	pSpeedometer_->SetIsDraw(false);
	pRankBoard_->SetIsDraw(false);
	pTimeBoard_->SetIsDraw(false);
}

