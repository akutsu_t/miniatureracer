#pragma once
#include "Engine/IGameObject.h"
#include <vector>
#include <chrono>				//時間測定用

//前方宣言
class Character;
class Stage;
class Player;

//画像
enum JUDGE_PICT {
	JUDGE_PICT_NUMBER_0,
	JUDGE_PICT_NUMBER_1,
	JUDGE_PICT_NUMBER_2,
	JUDGE_PICT_NUMBER_3,
	JUDGE_PICT_GO,
	JUDGE_PICT_GOAL,
	JUDGE_PICT_MAX
};

//状態
enum JUDGE_STATE {
	JUDGE_STATE_WAIT,			//待機
	JUDGE_STATE_READY,			//スタートカウント
	JUDGE_STATE_START,			//スタート合図
	JUDGE_STATE_RACE,			//レース中
	JUDGE_STATE_FINISH,			//レース終了
	JUDGE_STATE_RESULT,			//結果表示
	JUDGE_STATE_MAX,
};

//審判を管理するクラス
class Judge : public IGameObject
{
	int hPict_[JUDGE_PICT_MAX];				//画像番号
	std::string pictName_[JUDGE_PICT_MAX];	//画像名
	std::list<Character*> rankList_;		//順位リスト
	std::vector<Character*> finishList_;	//ゴールしたキャラクターのリスト
	Stage* pStage_;							//ステージのポインタ
	bool isStart_;							//スタートしたか
	bool isEnd_;							//レースが終了したか
	JUDGE_STATE state_;						//状態

	std::chrono::system_clock::time_point start_;		//開始時間
	double time_;										//経過時間
	int second_;										//経過秒
	int lapMax_;										//最大周回数

	Player* pPlayer_;						//プレイヤー
	bool isSignal_;							//開始合図をしたか
	
public:
	//コンストラクタ
	Judge(IGameObject* parent);

	//デストラクタ
	~Judge();

	//初期化
	void Initialize() override;

	//更新
	void Update() override;

	//描画
	void Draw() override;

	//解放
	void Release() override;

	//任意のキャラクターをリストに登録する
	//引数：pCharacter 追加したいキャラクター
	void RegistCharacter(Character* pCharacter);

	//キャラクター登録完了
	void CompleteRegistration();

	//任意のキャラクターをリストから削除
	//引数：pCharacter 削除したいキャラクター
	void RemoveCharacter(Character* pCharacter);

	//順位リストに任意のキャラクターが存在するか調べる
	//引数：pCharacter 調べたいキャラクター
	//戻り値：存在するなら真, 存在しないなら偽
	bool IsExistInRankList(Character* pCharacter);

	//任意のキャラクターをゴールリストに追加
	void AddToFinishList(Character* pCharacter);

	//順位判定
	void JudgeRanking();

	//チェックポイント内の位置比較（チェックポイントが同じ場合）
	//引数：player1 比較対象1,	player2 比較対象2
	//戻り値：player1が前にいたら真, 後ろにいるなら偽
	bool ComparePosition(Character* pCharacter1, Character* pCharacter2);

	//リスト内のキャラクターに現在の順位を通知
	void NoticeCharacterOfRank();

	//スタート合図を出す
	void SignalStart();

	//時間を測定する
	//戻り値：経過時間(1/100秒)
	double MeasureTime();

	//レース終了か判定する
	//戻り値：レース終了なら真
	bool DoJudgeEnd();

	//アクセス関数
	void SetStage(Stage* pStage) { pStage_ = pStage; }
	int GetLapMax() { return lapMax_; }
};