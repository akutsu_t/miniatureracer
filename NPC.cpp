#include "NPC.h"
#include "Engine/Model.h"
#include "CarPhysic.h"
#include "Engine/BoxCollider.h"
#include "Stage.h"
#include "Engine/Camera.h"
#include "Wheel.h"
#include "CharacterAI.h"

//定数
const D3DXVECTOR3 COLLIDER_SIZE = D3DXVECTOR3(2.5f, 2.0f, 4.0f);	//当たり判定のサイズ
const D3DXVECTOR3 COLLIDER_CENTER = D3DXVECTOR3(0, 1.0f, 0);		//当たり判定の中心位置

NPC::NPC(IGameObject* parent) : Character(parent, "NPC")
{
}


NPC::~NPC()
{
}

//初期化
void NPC::InitializeAddData()
{
	//エフェクト生成
	LPD3DXBUFFER err = 0;		//エラー用
	if (FAILED(D3DXCreateEffectFromFile(Direct3D::pDevice_, "CharacterShader.hlsl",
		NULL, NULL, D3DXSHADER_DEBUG, NULL, &pEffect_, &err)))
	{
		//失敗時	親ウィンドウ	表示内容			ウィンドウバー		ボタン
		MessageBox(NULL, (char*)err->GetBufferPointer(), "シェーダエラー", MB_OK);
	}

	//モデルデータのロード
	hModel_ = Model::Load("Data/Car_Red.fbx", pEffect_, true);
	assert(hModel_ > 0);

	//箱型コライダーをつける
	SetBoxCollider(COLLIDER_CENTER, COLLIDER_SIZE, worldMatrix_);

	//指定位置にタイヤを生成
	pWheels_[WHEEL_FL] = CreateGameObject<Wheel>(this);
	pWheels_[WHEEL_FL]->SetPosition(Model::GetBonePosition(hModel_, "wheel_FL"));
	pWheels_[WHEEL_FR] = CreateGameObject<Wheel>(this);
	pWheels_[WHEEL_FR]->SetPosition(Model::GetBonePosition(hModel_, "wheel_FR"));
	pWheels_[WHEEL_RL] = CreateGameObject<Wheel>(this);
	pWheels_[WHEEL_RL]->SetPosition(Model::GetBonePosition(hModel_, "wheel_RL"));
	pWheels_[WHEEL_RR] = CreateGameObject<Wheel>(this);
	pWheels_[WHEEL_RR]->SetPosition(Model::GetBonePosition(hModel_, "wheel_RR"));
}

//描画
void NPC::Draw()
{
	//行列を設定
	Model::SetMatrix(hModel_, worldMatrix_);

	//シェーダー
	PassShaderInfo();

	//pEffectを使って描画
	pEffect_->Begin(NULL, 0);

	//パス設定
	pEffect_->BeginPass(0);

	//描画
	Model::Draw(hModel_);

	//パス終了
	pEffect_->EndPass();

	pEffect_->End();
}

//解放
void NPC::ReleaseAddData()
{
}

//移動
void NPC::Move()
{
	pAI_->drive();
}






