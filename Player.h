#pragma once
#include "Character.h"

//前方宣言
class Camera;
class UI;

//自機を管理するクラス
class Player : public Character
{	
	Camera* pCamera_;		//デバッグ用テキスト表示
	UI* pUI_;				//各情報表示クラス

public:
	//コンストラクタ
	Player(IGameObject* parent);

	//デストラクタ
	~Player();

	//初期化
	void InitializeAddData() override;

	//描画
	void Draw() override;

	//解放
	void ReleaseAddData() override;

	//移動
	void Move() override;

	//入力
	void Input();

	//レース開始
	void StartGame() override;

	//ゲーム終了
	void EndGame() override;

	//リザルトを表示
	void ShowResult();
};