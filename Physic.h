#pragma once

#include <d3dx9.h>

class Collider;
class IGameObject;

//物理演算用のクラス
class Physic
{
protected:
	IGameObject*	pParent_;		//このクラスを持つオブジェクトのポインタ
	float			time_;			//差分時間
	D3DXVECTOR3		pos_;			//位置
	D3DXVECTOR3		rotate_;		//角度
	D3DXVECTOR3		vel_;			//速度
	D3DXVECTOR3		acc_;			//加速度
	D3DXVECTOR3		gravityAcc_;	//重力加速度
	float			staticFric_;	//静止摩擦係数
	float			dynamicFric_;	//動摩擦係数
	D3DXVECTOR3		angularVel_;	//角速度
	D3DXVECTOR3		size_;			//大きさ
	float			mass_;			//質量
	Collider*		pCollider_;		//衝突判定用コライダー
	bool			isStage_;		//接地しているかどうか

public:
	//引数ありコンストラクタ
	//引数：parent このクラスを持つオブジェクト（親）
	Physic(IGameObject* parent);
	~Physic();

	//更新
	virtual void Updata(float dt, bool isResetAccel = true);

	//接地しているか調べる
	//引数：pos オブジェクトの中心位置
	virtual bool IsStageed(D3DXVECTOR3& pos);

	//地面までの距離を求める
	//引数：pos オブジェクトの中心位置
	float DistToStage(D3DXVECTOR3& pos);


	//セッター
	virtual void SetPos(const D3DXVECTOR3 &pos) { pos_ = pos; }
	virtual void SetRotate(const D3DXVECTOR3 &rotate) { rotate_ = rotate; }
	virtual void AddRotate(const D3DXVECTOR3 &rotate) { rotate_ += rotate; }
	virtual void SetVelocity(const D3DXVECTOR3 &vel) { vel_ = vel; }
	virtual void AddVelocity(const D3DXVECTOR3 &vel) { vel_ += vel; }
	virtual void SetAccel(const D3DXVECTOR3 &acc) { acc_ = acc; }
	virtual void AddAccel(const D3DXVECTOR3 &acc) { acc_ += acc; }
	virtual void SetGravityAcc(const D3DXVECTOR3 &gravityAcc) { gravityAcc_ = gravityAcc; }
	virtual void SetStaticFric(const float staticFric) { staticFric_ = staticFric; }
	virtual void SetDynamicFric(const float dynamicFric) { dynamicFric_ = dynamicFric; }
	virtual void SetAngularVel(const D3DXVECTOR3 &angularVel) { angularVel_ = angularVel; }
	virtual void AddAngularVel(const D3DXVECTOR3 &angularVel) { angularVel_ += angularVel; }
	virtual void SetSize(const D3DXVECTOR3 &size) { size_ = size; }
	virtual void SetMass(const float mass) { mass_ = mass; }
	virtual void SetCollider(Collider* pCollider) { pCollider_ = pCollider; }

	//ゲッター
	virtual D3DXVECTOR3& GetPos() { return pos_; }
	virtual D3DXVECTOR3& GetRotate() { return rotate_; }
	bool GetIsStage() { return isStage_; }
};

