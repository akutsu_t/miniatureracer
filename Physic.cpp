#include <string>
#include "Physic.h"
#include "Engine/IGameObject.h"
#include "Engine/Model.h"

Physic::Physic(IGameObject* parent):pParent_(parent), time_(1 / 60.0f), gravityAcc_(D3DXVECTOR3(0, -9.8f, 0)), pos_(D3DXVECTOR3(0, 5, 0)),
	acc_(D3DXVECTOR3(0, 0, 0)), size_(D3DXVECTOR3(0, 0, 0))
{
}


Physic::~Physic()
{
}

//更新
void Physic::Updata(float dt, bool isResetAccel)
{
	//速度による回転
	D3DXMATRIX matR;
	D3DXMatrixRotationY(&matR, D3DXToRadian(-rotate_.y));
	//速度ベクトルをy角度をもとにz軸に平行にする
	D3DXVec3TransformCoord(&vel_, &vel_, &matR);
	//横滑り
	if (vel_.x > mass_ * staticFric_)
	{
		vel_.x -= mass_ * dynamicFric_;
	}
	//z速度をもとに回転
	rotate_.x += D3DXToDegree(vel_.z * time_ / size_.y);
	//回転に使った分減速
	vel_.z -= vel_.z * time_ / size_.y;
	//速度ベクトルをもとに戻す
	D3DXMatrixRotationY(&matR, D3DXToRadian(rotate_.y));
	D3DXVec3TransformCoord(&vel_, &vel_, &matR);

	//回転
	D3DXVECTOR3 nextRot = angularVel_ * time_;
	rotate_ += nextRot * dt;
	//回転による移動ベクトル
	D3DXVECTOR3 rotateVel = D3DXVECTOR3(0, 0, 0);


	//加速度
	acc_ += gravityAcc_;
	vel_ += acc_ * time_;
	//指定フレーム数分の移動ベクトルを算出
	D3DXVECTOR3 nextPos = (vel_ + acc_ * time_) * time_ * dt;

	//回転による移動（仮）
	rotateVel = D3DXVECTOR3(0, 0, nextRot.x * size_.y) ;
	//y角度に合わせて移動ベクトルを変形
	D3DXMATRIX matY;
	D3DXMatrixRotationY(&matY, D3DXToRadian(rotate_.y));
	D3DXVec3TransformCoord(&rotateVel, &rotateVel, &matY);
	//静止摩擦力より大きい
	if (D3DXVec3Length(&rotateVel) > (staticFric_ * mass_))
	{
		D3DXVec3Normalize(&rotateVel, &rotateVel);
		rotateVel *= dynamicFric_ * mass_;
		angularVel_ -= angularVel_ * time_;
	}
	vel_ += rotateVel * time_;
	nextPos += rotateVel * dt;

	//地面に接地しているか
	isStage_ = IsStageed(pos_);
	if (isStage_)
	{
		//地面との距離を求め、y位置を地面の高さに戻す
		pos_.y = DistToStage(pos_) + size_.y;

		//y成分を初期化
		acc_.y = 0;
		vel_.y = 0;
		nextPos.y = 0;

		//摩擦
		if (D3DXVec3Length(&vel_) > staticFric_ * mass_)
		{
			D3DXVECTOR3 velNormal;
			D3DXVec3Normalize(&velNormal , &vel_);
			vel_ -= velNormal * dynamicFric_ * mass_;
		}
		else
		{
			acc_ = D3DXVECTOR3(0, acc_.y, 0);

		}

		angularVel_ -= angularVel_ * time_;

	}
	else
	{
		acc_ = D3DXVECTOR3(0, acc_.y, 0);
		vel_ -= rotateVel;
		nextPos -= rotateVel * time_ * dt;
	}
	
	//位置を更新
	pos_ += nextPos;

	if (isResetAccel)
	{
		acc_ = D3DXVECTOR3(0, 0, 0);
	}

}

//接地しているか調べる
bool Physic::IsStageed(D3DXVECTOR3& pos)
{
	IGameObject* pObj = pParent_->FindObject("Stage");
	if (pObj != nullptr)
	{
		int hStageModel = pObj->GetModelHandle();
		//レイを飛ばす準備
		RayCastData data;
		data.start = pos + D3DXVECTOR3(0, 20, 0);
		data.dir = D3DXVECTOR3(0, -1, 0);
		//レイを発射
		Model::RayCast(hStageModel, &data);

		//レイの発射位置＋物体の中心から地面までの高さより近い
		if (data.hit && data.dist < 20 + size_.y)
		{
			//地面についている
			return true;
		}
	}

	return false;
}

float Physic::DistToStage(D3DXVECTOR3& pos)
{
	//地面を探す
	IGameObject* pObj = pParent_->FindObject("Stage");

	//地面が見つかった
	if (pObj != nullptr)
	{
		int hModel = pObj->GetModelHandle();
		//レイを飛ばす準備
		RayCastData data;
		data.start = pos + D3DXVECTOR3(0, 20, 0);
		data.dir = D3DXVECTOR3(0, -1, 0);
		//レイを発射
		Model::RayCast(hModel, &data);

		//レイが当たった
		if (data.hit)
		{
			//物体の底面から地面までの距離を返す
			return data.start.y - data.dist;
		}
	}

	//測定失敗（真下に地面がない）
	return -1;
}

