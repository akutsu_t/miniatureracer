#pragma once
#include <d3dx9.h>


//車の物理演算クラス
class CarPhysic
{
private:
	D3DXVECTOR3 velocity_;		//速度[m/s]
	D3DXVECTOR3 position_;		//位置[m]
	D3DXVECTOR3 rotate_;		//角度[deg]
	int rpm_;					//エンジンの回転数[rpm]
	D3DXVECTOR3 friction_;		//タイヤの最大摩擦力[N]
	float accel_;				//アクセルの開き具合(0.0〜1.0)
	float brake_;				//ブレーキの開き具合(0.0〜1.0)
	D3DXVECTOR3 wheelRotate_;	//タイヤの角度[deg]
	float handleRotate_;		//ハンドルの角度[deg]
	D3DXVECTOR3 externalForce_;	//外部から受けた力[N]
	int gear_;					//ギア

	//定数
	enum GEAR {				//ギア
		GEAR_1,				//1
		GEAR_2,				//2
		GEAR_3,				//3
		GEAR_BACK,			//後退
		GEAR_MAX
	};
	static constexpr float	MASS = 200.0f;							//質量[kg]
	static constexpr float	TREAD_WIDTH = 3.0f;						//トレッド幅[m]
	static constexpr float	TOTAL_HEIGHT = 2.0f;					//全高[m]
	static constexpr float	WHEEL_BASE = 40.0f;						//ホイールベース[m]
	static constexpr int	RPM_UP_RATE = 10;						//エンジン回転数の上昇率[rpm]
	static const int		RPM_DOWN_RATE = 5;						//エンジン回転数の減少率[rpm]
	static constexpr float	FRICTION_RATE = 0.7f;					//摩擦係数
	static constexpr float	FRICTION_RATE_F = 0.1f;					//前方方向の摩擦
	static constexpr float	WHEEL_RADIUS = 1.0f;					//タイヤ半径[m]
	static constexpr float	WHEEL_ROTATE_MAX = 30.0f;				//タイヤの最大角度
	static constexpr float	WHEEL_ROTATE_MIN = -30.0f;				//タイヤの最小角度
	static constexpr float	HANDLE_REVERSE_SPEED = 10.0f;			//ハンドルが元の状態に戻る速度
	static constexpr float	PEDAL_REVERSE_SPEED = 0.1f;				//ペダルが元の状態に戻る速度
	static constexpr float	PEDAL_STEP_SPEED = 0.2f;				//ペダルを踏みこむ速度
	static constexpr float	HANDLE_GEAR_RATE = 10.0f;				//ハンドルギア比
	static constexpr float	AIR_DENSITY = 1.293f;					//空気密度
	static constexpr float	GRAVITY = 9.8f;							//重力
	static const int		GEAR_TABLE_INTERVAL = 3000;				//ギアテーブルの要素間の回転数
	//ギア比テーブル
	static constexpr float	GEAR_TABLE[GEAR_MAX] = { 20.0f, 13.0f, 8.0f, 20.0f };

	//アクセルペダルを踏む
	void StepOnAccelPedal();

	//ブレーキペダルを踏む
	void StepOnBrakePedal();

	//各ペダルの更新
	void UpdatePedal();

	//エンジンの更新
	void UpdateEngine();

	//ハンドルの更新
	void UpdateHandle();

	//タイヤの角度を更新
	//引数：time 経過時間
	void UpdateWheelRotate(float time);

	//力を元に速度を計算
	//引数：time 経過時間
	void CalcVelocity(float time);

public:
	CarPhysic();
	~CarPhysic();

	//更新
	//引数：time 経過時間
	void Update(float time);

	//力を加える
	//引数：force 加える力[N]
	void AddForce(D3DXVECTOR3& force);

	//ハンドルを切る
	//引数：rotate 回転させる角度
	void TurnHandle(float rotate);

	//速度を元にエンジン回転数を再計算
	void CalcRpm();

	//前進
	void MoveFront();

	//後退
	void MoveBack();

	//アクセス関数
	D3DXVECTOR3& GetVelocity() { return velocity_; }
	D3DXVECTOR3& GetPosition() { return position_; }
	D3DXVECTOR3& GetRotate() { return rotate_; }
	D3DXVECTOR3 GetForce() { return velocity_ * MASS; }
	D3DXVECTOR3& GetWheelRotate() { return wheelRotate_; }
	void SetVelocity(D3DXVECTOR3 vel) { velocity_ = vel; }
	void SetAccel(float accel) { accel_ = accel; }
	void SetBrake(float brake) { brake_ = brake; }
	void SetWheelRotate(D3DXVECTOR3& rotate) { wheelRotate_ = rotate; }
	void SetPosition(D3DXVECTOR3& position) { position_ = position; }
	void AddPosition(D3DXVECTOR3& position) { position_ += position; }
};

