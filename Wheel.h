#pragma once
#include "Engine/IGameObject.h"

//タイヤを管理するクラス
class Wheel : public IGameObject
{
	//定数
	static constexpr float wheelRadius_ = 1.0f;			//タイヤ半径

public:
	//コンストラクタ
	Wheel(IGameObject* parent);

	//デストラクタ
	~Wheel();

	//初期化
	void Initialize() override;

	//更新
	void Update() override;

	//描画
	void Draw() override;

	//解放
	void Release() override;

};

