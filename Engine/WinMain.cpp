#include <Windows.h>
#include <chrono>				//時間測定用
#include "Direct3D.h"
#include "Global.h"
#include "Model.h"
#include "Image.h"
#include "Audio.h"

#include "GameObjectManager.h"

//Debug時のメモリリークチェック用
#ifdef _DEBUG
#include <crtdbg.h>
#endif

//リンカ(Direct3Dが使えるようになる)
#pragma comment(lib,"d3d9.lib")
#pragma comment(lib,"d3dx9.lib")

//定数
const char* WIN_CLASS_NAME = "Miniature Racer";	//ウィンドウクラス名
const int	WINDOW_WIDTH = 1200;				//ウィンドウの幅
const int	WINDOW_HEIGHT = 800;				//ウィンドウの高さ					

//グローバル変数
Global g;

//プロトタイプ宣言
LRESULT CALLBACK WndProc(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam);

//エントリーポイント(プログラムのmain)
int APIENTRY WinMain(HINSTANCE hInstance, HINSTANCE hPrevInst, LPSTR lpCmdLine, int nCmdShow)
{
//Debug時にメモリリークチェック
#ifdef _DEBUG
	// メモリリーク検出
	_CrtSetDbgFlag(_CRTDBG_ALLOC_MEM_DF | _CRTDBG_LEAK_CHECK_DF);
#endif

	//ウィンドウクラス（設計図）を作成
	WNDCLASSEX wc;
	wc.cbSize = sizeof(WNDCLASSEX);					//この構造体のサイズ
	wc.hInstance = hInstance;						//インスタンスハンドル
	wc.lpszClassName = WIN_CLASS_NAME;				//ウィンドウクラス名
	wc.lpfnWndProc = WndProc;						//ウィンドウプロシージャ
	wc.style = CS_VREDRAW | CS_HREDRAW;				//スタイル（デフォルト）
	wc.hIcon = LoadIcon(NULL, IDI_APPLICATION);		//アイコン
	wc.hIconSm = LoadIcon(NULL, IDI_WINLOGO);		//小さいアイコン
	wc.hCursor = LoadCursor(NULL, IDC_ARROW);		//マウスカーソル
	wc.lpszMenuName = NULL;							//メニュー（なし）
	wc.cbClsExtra = 0;
	wc.cbWndExtra = 0;
	wc.hbrBackground = (HBRUSH)GetStockObject(WHITE_BRUSH);	//背景（白）

	RegisterClassEx(&wc);	//クラスを登録

	RECT winRect = { 0, 0, WINDOW_WIDTH, WINDOW_HEIGHT };	//長方形を表すクラス
	AdjustWindowRect(&winRect, WS_OVERLAPPEDWINDOW, FALSE);	//ウィンドウの大きさの調整
	
	//ウィンドウを作成
	g.hWnd = CreateWindow(
		WIN_CLASS_NAME,					//ウィンドウクラス名
		"Miniature Racer",    			//タイトルバーに表示する内容
		WS_OVERLAPPEDWINDOW,			//スタイル（普通のウィンドウ）
		CW_USEDEFAULT,					//表示位置左（おまかせ）
		CW_USEDEFAULT,					//表示位置上（おまかせ）
		winRect.right - winRect.left,	//ウィンドウ幅
		winRect.bottom - winRect.top,	//ウィンドウ高さ
		NULL,							//親ウインドウ（なし）
		NULL,							//メニュー（なし）
		hInstance,						//インスタンス
		NULL							//パラメータ（なし）
	);
	assert(g.hWnd != NULL);				//エラー処理

	//ウィンドウを表示
	ShowWindow(g.hWnd, nCmdShow);

	g.screenWidth = WINDOW_WIDTH;
	g.screenHeight = WINDOW_HEIGHT;

	//Direct3Dの初期化（準備）
	Direct3D::Initialize(g.hWnd);

	//DirectInputの初期化
	Input::Initialize(g.hWnd);

	//3Dモデル準備
	Model::Initialize();

	//Audioの初期化
	Audio::Initialize();
	//Audioデータをロード
	Audio::LoadWaveBank("Data/Sound/Wave Bank.xwb");
	Audio::LoadSoundBank("Data/Sound/Sound Bank.xsb");

	//ルートオブジェクトとシーンマネージャーを生成
	GameObjectManager::Initialize();

	//時間測定用基準時間
	DWORD baseTime = timeGetTime();

	//メッセージループ（何か起きるのを待つ）
	MSG msg;
	ZeroMemory(&msg, sizeof(msg));
	while (msg.message != WM_QUIT)
	{
		//メッセージあり
		if (PeekMessage(&msg, NULL, 0U, 0U, PM_REMOVE))
		{
			TranslateMessage(&msg);
			DispatchMessage(&msg);
		}

		//メッセージなし
		else
		{
			//経過時間を計測
			DWORD nowTime = timeGetTime();
			float time = (float)(nowTime - baseTime);
			//経過時間から1秒あたりのフレーム数を算出
			g.fps = 1 / (time / 1000.0f);
			//基準時間を更新
			baseTime = nowTime;

			//入力情報更新
			Input::Update();

			//ルート更新
			GameObjectManager::Update();

			//描画開始
			Direct3D::BeginDraw();

			//描画
			GameObjectManager::Draw();

			//描画終了
			Direct3D::EndDraw();
			
		}
	}

	//解放
	GameObjectManager::Release();
	Audio::Release();
	Input::Release();
	Direct3D::Release();

	//ロードしたデータを全削除
	Model::AllRelease();
	Image::AllRelease();
	return 0;
}

//ウィンドウプロシージャ（何かあった時によばれる関数）
LRESULT CALLBACK WndProc(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam)
{
	switch (msg)
	{
	case WM_DESTROY:
		PostQuitMessage(0);		//プログラム終了
		return 0;
	}
	return DefWindowProc(hWnd, msg, wParam, lParam);
}