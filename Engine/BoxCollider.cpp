#include "BoxCollider.h"
#include "IGameObject.h"
#include "Direct3D.h"


//コンストラクタ（当たり判定の作成）
//引数：basePos	当たり判定の中心位置（ゲームオブジェクトの原点から見た位置）
//引数：size	当たり判定のサイズ
BoxCollider::BoxCollider(IGameObject* pParent, D3DXVECTOR3 basePos, D3DXVECTOR3 size, D3DXMATRIX worldMatrix)
{
	center_ = basePos;
	size_ = size;

	direction_[COLLIDER_AXIS_X] = D3DXVECTOR3(worldMatrix._11, worldMatrix._12, worldMatrix._13);
	direction_[COLLIDER_AXIS_Y] = D3DXVECTOR3(worldMatrix._21, worldMatrix._22, worldMatrix._23);
	direction_[COLLIDER_AXIS_Z] = D3DXVECTOR3(worldMatrix._31, worldMatrix._32, worldMatrix._33);
	type_ = COLLIDER_TYPE_BOX;



	//リリース時は判定枠は表示しない
#ifdef _DEBUG
	//テスト表示用判定枠
	D3DXCreateBox(Direct3D::pDevice_, size.x, size.y, size.z, &pMesh_, 0);
#endif
}

//x,y,z軸の長さを渡す
float BoxCollider::GetLen(int axisNum)
{
	switch (axisNum)
	{
	case COLLIDER_AXIS_X:
		return size_.x * 0.5f;
		break;
	case COLLIDER_AXIS_Y:
		return size_.y * 0.5f;
		break;
	case COLLIDER_AXIS_Z:
		return size_.z * 0.5f;
		break;
	}

	return 0.0f;
}

//x,y,z軸の方向ベクトルを渡す
D3DXVECTOR3 BoxCollider::GetDirect(int axisNum)
{
	direction_[COLLIDER_AXIS_X] = D3DXVECTOR3(pGameObject_->GetWorldMatrix()._11, pGameObject_->GetWorldMatrix()._12, pGameObject_->GetWorldMatrix()._13);
	direction_[COLLIDER_AXIS_Y] = D3DXVECTOR3(pGameObject_->GetWorldMatrix()._21, pGameObject_->GetWorldMatrix()._22, pGameObject_->GetWorldMatrix()._23);
	direction_[COLLIDER_AXIS_Z] = D3DXVECTOR3(pGameObject_->GetWorldMatrix()._31, pGameObject_->GetWorldMatrix()._32, pGameObject_->GetWorldMatrix()._33);

	D3DXVECTOR3 dirNormal = direction_[axisNum];
	D3DXVec3Normalize(&dirNormal, &direction_[axisNum]);
	return dirNormal;
}

float BoxCollider::CalcProjectveLine(D3DXVECTOR3 & vec)
{
	//渡されたベクトルに対する射影線の長さを算出
	float len = 0.0f;				//近接距離
	D3DXVECTOR3 vecNormal;			//単位ベクトル
	D3DXVec3Normalize(&vecNormal, &vec);
	for (int i = 0; i < COLLIDER_AXIS_MAX; i++)
	{
		D3DXVECTOR3 dir = GetDirect(i);			//OBBの各軸ベクトル
		len += fabs(D3DXVec3Dot(&(dir * GetLen(i)), &vecNormal));
	}

	return len;
}

D3DXVECTOR4 BoxCollider::GetRectXZ()
{
	//x,z軸に対する射影線の長さを求める
	float xLen = CalcProjectveLine(D3DXVECTOR3(1, 0, 0));
	float zLen = CalcProjectveLine(D3DXVECTOR3(0, 0, 1));

	//求めた長さをもとにワールド座標上のx,z最小・最大値を求める
	float xMax = center_.x + xLen;
	float zMax = center_.z + zLen;
	float xMin = center_.x - xLen;
	float zMin = center_.z - zLen;

	return D3DXVECTOR4(xMin, zMax, xMax, zMin);
}

void BoxCollider::ChangeScale(D3DXVECTOR3 & scale)
{
	size_.x *= scale.x;
	size_.y *= scale.y;
	size_.z *= scale.z;
}

D3DXVECTOR3 BoxCollider::DetectHitSurface(int & axisNum, float xDistSunk, float yDistSunk, float zDistSunk, D3DXVECTOR3& objectDir)
{
	//値がNULLの軸方向
	if (yDistSunk == NULL)
	{
		yDistSunk = -9999;
	}

	//z軸
	if (zDistSunk > xDistSunk && zDistSunk > yDistSunk)
	{
		axisNum = COLLIDER_AXIS_Z;
		//z軸のどっち側の面か判定
		if (objectDir.z > 0)
		{
			//上面の法線を取得
			return GetDirect(COLLIDER_AXIS_Z);
		}
		else
		{
			//下面の法線を取得
			return -(GetDirect(COLLIDER_AXIS_Z));
		}
	}
	//y軸
	else if (yDistSunk > zDistSunk && yDistSunk > xDistSunk)
	{
		axisNum = COLLIDER_AXIS_Y;

		//x軸のどっち側の面か判定
		if (objectDir.y > 0)
		{
			//右面の法線を取得
			return GetDirect(COLLIDER_AXIS_X);
		}
		else
		{
			//左面の法線を取得
			return -(GetDirect(COLLIDER_AXIS_X));
		}
	}
	//x軸
	else
	{
		axisNum = COLLIDER_AXIS_X;
		//x軸のどっち側の面か判定
		if (objectDir.x > 0)
		{
			//右面の法線を取得
			return GetDirect(COLLIDER_AXIS_X);
		}
		else
		{
			//左面の法線を取得
			return -(GetDirect(COLLIDER_AXIS_X));
		}	
	}

	return D3DXVECTOR3(0, 0, 0);
}

//接触判定
//引数：target	相手の当たり判定
//戻値：接触してればtrue
bool BoxCollider::IsHit(Collider* target, float* length)
{
	switch (target->type_)
	{
	case COLLIDER_TYPE_BOX:
		return IsHitBoxVsBox(this, (BoxCollider*)target);
		break;

	case COLLIDER_TYPE_SPHERE:
		return IsHitBoxVsSphere(this, (SphereCollider*)target);
		break;

	case COLLIDER_TYPE_PLANE:
		return IsHitOBBVsPlane(this, (PlaneCollider*)target, length);
		break;

	default:
		return false;
		break;
	}
}

