#include "Global.h"

//初期化
LPDIRECT3D9			Direct3D::pD3d_ = nullptr;
LPDIRECT3DDEVICE9	Direct3D::pDevice_ = nullptr;
bool				Direct3D::isDrawCollision_ = false;

//Direct3Dの初期化
void Direct3D::Initialize(HWND hWnd)
{
	//Direct3Dオブジェクトの作成
	pD3d_ = Direct3DCreate9(D3D_SDK_VERSION);			//インタフェースはnewできないので専用の関数を使用
	assert(pD3d_ != nullptr);							//エラー処理

	//DIRECT3Dデバイスオブジェクトの作成
	D3DPRESENT_PARAMETERS d3dpp;						//専用の構造体
	ZeroMemory(&d3dpp, sizeof(d3dpp));					//中身を全部0にする
	d3dpp.BackBufferFormat = D3DFMT_X8R8G8B8;
	d3dpp.BackBufferCount = 1;
	d3dpp.SwapEffect = D3DSWAPEFFECT_DISCARD;
	d3dpp.Windowed = TRUE;								//ウィンドウかフルスクリーンか(true:ウィンドウ)
	d3dpp.EnableAutoDepthStencil = TRUE;				//深度ステンシルバッファ作成指示
	d3dpp.AutoDepthStencilFormat = D3DFMT_D16;			//深度バッファフォーマット
	d3dpp.BackBufferWidth = g.screenWidth;				//幅
	d3dpp.BackBufferHeight = g.screenHeight;			//高さ
	d3dpp.PresentationInterval = D3DPRESENT_INTERVAL_ONE;
	pD3d_->CreateDevice(D3DADAPTER_DEFAULT, D3DDEVTYPE_HAL, hWnd,
		D3DCREATE_HARDWARE_VERTEXPROCESSING, &d3dpp, &pDevice_);
	assert(pDevice_ != nullptr);							//エラー処理

	//アルファブレンド
	pDevice_->SetRenderState(D3DRS_ALPHABLENDENABLE, TRUE);
	pDevice_->SetRenderState(D3DRS_SRCBLEND, D3DBLEND_SRCALPHA);
	pDevice_->SetRenderState(D3DRS_DESTBLEND, D3DBLEND_INVSRCALPHA);

	//ライティング
	pDevice_->SetRenderState(D3DRS_LIGHTING, TRUE);		//TRUEがデフォルトなので記述しなくてもいい

	//ライトを設置
	D3DLIGHT9 lightState;
	ZeroMemory(&lightState, sizeof(lightState));		//すべて0で初期化
	//必要な項目のみを設定
	lightState.Type = D3DLIGHT_DIRECTIONAL;				//種類
	lightState.Direction = D3DXVECTOR3(1, -1, 1);		//向き
	lightState.Diffuse.r = 1.0f;						//色（R）
	lightState.Diffuse.g = 1.0f;						//色（G）
	lightState.Diffuse.b = 1.0f;						//色（B）
	lightState.Ambient.r = 1.0f;						//アンビエント(R)
	lightState.Ambient.g = 1.0f;						//アンビエント(G)
	lightState.Ambient.b = 1.0f;						//アンビエント(B)
	//ライトを作成	 //ライト番号
	pDevice_->SetLight(0, &lightState);
	//ライトをONにする
	pDevice_->LightEnable(0, TRUE);

	//コリジョン表示設定
	isDrawCollision_ = true;
}

//描画開始
void Direct3D::BeginDraw()
{
	//画面をクリア
	pDevice_->Clear(0, nullptr, D3DCLEAR_TARGET | D3DCLEAR_ZBUFFER,
		D3DCOLOR_XRGB(0, 0, 255), 1.0f, 0);

	//描画開始
	pDevice_->BeginScene();
}

//描画終了
void Direct3D::EndDraw()
{
	//描画終了
	pDevice_->EndScene();

	//スワップ
	pDevice_->Present(nullptr, nullptr, nullptr, nullptr);
}

//解放
void Direct3D::Release()
{
	//解放処理
	SAFE_RELEASE(pDevice_);
	SAFE_RELEASE(pD3d_);
}
