#pragma once
#include "Collider.h"


//箱型の当たり判定
class BoxCollider :	public Collider
{
	friend class Collider;

	//判定サイズ（幅、高さ、奥行き）
	D3DXVECTOR3 size_;

	//箱のx,y,z軸の方向ベクトル
	D3DXVECTOR3 direction_[3];
	
public:
	//コンストラクタ（当たり判定の作成）
	//引数：basePos	当たり判定の中心位置（ゲームオブジェクトの原点から見た位置）
	//引数：size	当たり判定のサイズ（幅、高さ、奥行き）
	BoxCollider(IGameObject* pParent, D3DXVECTOR3 basePos, D3DXVECTOR3 size, D3DXMATRIX matrix);

	//x,y,z軸の長さを渡す
	//引数：axisNum	取得したい軸番号(x:0, y:1, z;2)
	//戻り値：指定した軸の長さ
	float GetLen(int axisNum);

	//x,y,z軸の方向ベクトルを渡す
	//引数：axisNum	取得したい軸番号(x:0, y:1, z;2)
	//戻り値：指定した軸の方向ベクトル
	D3DXVECTOR3 GetDirect(int axisNum);

	//任意のベクトルに対する射影線の長さを算出
	float CalcProjectveLine(D3DXVECTOR3& vec);

	//xz平面上で自身を囲む矩形を算出し、x,z座標の最小・最大を返す
	//戻り値：求めた矩形のx,z座標の最小・最大(xMin, zMax, xMax, zMin)
	D3DXVECTOR4 GetRectXZ() override;

	//スケール変更
	//引数：scale	変更後のスケール
	void ChangeScale(D3DXVECTOR3& scale);

	//衝突面を判定し、その面の軸番号と正負を算出
	//引数：axisNum 軸番号格納先, DistSunk 各軸方向のめり込み距離, objectDir boxから見た衝突物の方向
	//戻り値：衝突面の法線
	D3DXVECTOR3 DetectHitSurface(int& axisNum, float xDistSunk, float yDistSunk, float zDistSunk, D3DXVECTOR3& objectDir);

private:
	//接触判定
	//引数：target	相手の当たり判定, length めり込んだ長さの格納先
	//戻値：接触してればtrue
	bool IsHit(Collider* target, float* length = nullptr) override;

};

