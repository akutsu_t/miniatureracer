#pragma once
#include "Global.h"

class Sprite
{
	LPD3DXSPRITE pSprite_;			//スプライト
	LPDIRECT3DTEXTURE9 pTexture_;	//テクスチャ
	float alpha_;					//色

public:
	Sprite();
	~Sprite();

	//画像ファイルの読み込み
	//引数：pPath	画像ファイルのパス
	//戻り値：なし
	void Load(const char *pPath);

	//描画
	//引数：matrix	画像位置のワールド行列(参照渡し)
	//戻り値：なし
	void Draw(const D3DXMATRIX &matrix);

	//テクスチャのサイズを取得
	D3DXVECTOR2 GetTextureSize();

	//透明度
	//引数:alfa　α値の変更
	void Setcolor(float alpha);
};

