#include "IGameObject.h"
#include "Collider.h"
#include "BoxCollider.h"
#include "PlaneCollider.h"
#include "Global.h"
#include "GameObjectManager.h"
#include "../CollisionTree.h"
#include <windows.h>


IGameObject::IGameObject()
	:IGameObject(nullptr, "")
{
}

IGameObject::IGameObject(IGameObject * parent)
	:IGameObject(parent, "")
{
}

IGameObject::IGameObject(IGameObject * parent, const std::string & name, int id) 
	:pParent_(parent), name_(name), position_(D3DXVECTOR3(0, 0, 0)),
	rotate_(D3DXVECTOR3(0, 0, 0)), scale_(D3DXVECTOR3(1, 1, 1)), isDead_(false), isHit_(false),
	pCollider_(nullptr), hModel_(-1), pEffect_(nullptr), pPre_(nullptr), pNext_(nullptr), id_(id),
	pCell_(nullptr)
{
	//初期化
	D3DXMatrixIdentity(&localMatrix_);
	D3DXMatrixIdentity(&worldMatrix_);
}


IGameObject::~IGameObject()
{
	//解放
	SAFE_DELETE(pCollider_);
	SAFE_RELEASE(pEffect_);
}

void IGameObject::Transform()
{
	//移動行列を作成
	D3DXMATRIX matT, matRX, matRY, matRZ, matS;
	D3DXMatrixTranslation(&matT, position_.x, position_.y, position_.z);
	//回転行列の作成
	//X
	D3DXMatrixRotationX(&matRX, D3DXToRadian(rotate_.x));
	//Y
	D3DXMatrixRotationY(&matRY, D3DXToRadian(rotate_.y));
	//Z
	D3DXMatrixRotationZ(&matRZ, D3DXToRadian(rotate_.z));
	//回転行列を合わせる
	D3DXMATRIX matR = matRX * matRY * matRZ;

	//拡大行列の作成
	D3DXMatrixScaling(&matS, scale_.x, scale_.y, scale_.z);
	//行列を合わせる	拡大 * 回転 * 位置
	localMatrix_ = matS * matR * matT;
	//ワールド行列
	if (pParent_ != nullptr)
	{
		worldMatrix_ = localMatrix_ * pParent_->worldMatrix_;
	}
	else
	{
		worldMatrix_ = localMatrix_;
	}
}

void IGameObject::PushBackChild(IGameObject* pObj)
{
	assert(pObj != nullptr);
	//子のリストの末尾に追加
	childList_.push_back(pObj);
}

bool IGameObject::IsExistInChildList(const std::string& objectName)
{
	for (auto it = childList_.begin(); it != childList_.end(); it++)
	{
		if ((*it)->GetName() == objectName)
		{
			return true;
		}
	}

	return false;
}

IGameObject * IGameObject::FindChildObject(const std::string & objectName)
{
	//子リストが空
	if (childList_.empty())
	{
		//見つからない
		return nullptr;
	}

	//見つけたオブジェクトを入れる
	IGameObject* pObj = nullptr;

	//子リストを全て調べる
	for (auto it = childList_.begin(); it != childList_.end(); it++)
	{
		//名前が一致しているか
		if ((*it)->GetName() == objectName)
		{
			//そのオブジェクトのポインタを返す
			pObj = *it;
		}
		else
		{
			//そのオブジェクトの子も調べる
			pObj = (*it)->FindChildObject(objectName);
		}

		//見つけている
		if (pObj != nullptr)
		{
			break;
		}
	}

	return pObj;
}

IGameObject * IGameObject::FindObject(const std::string & objectName)
{
	return GameObjectManager::FindChildObject(objectName);
}

void IGameObject::CollisionDraw()
{
	Direct3D::pDevice_->SetRenderState(D3DRS_FILLMODE, D3DFILL_WIREFRAME);	//ワイヤーフレーム
	Direct3D::pDevice_->SetRenderState(D3DRS_LIGHTING, FALSE);				//ライティングOFF
	Direct3D::pDevice_->SetTexture(0, nullptr);								//テクスチャなし

	if (pCollider_ != nullptr)
	{
		pCollider_->Draw(position_);
	}

	Direct3D::pDevice_->SetRenderState(D3DRS_FILLMODE, D3DFILL_SOLID);
	Direct3D::pDevice_->SetRenderState(D3DRS_LIGHTING, TRUE);
}

D3DXVECTOR3 IGameObject::CalcPushBackVec(IGameObject * pObject)
{
	//各情報の取得
	BoxCollider* pBoxCollider = (BoxCollider*)pCollider_;
	D3DXVECTOR3 characterDir = pObject->GetPosition() - position_;

	//衝突面の判定
	//キャラクターの方向を調べる
	D3DXMATRIX mat, matX, matY, matZ;
	D3DXMatrixRotationX(&matX, D3DXToRadian(-rotate_.x));
	D3DXMatrixRotationY(&matY, D3DXToRadian(-rotate_.y));
	D3DXMatrixRotationZ(&matZ, D3DXToRadian(-rotate_.z));
	mat = matX * matY * matZ;
	D3DXVECTOR3 characterDirOfBox;
	D3DXVec3TransformCoord(&characterDirOfBox, &characterDir, &mat);

	//射影線の長さを算出
	float len[COLLIDER_TYPE_MAX];
	ZeroMemory(&len, sizeof(len));
	//x軸
	D3DXVECTOR3 axisX = pBoxCollider->GetDirect(COLLIDER_AXIS_X);
	len[COLLIDER_AXIS_X] = ((BoxCollider*)pObject->GetCollider())->CalcProjectveLine(axisX);
	//z軸
	D3DXVECTOR3 axisZ = pBoxCollider->GetDirect(COLLIDER_AXIS_Z);
	len[COLLIDER_AXIS_Z] = ((BoxCollider*)pObject->GetCollider())->CalcProjectveLine(axisZ);
	//各軸ごとにめり込んでいる距離を算出
	float xDistSunk = fabs(characterDir.x) - len[COLLIDER_AXIS_X] - pBoxCollider->GetLen(COLLIDER_AXIS_X);
	float zDistSunk = fabs(characterDir.z) - len[COLLIDER_AXIS_Z] - pBoxCollider->GetLen(COLLIDER_AXIS_Z);

	//どの面に当たっているか求める
	int axisNum = 0;
	D3DXVECTOR3 hitFaceNormal = pBoxCollider->DetectHitSurface(axisNum, xDistSunk, NULL, zDistSunk, characterDirOfBox);

	//法線方向の距離を求める
	float dist = D3DXVec3Dot(&characterDir, &hitFaceNormal);

	//戻す長さを求める
	float backLength = len[axisNum] + pBoxCollider->GetLen(axisNum) - fabs(dist);
	
	D3DXVECTOR3 result = hitFaceNormal * backLength;

	return result;
}

void IGameObject::PassShaderInfo()
{
	//シェーダー
	//スクリーン座標に変換用行列を作成
	//ビュー行列を取得（変わらないので初期化で行ったほうがいい）
	D3DXMATRIX view;
	Direct3D::pDevice_->GetTransform(D3DTS_VIEW, &view);
	//プロジェクション行列を取得（変わらないので初期化で行ったほうがいい）
	D3DXMATRIX proj;
	Direct3D::pDevice_->GetTransform(D3DTS_PROJECTION, &proj);
	//各行列を合わせる
	D3DXMATRIX matWVP = worldMatrix_ * view * proj;
	//第一引数の名前の変数に第二引数の値を渡す
	pEffect_->SetMatrix("WVP", &matWVP);

	//オブジェクトに合わせて法線を変形
	D3DXMATRIX mat, matX, matY, matZ, scale;
	//拡大行列の逆行列
	D3DXMatrixScaling(&scale, scale_.x, scale_.y, scale_.z);
	D3DXMatrixInverse(&scale, nullptr, &scale);
	//回転行列
	D3DXMatrixRotationX(&matX, D3DXToRadian(rotate_.x));
	D3DXMatrixRotationY(&matY, D3DXToRadian(rotate_.y));
	D3DXMatrixRotationZ(&matZ, D3DXToRadian(rotate_.z));
	//各行列を合わせる
	mat = matX * matY * matZ * scale;
	//法線変形用の行列を渡す
	pEffect_->SetMatrix("RS", &mat);

	//ライトの方向
	D3DLIGHT9 lightState;
	//ライトの方向を取得
	Direct3D::pDevice_->GetLight(0, &lightState);
	//渡す
	pEffect_->SetVector("LIGHT_DIR", (D3DXVECTOR4*)&lightState.Direction);

	//視点（カメラ位置）を指定
	pEffect_->SetVector("CAMERA_POS", (D3DXVECTOR4*)&D3DXVECTOR3(0, 3, -7));
	//ワールド行列を渡す
	pEffect_->SetMatrix("W", &worldMatrix_);
}

bool IGameObject::RemoveCollisionCell()
{
	//すでに離脱しているなら処理終了
	if (!pCell_)
	{
		return false;
	}

	//自分が登録している空間に離脱することを通知
	if (!pCell_->OnRemove(this))
	{
		//離脱失敗
		return false;
	}

	//離脱処理
	//前後のオブジェクトをつなげる
	//前にオブジェクトがいる
	if (pPre_ != nullptr)
	{
		pPre_->pNext_ = pNext_;
	}
	//後ろにオブジェクトがいる
	if (pNext_ != nullptr)
	{
		pNext_->pPre_ = pPre_;
	}
	//前後のオブジェクト情報を初期化
	pPre_ = nullptr;
	pNext_ = nullptr;
	pCell_ = nullptr;

	//離脱成功
	return true;
}

bool IGameObject::RegistCollisionCell()
{
	//コライダーがついていないなら登録しない
	if (pCollider_ == nullptr)
	{
		return false;
	}

	//衝突空間に登録
	GameObjectManager::RegistTree(this);
}

D3DXVECTOR4 & IGameObject::GetRectXZ()
{
	return pCollider_->GetRectXZ();
}

void IGameObject::SetScale(D3DXVECTOR3& scale)
{
	scale_ = scale;
	
	//コライダーがついている
	if (pCollider_ != nullptr)
	{
		//箱型
		if (pCollider_->GetColliderType() == COLLIDER_TYPE_BOX)
		{
			//コライダーのスケールも変更
			((BoxCollider*)pCollider_)->ChangeScale(scale_);
		}
	}
}


void IGameObject::UpdateSub()
{
	//更新処理
 	Update();
	Transform();

	//コライダーがついているオブジェクトのみ空間に登録
	if (pCollider_ != nullptr)
	{
		//ツリーから離脱
		RemoveCollisionCell();
		//再登録
		GameObjectManager::RegistTree(this);
	}

	//子の更新
	for (auto it = childList_.begin(); it != childList_.end(); it++)
	{
		(*it)->UpdateSub();
	}

	for (auto it = childList_.begin(); it != childList_.end();)
	{
		if ((*it)->isDead_ == true)
		{
			(*it)->ReleaseSub();
			SAFE_DELETE(*it);
			//消した次をitに入れる
			it = childList_.erase(it);
		}
		else
		{
			it++;
		}
		
	}
}

void IGameObject::DrawSub()
{
	//描画処理
	Draw();

	//子の描画
	for (auto it = childList_.begin(); it != childList_.end(); it++)
	{
		(*it)->DrawSub();
	}
}

void IGameObject::ReleaseSub()
{
	//解放処理
	Release();
	//登録空間がある場合
	if (pCell_ != nullptr)
	{
		//空間から離脱
		RemoveCollisionCell();
	}

	//子の解放
	for (auto it = childList_.begin(); it != childList_.end(); it++)
	{
		(*it)->ReleaseSub();
		SAFE_DELETE(*it);
	}
}

void IGameObject::KillMe()
{
	isDead_ = true;
}

void IGameObject::SetBoxCollider(D3DXVECTOR3 basePos, D3DXVECTOR3 size, D3DXMATRIX worldMatrix)
{
	pCollider_ = new BoxCollider(this, basePos, size, worldMatrix);
	pCollider_->SetGameObject(this);
}

void IGameObject::SetPlaneCollider(D3DXVECTOR3 basePos, D3DXVECTOR3 size, D3DXMATRIX worldMatrix)
{
	pCollider_ = new PlaneCollider(this, basePos, size, worldMatrix);
	pCollider_->SetGameObject(this);
}

void IGameObject::Collision(IGameObject* targetObject)
{
	//めり込んだ長さ格納用
	float len = 0.0f;

	//衝突処理
	if (pCollider_ != nullptr && targetObject != this &&
		targetObject->pCollider_ != nullptr && 
		pCollider_->IsHit(targetObject->pCollider_, &len))
	{
		//当たった
		OnCollision(targetObject, len);
		Transform();
		isHit_ = true;
	}

	////対象オブジェクトの子との当たり判定
	//for (auto itr = targetObject->childList_.begin(); itr != targetObject->childList_.end(); itr++)
	//{
	//	Collision(*itr);
	//}	
}

IGameObject * IGameObject::GetChild(int ID)
{
	auto itr = childList_.begin();
	for (int i = 0; i < ID; i++)
	{
		itr++;
	}
	return *itr;
}

std::list<IGameObject*>* IGameObject::GetChildList()
{
	return &childList_;
}


