#pragma once

#include <d3dx9.h>


class IGameObject;

//あたり判定のタイプ
enum COLLIDER_TYPE
{
	COLLIDER_TYPE_BOX,		//箱型
	COLLIDER_TYPE_SPHERE,	//球体
	COLLIDER_TYPE_PLANE,	//平面
	COLLIDER_TYPE_MAX
};

//コライダーの軸
enum COLLIDER_AXIS
{
	COLLIDER_AXIS_X,		//x軸
	COLLIDER_AXIS_Y,		//y軸
	COLLIDER_AXIS_Z,		//z軸
	COLLIDER_AXIS_MAX
};


//あたり判定を管理するクラス
class Collider
{
	friend class BoxCollider;
	friend class SphereCollider;
	friend class PlaneCollider;

protected:
	IGameObject*	pGameObject_;	//この判定をつけたゲームオブジェクト
	COLLIDER_TYPE	type_;			//種類
	D3DXVECTOR3		center_;		//中心位置（ゲームオブジェクトの原点から見た位置）
	LPD3DXMESH		pMesh_;			//テスト表示用の枠

public:
	//コンストラクタ
	Collider();

	//デストラクタ
	virtual ~Collider();

	//接触判定（継承先のSphereColliderかBoxColliderでオーバーライド）
	//引数：target	相手の当たり判定, length めり込んだ長さの格納先
	//戻値：接触してればtrue
	virtual bool IsHit(Collider* target, float* length = nullptr) = 0;

	//箱型同士の衝突判定
	//引数：boxA １つ目の箱型判定, boxB ２つ目の箱型判定
	//戻値：接触していればtrue
	bool IsHitBoxVsBox(BoxCollider* boxA, BoxCollider* boxB);

	//箱型と球体の衝突判定
	//引数：box	箱型判定, sphere ２つ目の箱型判定
	//戻値：接触していればtrue
	bool IsHitBoxVsSphere(BoxCollider* box, SphereCollider* sphere);

	//球体同士の衝突判定
	//引数：circleA	１つ目の球体判定, circleB ２つ目の球体判定
	//戻値：接触していればtrue
	bool IsHitSphereVsSphere(SphereCollider* circleA, SphereCollider* circleB);

	//テスト表示用の枠を描画
	//引数：position	位置
	void Draw(D3DXVECTOR3 position);

	//ワールド座標を取得
	//戻り値：ワールド座標
	D3DXVECTOR3 GetCenterPos();

	//3次元OBB(直方体)と点(球体)の最短距離算出
	//引数：obb OBB判定, point 点(球体判定)
	float LenOBBToPoint(BoxCollider* obb, SphereCollider* point);

	//分離軸に投影された軸成分から投影線分長を算出
	//戻り値：投影線分の長さ
	FLOAT LenSegOnSeparateAxis(D3DXVECTOR3 *Sep, D3DXVECTOR3 *e1, D3DXVECTOR3 *e2, D3DXVECTOR3 *e3 = 0);
	
	//OBBvsOBBの衝突判定
	//引数：obb1 1つ目のOBB判定, obb2 2つ目のOBB判定
	//戻り値：衝突していればtrue
	bool IsHitOBBVsOBB(BoxCollider &obb1, BoxCollider &obb2);

	//OBBvs平面の衝突判定
	//戻り値：衝突していればtrue
	bool IsHitOBBVsPlane(BoxCollider *obb, PlaneCollider *plane, float *length = nullptr);

	//xz平面上で自身を囲む矩形を算出し、x,z座標の最小・最大を返す
	//戻り値：求めた矩形のx,z座標の最小・最大(xMin, zMax, xMax, zMin)
	virtual D3DXVECTOR4 GetRectXZ() { return D3DXVECTOR4(0, 0, 0, 0); }
	
	//アクセス関数
	void SetGameObject(IGameObject* gameObject) { pGameObject_ = gameObject; }
	int GetColliderType() { return type_; }
};

