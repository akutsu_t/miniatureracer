#include "GameObjectManager.h"
#include "IGameObject.h"
#include <list>
#include "Global.h"
#include "../CollisionTree.h"

#pragma comment(lib, "winmm.lib")

namespace
{
	//ルートオブジェクトのクラス
	class RootJob : public IGameObject 
	{
	public:
		RootJob() : IGameObject(nullptr, "RootJob") {}
		~RootJob() {}
		void Initialize() {}
		void Update() {}
		void Draw() {}
		void Release() {}
		void CollisionInList();
	};


	//ルートオブジェクト
	RootJob* pRootJob = nullptr;

	//線分4分木空間管理オブジェクト
	CollisionTree::CLiner4TreeManager<IGameObject>* pTreeManager = nullptr;

	//衝突判定リスト
	CollisionTree::CollisionList<IGameObject>* pColList;

	//解放処理
	//引数：obj	対象のオブジェクト
	void ReleaseSub(IGameObject* obj)
	{
		//まずは子オブジェクトを解放
		auto list = obj->GetChildList();
		for (auto it = list->begin(); it != list->end(); it++) 
		{
			ReleaseSub(*it);
		}

		//対象オブジェクトを解放
		obj->Release();
		SAFE_DELETE(obj);
	}

	//更新処理
	//引数：obj	対象のオブジェクト
	void UpdateSub(IGameObject* obj)
	{
		//対象オブジェクトの更新処理
		obj->Update();
		obj->Transform();

		//衝突フラグをfalseに
		obj->SetIsHit(false);

		//その子オブジェクトの更新処理
		auto list = obj->GetChildList();
		for (auto it = list->begin(); it != list->end();) 
		{
			//更新処理
			UpdateSub(*it);

			//コライダーがついているオブジェクトのみ空間に登録
			if ((*it)->GetCollider() != nullptr)
			{
				//ツリーから離脱
				(*it)->RemoveCollisionCell();
				//再登録
				GameObjectManager::RegistTree(*it);
			}

			//削除フラグが立ってたら削除
			if ((*it)->GetIsDead()) 
			{
				
				ReleaseSub(*it);
				it = list->erase(it);
			}
			else
			{
				it++;
			}
		}
	}


	//描画処理
	//引数：obj	対象のオブジェクト
	void DrawSub(IGameObject* obj)
	{
		//対象オブジェクトの描画
		obj->Draw();


//リリース時は削除
#ifdef _DEBUG
		//コリジョンの描画
		if (Direct3D::isDrawCollision_)
		{
			obj->CollisionDraw();
		}
#endif


		//その子オブジェクトの描画処理
		auto list = obj->GetChildList();
		for (auto it = list->begin(); it != list->end(); it++) 
		{
			DrawSub(*it);
		}
	}

	//衝突判定リストをもとに衝突判定
	void RootJob::CollisionInList()
	{
		//衝突判定リスト内のオブジェクト数を取得
		DWORD colNum = pTreeManager->GetAllCollisionList(&pColList);
		//衝突判定数を算出（2つずつ組になっている）
		colNum /= 2;
		//衝突判定リストのルートポインタを取得
		IGameObject** pRoot = pTreeManager->GetRoot();
		//算出した回数分の衝突判定
		for (DWORD i = 0; i < colNum; i++)
		{
			pRoot[i * 2]->Collision(pRoot[i * 2 + 1]);
			pRoot[i * 2 + 1]->Collision(pRoot[i * 2]);
		}
	}
}


//ゲームオブジェクトを管理する関数達
namespace GameObjectManager
{
	//初期化
	void Initialize()
	{
		//ルートオブジェクト作成
		pRootJob = new RootJob;

		//シーンマネージャー作成
		pRootJob->CreateGameObject<SceneManager>(pRootJob);

		//線分4分木空間管理オブジェクトを作成
		pTreeManager = new CollisionTree::CLiner4TreeManager<IGameObject>();
		//初期化
		pTreeManager->Init(6, -500, 500, 500, -500);
	}

	//更新
	void Update()
	{
		//ルートの更新
		UpdateSub(pRootJob);

		//衝突判定
		pRootJob->CollisionInList();
	}


	//描画
	void Draw()
	{
		//ルートの描画
		DrawSub(pRootJob);
	}


	//解放
	void Release()
	{
		//衝突リストの解放
		SAFE_DELETE(pColList);

		//空間管理オブジェクトの解放
		SAFE_DELETE(pTreeManager);

		//ルートの解放
		ReleaseSub(pRootJob);
	}


	//ルートの子供としてオブジェクトを追加
	//引数：追加するオブジェクト（実態はSceneManager）
	void PushBackChild(IGameObject * obj)
	{
		pRootJob->GetChildList()->push_back(obj);
	}


	//名前でオブジェクトを検索
	//引数：探したい名前
	//引数：見つけたオブジェクトのアドレス（見つからなければnullptr）
	IGameObject * FindChildObject(const std::string & name)
	{
		return pRootJob->FindChildObject(name);
	}

	//4分木ツリーにオブジェクトを登録
	//引数：pObject	登録したいオブジェクト
	//戻り値：登録成功ならtrue
	bool RegistTree(IGameObject * pObject)
	{
		//空間登録用に座標を取得
		D3DXVECTOR4 rectXZ = pObject->GetRectXZ();
		//ツリーに登録
		return pTreeManager->Regist(rectXZ.x, rectXZ.y, rectXZ.z, rectXZ.w, pObject);
	}

}
