#include "FbxParts.h"
#include "Fbx.h"

//コンストラクタ
FbxParts::FbxParts(LPD3DXEFFECT pEffect) :
	vertexCount_(0),
	polygonCount_(0),
	indexCount_(0),
	materialCount_(0),
	pPolygonCountOfMaterial_(nullptr),
	pEffect_(pEffect),
	pVertexList_(nullptr),
	vertexBuffer_(nullptr),
	pIndexBuffer_(nullptr),
	pMaterial_(nullptr),
	pTexture_(nullptr),
	pNormalMap_(nullptr),
	pSkinInfo_(nullptr),
	ppCluster_(nullptr),
	pBoneArray_(nullptr),
	pWeightArray_(nullptr),
	pVerDec_(nullptr)
{

}

//デストラクタ
FbxParts::~FbxParts()
{
	SAFE_RELEASE(pVerDec_);
	SAFE_DELETE_ARRAY(pBoneArray_);
	SAFE_DELETE_ARRAY(ppCluster_);

	if (pWeightArray_ != nullptr)
	{
		for (DWORD i = 0; i < (unsigned int)vertexCount_; i++)
		{
			SAFE_DELETE_ARRAY(pWeightArray_[i].pBoneIndex);
			SAFE_DELETE_ARRAY(pWeightArray_[i].pBoneWeight);
		}
		SAFE_DELETE_ARRAY(pWeightArray_);
	}

	for (int i = 0; i < materialCount_; i++)
	{
		SAFE_RELEASE(pIndexBuffer_[i]);
	}
	SAFE_DELETE_ARRAY(pIndexBuffer_);
	SAFE_DELETE_ARRAY(pTexture_);
	SAFE_DELETE_ARRAY(pNormalMap_);
	SAFE_DELETE_ARRAY(pMaterial_);

	SAFE_DELETE_ARRAY(pPolygonCountOfMaterial_);

	SAFE_RELEASE(vertexBuffer_);
	SAFE_DELETE_ARRAY(pVertexList_);

	for (unsigned int i = 0; i < childParts_.size(); i++)
	{
		SAFE_DELETE(childParts_[i]);
	}
	childParts_.clear();
}

//マテリアル（色や質感、テクスチャ）の情報をロード
void FbxParts::InitMaterial(FbxNode *pNode, Fbx* pFbx)
{
	//ノード情報のアドレスを入れておく
	pNode_ = pNode;

	//各データを入れる準備
	materialCount_ = pNode->GetMaterialCount();				//マテリアルの数を取得
	pMaterial_ = new D3DMATERIAL9[materialCount_];			//マテリアルを入れる配列作成
	pTexture_ = new LPDIRECT3DTEXTURE9[materialCount_];		//テクスチャを入れる配列作成
	pNormalMap_ = new LPDIRECT3DTEXTURE9[materialCount_];	//法線マップを入れる配列作成

	//マテリアル情報を1個ずつ確認していく
	for (int i = 0; i < materialCount_; i++)
	{
		//マテリアル
		FbxSurfacePhong* surface = (FbxSurfacePhong*)pNode->GetMaterial(i);
		FbxDouble3 diffuse = surface->Diffuse;
		FbxDouble3 ambient = surface->Ambient;
		//FbxDouble3 specular;

		ZeroMemory(&pMaterial_[i], sizeof(D3DMATERIAL9));

		//ポリゴンの色
		pMaterial_[i].Diffuse.r = (float)diffuse[0];
		pMaterial_[i].Diffuse.g = (float)diffuse[1];
		pMaterial_[i].Diffuse.b = (float)diffuse[2];
		pMaterial_[i].Diffuse.a = 1.0f;
		//pMaterial_[i].Diffuse.a = (float)diffuse[3];

		//環境光
		pMaterial_[i].Ambient.r = (float)ambient[0];
		pMaterial_[i].Ambient.g = (float)ambient[1];
		pMaterial_[i].Ambient.b = (float)ambient[2];
		pMaterial_[i].Ambient.a = 1.0f;
		//pMaterial_[i].Ambient.a = (float)diffuse[3];

		//フォンシェーダーの場合
		if (surface->GetClassId().Is(FbxSurfacePhong::ClassId))
		{
			//鏡面反射光
			//色
			FbxDouble3 specular = surface->Specular;
			pMaterial_[i].Specular.r = (float)specular[0];
			pMaterial_[i].Specular.g = (float)specular[1];
			pMaterial_[i].Specular.b = (float)specular[2];
			pMaterial_[i].Specular.a = 1.0f;
			//大きさ
			pMaterial_[i].Power = (float)surface->Shininess;

		}

		//普通のテクスチャをロード
		{
			//テクスチャ情報
			FbxProperty lProperty = pNode->GetMaterial(i)->FindProperty(FbxSurfaceMaterial::sDiffuse);
			FbxFileTexture* textureFile = lProperty.GetSrcObject<FbxFileTexture>(0);

			//テクスチャ使ってない場合
			if (textureFile == nullptr)
			{
				pTexture_[i] = nullptr;
			}

			//テクスチャ使ってる場合
			else
			{
				pTexture_[i] = pFbx->LoadTexture(textureFile->GetFileName());
			}
		}

		//法線マップをロード
		{
			//テクスチャ情報
			FbxProperty lProperty = pNode->GetMaterial(i)->FindProperty(FbxSurfaceMaterial::sBump);
			FbxFileTexture* textureFile = lProperty.GetSrcObject<FbxFileTexture>(0);

			//法線マップ使ってない場合
			if (textureFile == nullptr)
			{
				pNormalMap_[i] = nullptr;
			}

			//法線マップ使ってる場合
			else
			{
				pNormalMap_[i] = pFbx->LoadTexture(textureFile->GetFileName());
			}
		}
	}
}

//頂点バッファの作成
void FbxParts::InitVertexBuffer(FbxMesh * pMesh)
{
	//とりあえず専用のデータ型に入れる
	FbxVector4* pVertexPos = pMesh->GetControlPoints();

	//各情報を調べる
	vertexCount_ = pMesh->GetControlPointsCount();	//頂点数
	polygonCount_ = pMesh->GetPolygonCount();		//ポリゴン数
	indexCount_ = pMesh->GetPolygonVertexCount();	//インデックス数

															//頂点データを入れる配列作成
	pVertexList_ = new FbxParts::Vertex[vertexCount_];


	/////////////////////////頂点の位置/////////////////////////////////////

	//1個ずつ頂点の位置を入れていく
	for (int i = 0; vertexCount_ > i; i++)
	{
		pVertexList_[i].pos.x = (float)pVertexPos[i][0];
		pVertexList_[i].pos.y = (float)pVertexPos[i][1];
		pVertexList_[i].pos.z = (float)pVertexPos[i][2];
	}


	//ポリゴン1枚ずつ調べていく
	FbxLayerElementUV* leUV = pMesh->GetLayer(0)->GetUVs();
	for (int poly = 0; poly < polygonCount_; poly++)
	{
		//データの先頭をゲットして
		int startIndex = pMesh->GetPolygonVertexIndex(poly);

		//3頂点分
		for (int vertex = 0; vertex < 3; vertex++)
		{
			//int index = pMesh->GetPolygonVertices()[startIndex + vertex];
			int index = pMesh->GetPolygonVertex(poly, vertex);

			/////////////////////////頂点の法線/////////////////////////////////////
			FbxVector4 Normal;
			pMesh->GetPolygonVertexNormal(poly, vertex, Normal);	//ｉ番目のポリゴンの、ｊ番目の頂点の法線をゲット
			pVertexList_[index].normal = D3DXVECTOR3((float)Normal[0], (float)Normal[1], (float)Normal[2]);
			D3DXVec3Normalize(&pVertexList_[index].normal, &pVertexList_[index].normal);


			/////////////////////////頂点のUV///////////////////
			// インデックスバッファからインデックスを取得する
			//int lUVIndex = leUV->GetIndexArray().GetAt(poly * 3 + vertex);

			//// 取得したインデックスから UV を取得する
			//FbxVector2 lVec2 = leUV->GetDirectArray().GetAt(lUVIndex);

			
			{
				//uv情報を取得
				FbxVector2 lVec2 = leUV->GetDirectArray().GetAt(index);
				//取得した情報をリストに格納
				pVertexList_[index].uv = D3DXVECTOR2((float)lVec2.mData[0], (float)(1.0 - lVec2.mData[1]));
			}


			// UV値セット
			//pVertexList_[index].uv.x = (float)lVec2.mData[0];
			//pVertexList_[index].uv.y = 1.0f - (float)lVec2.mData[1];	//Yは逆になってる

		}
	}

	//接線(tangent)を算出
	for (int i = 0; i < polygonCount_; i++)
	{
		int startIndex = pMesh->GetPolygonVertexIndex(i);

		D3DXVECTOR3 tangent = CalcTangent(
			&pVertexList_[pMesh->GetPolygonVertices()[startIndex + 0]].pos,
			&pVertexList_[pMesh->GetPolygonVertices()[startIndex + 1]].pos,
			&pVertexList_[pMesh->GetPolygonVertices()[startIndex + 2]].pos,
			&pVertexList_[pMesh->GetPolygonVertices()[startIndex + 0]].uv,
			&pVertexList_[pMesh->GetPolygonVertices()[startIndex + 1]].uv,
			&pVertexList_[pMesh->GetPolygonVertices()[startIndex + 2]].uv);


		for (int j = 0; j < 3; j++)
		{
			int index = pMesh->GetPolygonVertices()[startIndex + j];
			pVertexList_[index].tangent = tangent;
		}
	}

	/////////////////////////////////////////////////
	//  頂点バッファを作成！！
	/////////////////////////////////////////////////

	//頂点情報を設定
	D3DVERTEXELEMENT9 vertexElement[] = {
		{0,  0, D3DDECLTYPE_FLOAT3, D3DDECLMETHOD_DEFAULT, D3DDECLUSAGE_POSITION, 0},	//位置(4bite * 3)
		{0, 12, D3DDECLTYPE_FLOAT3, D3DDECLMETHOD_DEFAULT, D3DDECLUSAGE_NORMAL  , 0},	//法線(4bite * 3)
		{0, 24, D3DDECLTYPE_FLOAT2, D3DDECLMETHOD_DEFAULT, D3DDECLUSAGE_TEXCOORD, 0},	//uv(4bite * 2)
		{0, 32, D3DDECLTYPE_FLOAT3, D3DDECLMETHOD_DEFAULT, D3DDECLUSAGE_TANGENT , 0},	//接線(4bite * 3)
		D3DDECL_END()
	};
	Direct3D::pDevice_->CreateVertexDeclaration(vertexElement, &pVerDec_);

	//まずカラのバッファ作って
	Direct3D::pDevice_->CreateVertexBuffer(sizeof(FbxParts::Vertex)*vertexCount_, 0, 0, D3DPOOL_MANAGED, &vertexBuffer_, 0);

	//ロックすればいじれる
	FbxParts::Vertex *vCopy;
	vertexBuffer_->Lock(0, 0, (void**)&vCopy, 0);

	//カラのバッファにデータコピー
	memcpy(vCopy, pVertexList_, sizeof(FbxParts::Vertex)*vertexCount_);

	//終わったらアンロック
	vertexBuffer_->Unlock();



	/////////////////////////////////////////////////
	//	頂点バッファ完成！！！！
	/////////////////////////////////////////////////
}



//インデックスバッファの作成
void FbxParts::InitIndexBuffer(FbxMesh * pMesh)
{
	/////////////////////////////////////////////////
	//	インデックスバッファ作成！！
	/////////////////////////////////////////////////

	//インデックス数分の配列を作成
	pIndexBuffer_ = new IDirect3DIndexBuffer9*[materialCount_];

	//マテリアルごとのポリゴン数を入れるための配列作成
	pPolygonCountOfMaterial_ = new int[materialCount_];

	//マテリアルごとにインデックスバッファ作る
	for (int i = 0; i < materialCount_; i++)
	{
		//とりあえずインデックス番号入れる配列
		int* indexList = new int[indexCount_];

		//ポリゴンごとに
		int count = 0;
		for (int polygon = 0; polygon < polygonCount_; polygon++)
		{
			//そのポリゴンのマテリアルの番号をゲット
			int materialID = pMesh->GetLayer(0)->GetMaterials()->GetIndexArray().GetAt(polygon);

			//今処理中のマテリアルだった
			if (materialID == i)
			{
				//3つ分
				for (int vertex = 0; vertex < 3; vertex++)
				{
					indexList[count++] = pMesh->GetPolygonVertex(polygon, vertex);
				}
			}
		}

		//マテリアルごとのポリゴン数　＝　頂点数÷３
		pPolygonCountOfMaterial_[i] = count / 3;

		//インデックスバッファ作成！！
		Direct3D::pDevice_->CreateIndexBuffer(sizeof(int)* indexCount_, 0, D3DFMT_INDEX32, D3DPOOL_MANAGED, &pIndexBuffer_[i], 0);
		DWORD *iCopy;
		pIndexBuffer_[i]->Lock(0, 0, (void**)&iCopy, 0);
		memcpy(iCopy, indexList, sizeof(int)* indexCount_);
		pIndexBuffer_[i]->Unlock();
		delete[] indexList;
	}
}


//アニメーションデータの準備
void FbxParts::InitAnimation(FbxMesh * pMesh)
{
	// デフォーマ情報（ボーンとモデルの関連付け）の取得
	FbxDeformer *   pDeformer = pMesh->GetDeformer(0);
	if (pDeformer == nullptr)
	{
		//ボーン情報なし
		return;
	}


	// デフォーマ情報からスキンメッシュ情報を取得
	pSkinInfo_ = (FbxSkin *)pDeformer;

	// 頂点からポリゴンを逆引きするための情報を作成する
	struct  POLY_INDEX
	{
		int *   polyIndex;      // ポリゴンの番号
		int *   vertexIndex;    // 頂点の番号
		int     numRef;         // 頂点を共有するポリゴンの数
	};

	POLY_INDEX * polyTable = new POLY_INDEX[vertexCount_];
	for (DWORD i = 0; i < (unsigned int)vertexCount_; i++)
	{
		// 三角形ポリゴンに合わせて、頂点とポリゴンの関連情報を構築する
		// 総頂点数＝ポリゴン数×３頂点
		polyTable[i].polyIndex = new int[polygonCount_ * 3];
		polyTable[i].vertexIndex = new int[polygonCount_ * 3];
		polyTable[i].numRef = 0;
		ZeroMemory(polyTable[i].polyIndex, sizeof(int)* polygonCount_ * 3);
		ZeroMemory(polyTable[i].vertexIndex, sizeof(int)* polygonCount_ * 3);

		// ポリゴン間で共有する頂点を列挙する
		for (DWORD k = 0; k < (unsigned int)polygonCount_; k++)
		{
			for (int m = 0; m < 3; m++)
			{
				if (pMesh->GetPolygonVertex(k, m) == i)
				{
					polyTable[i].polyIndex[polyTable[i].numRef] = k;
					polyTable[i].vertexIndex[polyTable[i].numRef] = m;
					polyTable[i].numRef++;
				}
			}
		}
	}

	// ボーン情報を取得する
	numBone_ = pSkinInfo_->GetClusterCount();
	ppCluster_ = new FbxCluster*[numBone_];
	for (int i = 0; i < numBone_; i++)
	{
		ppCluster_[i] = pSkinInfo_->GetCluster(i);
	}

	// ボーンの数に合わせてウェイト情報を準備する
	pWeightArray_ = new FbxParts::Weight[vertexCount_];
	for (DWORD i = 0; i < (unsigned int)vertexCount_; i++)
	{
		pWeightArray_[i].posOrigin = pVertexList_[i].pos;
		pWeightArray_[i].normalOrigin = pVertexList_[i].normal;
		pWeightArray_[i].pBoneIndex = new int[numBone_];
		pWeightArray_[i].pBoneWeight = new float[numBone_];
		for (int j = 0; j < numBone_; j++)
		{
			pWeightArray_[i].pBoneIndex[j] = -1;
			pWeightArray_[i].pBoneWeight[j] = 0.0f;
		}
	}




	// それぞれのボーンに影響を受ける頂点を調べる
	// そこから逆に、頂点ベースでボーンインデックス・重みを整頓する
	for (int i = 0; i < numBone_; i++)
	{
		int numIndex = ppCluster_[i]->GetControlPointIndicesCount();   //このボーンに影響を受ける頂点数
		int * piIndex = ppCluster_[i]->GetControlPointIndices();       //ボーン/ウェイト情報の番号
		double * pdWeight = ppCluster_[i]->GetControlPointWeights();     //頂点ごとのウェイト情報

																				 //頂点側からインデックスをたどって、頂点サイドで整理する
		for (int k = 0; k < numIndex; k++)
		{
			// 頂点に関連付けられたウェイト情報がボーン５本以上の場合は、重みの大きい順に４本に絞る
			for (int m = 0; m < 4; m++)
			{
				if (m >= numBone_)
					break;

				if (pdWeight[k] > pWeightArray_[piIndex[k]].pBoneWeight[m])
				{
					for (int n = numBone_ - 1; n > m; n--)
					{
						pWeightArray_[piIndex[k]].pBoneIndex[n] = pWeightArray_[piIndex[k]].pBoneIndex[n - 1];
						pWeightArray_[piIndex[k]].pBoneWeight[n] = pWeightArray_[piIndex[k]].pBoneWeight[n - 1];
					}
					pWeightArray_[piIndex[k]].pBoneIndex[m] = i;
					pWeightArray_[piIndex[k]].pBoneWeight[m] = (float)pdWeight[k];
					break;
				}
			}

		}
	}

	//ボーンを生成
	pBoneArray_ = new FbxParts::Bone[numBone_];
	for (int i = 0; i < numBone_; i++)
	{
		// ボーンのデフォルト位置を取得する
		FbxAMatrix  matrix;
		ppCluster_[i]->GetTransformLinkMatrix(matrix);

		// 行列コピー（Fbx形式からDirectXへの変換）
		for (DWORD x = 0; x < 4; x++)
		{
			for (DWORD y = 0; y < 4; y++)
			{
				pBoneArray_[i].bindPose(x, y) = (float)matrix.Get(x, y);
			}
		}
	}

	// 一時的なメモリ領域を解放する
	for (DWORD i = 0; i < (unsigned int)vertexCount_; i++)
	{
		SAFE_DELETE_ARRAY(polyTable[i].polyIndex);
		SAFE_DELETE_ARRAY(polyTable[i].vertexIndex);
	}
	SAFE_DELETE_ARRAY(polyTable);
}


//ボーン有りのモデルを描画
void FbxParts::DrawSkinAnime(D3DXMATRIX &matrix, FbxTime time)
{
	// ボーンごとの現在の行列を取得する
	for (int i = 0; i < numBone_; i++)
	{
		FbxAnimEvaluator * evaluator = ppCluster_[i]->GetLink()->GetScene()->GetAnimationEvaluator();
		FbxMatrix mCurrentOrentation = evaluator->GetNodeGlobalTransform(ppCluster_[i]->GetLink(), time);

		// 行列コピー（Fbx形式からDirectXへの変換）
		for (DWORD x = 0; x < 4; x++)
		{
			for (DWORD y = 0; y < 4; y++)
			{
				pBoneArray_[i].newPose(x, y) = (float)mCurrentOrentation.Get(x, y);
			}
		}

		// オフセット時のポーズの差分を計算する
		D3DXMatrixInverse(&pBoneArray_[i].diffPose, nullptr, &pBoneArray_[i].bindPose);
		pBoneArray_[i].diffPose *= pBoneArray_[i].newPose;
	}

	// 各ボーンに対応した頂点の変形制御
	for (DWORD i = 0; i < (unsigned int)vertexCount_; i++)
	{
		// 各頂点ごとに、「影響するボーン×ウェイト値」を反映させた関節行列を作成する
		D3DXMATRIX  matrix;
		ZeroMemory(&matrix, sizeof(matrix));
		for (int m = 0; m < numBone_; m++)
		{
			if (pWeightArray_[i].pBoneIndex[m] < 0)
			{
				break;
			}
			matrix += pBoneArray_[pWeightArray_[i].pBoneIndex[m]].diffPose * pWeightArray_[i].pBoneWeight[m];

		}

		// 作成された関節行列を使って、頂点を変形する
		D3DXVECTOR3 Pos = pWeightArray_[i].posOrigin;
		D3DXVECTOR3 Normal = pWeightArray_[i].normalOrigin;
		D3DXVec3TransformCoord(&pVertexList_[i].pos, &Pos, &matrix);
		D3DXVec3TransformCoord(&pVertexList_[i].normal, &Normal, &matrix);

	}

	// 頂点バッファをロックして、変形させた後の頂点情報で上書きする
	FbxParts::Vertex * pv;
	if (vertexBuffer_ != nullptr && SUCCEEDED(vertexBuffer_->Lock(0, 0, (void**)&pv, 0)))
	{
		memcpy(pv, pVertexList_, sizeof(FbxParts::Vertex)* vertexCount_);
		vertexBuffer_->Unlock();
	}

	Draw(matrix);
}


//ボーン無しのモデルを描画
void FbxParts::DrawMeshAnime(D3DXMATRIX &matrix, FbxTime time, FbxScene *scene)
{
	// その瞬間の自分の姿勢行列を得る
	FbxAnimEvaluator *evaluator = scene->GetAnimationEvaluator();
	FbxMatrix mCurrentOrentation = evaluator->GetNodeGlobalTransform(pNode_, time);

	// Fbx形式の行列からDirectX形式の行列へのコピー（4×4の行列）
	for (DWORD x = 0; x < 4; x++)
	{
		for (DWORD y = 0; y < 4; y++)
		{
			localMatrix_(x, y) = (float)mCurrentOrentation.Get(x, y);
		}
	}

	Draw(matrix);
}


//描画（実際に描画しているのはここ）
void FbxParts::Draw(D3DXMATRIX &matrix)
{
	//頂点バッファの設定
	Direct3D::pDevice_->SetStreamSource(0, vertexBuffer_, 0, sizeof(FbxParts::Vertex));

	//頂点ストリームを指定
	//Direct3D::pDevice_->SetFVF(D3DFVF_XYZ | D3DFVF_NORMAL | D3DFVF_TEX1);
	Direct3D::pDevice_->SetVertexDeclaration(pVerDec_);

	//ワールド行列をセット（パーツごとの行列を先にかける）
	Direct3D::pDevice_->SetTransform(D3DTS_WORLD, &matrix);

	//マテリアルごとに描画
	for (int i = 0; i < materialCount_; i++)
	{
		Direct3D::pDevice_->SetIndices(pIndexBuffer_[i]);
		Direct3D::pDevice_->DrawIndexedPrimitive(D3DPT_TRIANGLELIST, 0, i * 0, vertexCount_, 0, pPolygonCountOfMaterial_[i]);

		//シェーダー使用
		if (pEffect_)
		{
			//シェーダーに色を渡す
			//拡散反射光
			pEffect_->SetVector("DIFFUSE_COLOR", &D3DXVECTOR4(pMaterial_[i].Diffuse.r, pMaterial_[i].Diffuse.g,
				pMaterial_[i].Diffuse.b, pMaterial_[i].Diffuse.a));
			//環境光
			pEffect_->SetVector("AMBIENT_COLOR", &D3DXVECTOR4(pMaterial_[i].Ambient.r, pMaterial_[i].Ambient.g,
				pMaterial_[i].Ambient.b, pMaterial_[i].Ambient.a));
			//拡散反射光（色）
			pEffect_->SetVector("SPECULAR_COLOR", &D3DXVECTOR4(pMaterial_[i].Specular.r, pMaterial_[i].Specular.g,
				pMaterial_[i].Specular.b, pMaterial_[i].Specular.a));
			//拡散反射光（大きさ）
			pEffect_->SetFloat("SPECULAR_POWER", pMaterial_[i].Power);
			//テクスチャが貼られているかどうか
			pEffect_->SetBool("EXIST_TEXTURE", pTexture_[i] != NULL);
			//テクスチャを渡す
			pEffect_->SetTexture("TEXTURE", pTexture_[i]);
			//法線マップを渡す
			pEffect_->SetTexture("NORMAL_MAP", pNormalMap_[i]);
		}
		//未使用
		else
		{
			Direct3D::pDevice_->SetTexture(0, pTexture_[i]);
			Direct3D::pDevice_->SetMaterial(&pMaterial_[i]);
		}
	}
}

//接線(tangent)を求める
D3DXVECTOR3 FbxParts::CalcTangent(D3DXVECTOR3 * pos0, D3DXVECTOR3 * pos1, D3DXVECTOR3 * pos2, D3DXVECTOR2 * uv0, D3DXVECTOR2 * uv1, D3DXVECTOR2 * uv2)
{
	D3DXVECTOR3 cp0[3] = {
		D3DXVECTOR3(pos0->x, uv0->x, uv0->y),
		D3DXVECTOR3(pos0->y, uv0->x, uv0->y),
		D3DXVECTOR3(pos0->z, uv0->x, uv0->y)
	};

	D3DXVECTOR3 cp1[3] = {
		D3DXVECTOR3(pos1->x, uv1->x, uv1->y),
		D3DXVECTOR3(pos1->y, uv1->x, uv1->y),
		D3DXVECTOR3(pos1->z, uv1->x, uv1->y)
	};

	D3DXVECTOR3 cp2[3] = {
		D3DXVECTOR3(pos2->x, uv2->x, uv2->y),
		D3DXVECTOR3(pos2->y, uv2->x, uv2->y),
		D3DXVECTOR3(pos2->z, uv2->x, uv2->y)
	};

	float f[3];
	for (int i = 0; i < 3; i++)
	{
		D3DXVECTOR3 v1 = cp1[i] - cp0[i];
		D3DXVECTOR3 v2 = cp2[i] - cp1[i];
		D3DXVECTOR3 cross;
		D3DXVec3Cross(&cross, &v1, &v2);

		f[i] = -cross.y / cross.x;
	}

	D3DXVECTOR3 tangent;
	memcpy(tangent, f, sizeof(float) * 3);
	D3DXVec3Normalize(&tangent, &tangent);
	return tangent;
}

//レイキャスト（レイを飛ばして当たり判定）
void FbxParts::RayCast(RayCastData * data)
{
	data->hit = FALSE;

	//頂点バッファをロック
	Vertex *vCopy;
	vertexBuffer_->Lock(0, 0, (void**)&vCopy, 0);

	//マテリアル毎
	for (DWORD i = 0; i < (unsigned int)materialCount_; i++)
	{
		//インデックスバッファをロック
		DWORD *iCopy;
		pIndexBuffer_[i]->Lock(0, 0, (void**)&iCopy, 0);

		//そのマテリアルのポリゴン毎
		for (DWORD j = 0; j < (unsigned int)pPolygonCountOfMaterial_[i]; j++)
		{
			//3頂点
			D3DXVECTOR3 ver[3];
			ver[0] = vCopy[iCopy[j * 3 + 0]].pos;
			ver[1] = vCopy[iCopy[j * 3 + 1]].pos;
			ver[2] = vCopy[iCopy[j * 3 + 2]].pos;

			BOOL  hit;
			float dist;

			hit = D3DXIntersectTri(&ver[0], &ver[1], &ver[2],
				&data->start, &data->dir, NULL, NULL, &dist);

			if (hit && dist < data->dist)
			{
				//法線を求める
				D3DXVECTOR3 vec1 = ver[0] - ver[2];
				D3DXVECTOR3 vec2 = ver[1] - ver[2];
				D3DXVec3Cross(&data->normal, &vec1, &vec2);
				
				//正規化
				D3DXVec3Normalize(&data->normal, &data->normal);

				data->hit = TRUE;
				data->dist = dist;
			}
		}

		//インデックスバッファ使用終了
		pIndexBuffer_[i]->Unlock();
	}

	//頂点バッファ使用終了
	vertexBuffer_->Unlock();
}

//テクスチャ再設定
void FbxParts::SetTexture(LPDIRECT3DTEXTURE9 pTexture)
{
	for (int i = 0; i < materialCount_; i++)
	{
		pTexture_[i] = pTexture;
	}
}

//任意のボーンの位置を取得
bool FbxParts::GetBonePosition(std::string boneName, D3DXVECTOR3* position)
{
	for (int i = 0; i < numBone_; i++)
	{
		if (boneName == ppCluster_[i]->GetLink()->GetName())
		{
			FbxAMatrix  matrix;
			ppCluster_[i]->GetTransformLinkMatrix(matrix);

			position->x = (float)matrix[3][0];
			position->y = (float)matrix[3][1];
			position->z = (float)matrix[3][2];

			return true;
		}

	}

	return false;
}

//任意のボーンのワールド行列を取得
bool FbxParts::GetBoneMatrix(std::string boneName, D3DXMATRIX * matrix)
{
	for (int i = 0; i < numBone_; i++)
	{
		if (boneName == ppCluster_[i]->GetLink()->GetName())
		{
			FbxAMatrix  boneMatrix;
			ppCluster_[i]->GetTransformLinkMatrix(boneMatrix);

			//行列をコピー
			for (int x = 0; x < 4; x++)
			{
				for (int y = 0; y < 4; y++)
				{
					(*matrix)(x, y) = (float)boneMatrix.Get(x, y);
				}
			}
			return true;
		}

	}

	return false;
}

//任意のボーンの角度を取得
bool FbxParts::GetBoneRotate(std::string boneName, D3DXVECTOR3 * rotate)
{
	for (int i = 0; i < numBone_; i++)
	{
		if (boneName == ppCluster_[i]->GetLink()->GetName())
		{
			fbxsdk::FbxDouble3 localRotate = ppCluster_[i]->GetLink()->LclRotation.Get();
			rotate->x = (float)localRotate[0];
			rotate->y = (float)localRotate[1];
			rotate->z = (float)localRotate[2];
			return true;
		}

	}

	return false;
}

//任意のボーンの拡大率を取得
bool FbxParts::GetBoneScale(std::string boneName, D3DXVECTOR3 * scale)
{
	for (int i = 0; i < numBone_; i++)
	{
		if (boneName == ppCluster_[i]->GetLink()->GetName())
		{
			fbxsdk::FbxDouble3 localScale = ppCluster_[i]->GetLink()->LclScaling.Get();
			scale->x = (float)localScale[0];
			scale->y = (float)localScale[1];
			scale->z = (float)localScale[2];
			return true;
		}

	}

	return false;
}
