#include "PlaneCollider.h"
#include "IGameObject.h"
#include "Direct3D.h"


//コンストラクタ（当たり判定の作成）
//引数：basePos	当たり判定の中心位置（ゲームオブジェクトの原点から見た位置）
//引数：size	当たり判定のサイズ
PlaneCollider::PlaneCollider(IGameObject* pParent, D3DXVECTOR3 basePos, D3DXVECTOR3 size, D3DXMATRIX worldMatrix)
{
	center_ = basePos;
	size_ = size;

	direction_[COLLIDER_AXIS_X] = D3DXVECTOR3(worldMatrix._11, worldMatrix._12, worldMatrix._13);
	direction_[COLLIDER_AXIS_Y] = D3DXVECTOR3(worldMatrix._21, worldMatrix._22, worldMatrix._23);
	direction_[COLLIDER_AXIS_Z] = D3DXVECTOR3(worldMatrix._31, worldMatrix._32, worldMatrix._33);
	type_ = COLLIDER_TYPE_PLANE;

	//リリース時は判定枠は表示しない
#ifdef _DEBUG
	//テスト表示用判定枠
	D3DXCreateBox(Direct3D::pDevice_, size.x, size.y, size.z, &pMesh_, 0);
#endif
}

//x,y,z軸の長さを渡す
float PlaneCollider::GetLen(int axisNum)
{
	switch (axisNum)
	{
	case COLLIDER_AXIS_X:
		return size_.x * 0.5f;
		break;
	case COLLIDER_AXIS_Y:
		return size_.y * 0.5f;
		break;
	case COLLIDER_AXIS_Z:
		return size_.z * 0.5f;
		break;
	}

	return 0.0f;
}

//x,y,z軸の方向ベクトルを渡す
D3DXVECTOR3 PlaneCollider::GetDirect(int axisNum)
{
	IGameObject* pObj = (IGameObject*)pGameObject_;
	switch (axisNum)
	{
	case COLLIDER_AXIS_X:
		direction_[COLLIDER_AXIS_X] = D3DXVECTOR3(pObj->GetWorldMatrix()._11, pObj->GetWorldMatrix()._12, pObj->GetWorldMatrix()._13);
		break;
	case COLLIDER_AXIS_Y:
		direction_[COLLIDER_AXIS_Y] = D3DXVECTOR3(pObj->GetWorldMatrix()._21, pObj->GetWorldMatrix()._22, pObj->GetWorldMatrix()._23);
		break;
	case COLLIDER_AXIS_Z:
		direction_[COLLIDER_AXIS_Z] = D3DXVECTOR3(pObj->GetWorldMatrix()._31, pObj->GetWorldMatrix()._32, pObj->GetWorldMatrix()._33);
		break;
	}

	D3DXVECTOR3 dirNormal;
	D3DXVec3Normalize(&dirNormal, &direction_[axisNum]);
	return dirNormal;
}

//接触判定
//引数：target	相手の当たり判定
//戻値：接触してればtrue
bool PlaneCollider::IsHit(Collider* target, float* length)
{
	switch (target->type_)
	{
	case COLLIDER_TYPE_BOX:
		return IsHitOBBVsPlane((BoxCollider*)target, this);
		break;

	case COLLIDER_TYPE_SPHERE:
		break;

	case COLLIDER_TYPE_PLANE:
		break;

	default:
		break;
	}
}

