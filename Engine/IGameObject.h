#pragma once
#include <d3dx9.h>
#include <list>
#include <string>

//前方宣言
class Collider;					//コライダークラス
namespace CollisionTree {
	template<class T>
	class CollisionCell;			//衝突空間クラス
}


class IGameObject
{
protected:
	IGameObject* pParent_;					//親
	std::list<IGameObject*> childList_;		//子のリスト
	std::string name_;						//オブジェクトの名前

	D3DXVECTOR3 position_;					//位置
	D3DXVECTOR3 rotate_;					//向き
	D3DXVECTOR3 scale_;						//拡大率

	//D3DXVECTOR3 normal_;					//反射ベクトル計算用の法線

	D3DXMATRIX localMatrix_;				//オブジェクトの行列（親から見た）
	D3DXMATRIX worldMatrix_;				//親の影響を受けた行列

	bool isDead_;							//死亡フラグ
	bool isHit_;							//衝突フラグ

	Collider* pCollider_;					//コライダー

	int hModel_;							//モデル番号
	LPD3DXEFFECT pEffect_;					//エフェクト


public:
	IGameObject* pPre_;									//前の登録オブジェクト
	IGameObject* pNext_;								//次の登録オブジェクト
	int id_;											//識別ID
	CollisionTree::CollisionCell<IGameObject>* pCell_;	//登録空間

	//コンストラクタ
	IGameObject();
	//引数:	parent	親
	IGameObject(IGameObject* parent);
	//引数:	parent	親		name	オブジェクト名
	IGameObject(IGameObject* parent, const std::string& name, int id = 0);

	//仮想デストラクタ
	virtual ~IGameObject();

	//変形
	void Transform();

	//初期化
	virtual void Initialize() = 0;

	//更新
	virtual void Update() = 0;

	//描画
	virtual void Draw() = 0;

	//解放
	virtual void Release() = 0;

	//何かと衝突した場合
	//引数：pTarget	衝突した相手, length めり込んだ長さ
	virtual void OnCollision(IGameObject* pTarget, float length) {};


	//子と自分の更新を呼ぶ
	void UpdateSub();

	//子と自分の描画を呼ぶ
	virtual void DrawSub();

	//子と自分の解放を呼ぶ
	void ReleaseSub();

	//自分を削除する
	void KillMe();

	//球体コライダーを設定
	//引数：center	中心位置	radius	半径
	void SetSphereCollider(D3DXVECTOR3& center, float radius);
	
	//箱型コライダーを設定
	//引数：center 中心位置, size サイズ, worldMatrix ワールド行列
	void SetBoxCollider(D3DXVECTOR3 basePos, D3DXVECTOR3 size, D3DXMATRIX worldMatrix);
	
	//平面コライダーを設定
	//引数：center 中心位置, size サイズ, worldMatrix ワールド行列
	void SetPlaneCollider(D3DXVECTOR3 basePos, D3DXVECTOR3 size, D3DXMATRIX worldMatrix);

	//対象オブジェクト以下の衝突処理
	//引数：targetObject	対象とするオブジェクト
	void Collision(IGameObject* targetObject);

	//オブジェクト生成
	//引数:	parent	親
	template<class T>
	T* CreateGameObject(IGameObject* parent)
	{
		//引数のparentを親としてオブジェクトを生成
		T* p = new T(parent);
		//parentに自分を子として登録
		parent->PushBackChild(p);
		//初期化
		p->Initialize();
		//コライダーがついている場合のみ空間に登録
		if (p->GetCollider() != nullptr)
		{
			p->RegistCollisionCell();
		}

		return p;
	}

	//子のリストに追加
	//引数:	pObj	追加するオブジェクト
	void PushBackChild(IGameObject* pObj);

	//子のリストに存在するか
	bool IsExistInChildList(const std::string& objectName);

	//子に指定された名前のオブジェクトが存在すればそのポインタを返す
	//引数：objectName 探したいオブジェクトの名前
	//戻り値：見つけたオブジェクトのポインタ（見つからない場合はnullptr）
	IGameObject* FindChildObject(const std::string& objectName);

	//オブジェクトを名前で検索
	//引数：objectName 探したいオブジェクトの名前
	//戻り値：見つけたオブジェクトのポインタ（見つからない場合はnullptr）
	IGameObject* FindObject(const std::string& objectName);

	//テスト用の衝突判定枠を表示
	void CollisionDraw();

	//めり込み分の押し戻しベクトルを算出
	//引数：pObject めり込んでいるオブジェクト
	D3DXVECTOR3 CalcPushBackVec(IGameObject* pObject);

	//位置を加える
	virtual void AddPosition(D3DXVECTOR3& addPosition) { position_ += addPosition; }

	//シェーダーに必要な情報を渡す
	void PassShaderInfo();

	//衝突空間から離脱
	//戻り値：離脱成功ならtrue、失敗ならfalse
	bool RemoveCollisionCell();

	//衝突空間に登録
	//戻り値：登録成功ならtrue、失敗ならfalse
	bool RegistCollisionCell();

	//オブジェクトを囲む短径のxz座標最大・最小値を取得
	D3DXVECTOR4& GetRectXZ();

	//セッター
	virtual void SetPosition(D3DXVECTOR3& position) { position_ = position; }
	virtual void SetPositionX(float x) { position_.x = x; }
	virtual void SetPositionY(float y) { position_.y = y; }
	virtual void SetPositionZ(float z) { position_.z = z; }
	void SetRotate(D3DXVECTOR3& rotate) { rotate_ = rotate; }
	void SetScale(D3DXVECTOR3& scale);
	void SetIsHit(bool flag) { isHit_ = flag; }

	//ゲッター
	D3DXVECTOR3& GetPosition() { return position_; }
	D3DXVECTOR3& GetRotate() { return rotate_; }
	D3DXVECTOR3& GetScale() { return scale_; }
	std::string GetName() { return name_; }
	IGameObject* GetChild(int ID);
	std::list<IGameObject*>* GetChildList();
	IGameObject* GetParent() {return pParent_; }
	D3DXMATRIX& GetLocalMatrix() { return localMatrix_; }
	D3DXMATRIX& GetWorldMatrix() { return worldMatrix_; }

	//D3DXVECTOR3& GetNormal() { return normal_; }
	int GetModelHandle() { return hModel_; }
	bool GetIsDead() { return isDead_; }
	Collider* GetCollider() { return pCollider_; }
};

