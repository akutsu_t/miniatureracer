#pragma once
#include "Collider.h"

class PlaneCollider : public Collider
{
	friend class Collider;

	//判定サイズ（幅、高さ、奥行き）
	D3DXVECTOR3 size_;

	//箱のx,y,z軸の方向ベクトル
	D3DXVECTOR3 direction_[3];

public:
	//コンストラクタ（当たり判定の作成）
	//引数：basePos	当たり判定の中心位置（ゲームオブジェクトの原点から見た位置）
	//引数：size	当たり判定のサイズ（幅、高さ、奥行き）
	PlaneCollider(IGameObject* pParent, D3DXVECTOR3 basePos, D3DXVECTOR3 size, D3DXMATRIX matrix);

	//x,y,z軸の長さを渡す
	float GetLen(int axisNum);

	//x,y,z軸の方向ベクトルを渡す
	D3DXVECTOR3 GetDirect(int axisNum);

private:
	//接触判定
	//引数：target	相手の当たり判定, length めり込んだ長さの格納先
	//戻値：接触してればtrue
	bool IsHit(Collider* target, float* length = nullptr) override;
};

