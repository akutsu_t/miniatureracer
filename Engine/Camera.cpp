#include "Camera.h"
#include "Global.h"

//定数
const float CAMERA_Z_NEAR = 0.5f;		//カメラ描画手前位置
const float CAMERA_Z_FAR = 1500.0f;		//カメラ描画最奥位置


//コンストラクタ
Camera::Camera(IGameObject * parent)
	:IGameObject(parent, "Camera"), target_(D3DXVECTOR3(0, 0, 1)), viewAngle_(60)
{
}

//デストラクタ
Camera::~Camera()
{
}

//初期化
void Camera::Initialize()
{
	D3DXMATRIX proj;			//プロジェクション行列
							  //格納先	//視野角		//アスペクト比（縦横比）		//描画手前位置	//描画最奥位置
	D3DXMatrixPerspectiveFovLH(&proj, D3DXToRadian(viewAngle_), (float)g.screenWidth / g.screenHeight, CAMERA_Z_NEAR, CAMERA_Z_FAR);	//プロジェクション行列作成
	Direct3D::pDevice_->SetTransform(D3DTS_PROJECTION, &proj);		//プロジェクション行列をセット
}

//更新
void Camera::Update()
{
	Transform();

	//ビュー行列変更
	D3DXVECTOR3 worldPosition;
	D3DXVec3TransformCoord(&worldPosition, &position_, &worldMatrix_);
	D3DXVECTOR3 worldTarget;
	D3DXVec3TransformCoord(&worldTarget, &target_, &worldMatrix_);
	D3DXMATRIX view;
					//格納先	//視点	//焦点（注視点）//カメラの上の位置
	D3DXMatrixLookAtLH(&view, &worldPosition, &worldTarget, &D3DXVECTOR3(0, 1, 0));			//ビュー行列作成
	Direct3D::pDevice_->SetTransform(D3DTS_VIEW, &view);		//ビュー行列をセット
}

//描画
void Camera::Draw()
{
}

//解放
void Camera::Release()
{
}

//このカメラに切り替える
void Camera::ChangeThis()
{
	//ビュー行列変更
	D3DXVECTOR3 worldPosition;
	D3DXVec3TransformCoord(&worldPosition, &position_, &worldMatrix_);
	D3DXVECTOR3 worldTarget;
	D3DXVec3TransformCoord(&worldTarget, &target_, &worldMatrix_);
	D3DXMATRIX view;
	//格納先	//視点	//焦点（注視点）//カメラの上の位置
	D3DXMatrixLookAtLH(&view, &worldPosition, &worldTarget, &D3DXVECTOR3(0, 1, 0));			//ビュー行列作成
	Direct3D::pDevice_->SetTransform(D3DTS_VIEW, &view);		//ビュー行列をセット
}
