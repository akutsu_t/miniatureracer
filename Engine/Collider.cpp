#include "BoxCollider.h"
#include "SphereCollider.h"
#include "PlaneCollider.h"
#include "IgameObject.h"
#include "Direct3D.h"

//コンストラクタ
Collider::Collider():
	pGameObject_(nullptr),pMesh_(nullptr)
{
}

//デストラクタ
Collider::~Collider()
{
	if (pMesh_ != nullptr)
	{
		pMesh_->Release();
	}
}

//箱型同士の衝突判定
//引数：boxA	１つ目の箱型判定
//引数：boxB	２つ目の箱型判定
//戻値：接触していればtrue
bool Collider::IsHitBoxVsBox(BoxCollider* boxA, BoxCollider* boxB)
{
	//分離軸による衝突判定
	return IsHitOBBVsOBB(*boxA, *boxB);
}

//箱型と球体の衝突判定
//引数：box	箱型判定
//引数：sphere	２つ目の箱型判定
//戻値：接触していればtrue
bool Collider::IsHitBoxVsSphere(BoxCollider* box, SphereCollider* sphere)
{
	//boxとsphereの中心の最短距離を求める
	float dir = LenOBBToPoint(box, sphere);

	//距離がsphereの半径以下なら衝突している
	if (dir <= sphere->radius_)
	{
		return true;
	}
	else
	{
		return false;
	}
}

//球体同士の衝突判定
//引数：circleA	１つ目の球体判定
//引数：circleB	２つ目の球体判定
//戻値：接触していればtrue
bool Collider::IsHitSphereVsSphere(SphereCollider* circleA, SphereCollider* circleB)
{
	D3DXVECTOR3 v = (circleA->center_ + circleA->pGameObject_->GetPosition()) 
		- (circleB->center_ + circleB->pGameObject_->GetPosition());

	if (D3DXVec3Length(&v) <= (circleA->radius_ + circleB->radius_))
	{
		return true;
	}

	return false;
}

//テスト表示用の枠を描画
//引数：position	位置
void Collider::Draw(D3DXVECTOR3 position)
{
	D3DXMATRIX mat;
	D3DXMatrixTranslation(&mat, center_.x, center_.y, center_.z);
	mat = mat * pGameObject_->GetWorldMatrix();
	Direct3D::pDevice_->SetTransform(D3DTS_WORLD, &mat);
	pMesh_->DrawSubset(0);
}

D3DXVECTOR3 Collider::GetCenterPos()
{
	return center_ + pGameObject_->GetPosition();
}

//3次元OBB(直方体)と点(球体)の最短距離算出
float Collider::LenOBBToPoint(BoxCollider * obb, SphereCollider * point)
{
	D3DXVECTOR3 Vec(0, 0, 0);   // 最終的に長さを求めるベクトル

   // 各軸についてはみ出た部分のベクトルを算出
	for (int i = 0; i < COLLIDER_AXIS_MAX; i++)
	{
		float length = obb->GetLen(i);
		if (length <= 0)
		{
			continue;  // 0は計算できない
		}

		D3DXVECTOR3 pCenter = point->center_ + point->pGameObject_->GetPosition();
		D3DXVECTOR3 bCenter = obb->center_ + obb->pGameObject_->GetPosition()
			+ obb->pGameObject_->GetParent()->GetPosition();

		float s = D3DXVec3Dot(&(pCenter - bCenter), &(obb->GetDirect(i))) / length;

		// sの値から、はみ出した部分があればそのベクトルを加算
		s = fabs(s);
		if (s > 1)
		{
			Vec += (1 - s) * length * obb->GetDirect(i);   // はみ出した部分のベクトル算出
		}
	}

	return D3DXVec3Length(&Vec);   // 長さを出力
}

//分離軸に投影された軸成分から投影線分長を算出
FLOAT Collider::LenSegOnSeparateAxis(D3DXVECTOR3 * Sep, D3DXVECTOR3 * e1, D3DXVECTOR3 * e2, D3DXVECTOR3 * e3)
{
	// 3つの内積の絶対値の和で投影線分長を計算
	FLOAT r1 = fabs(D3DXVec3Dot(Sep, e1));
	FLOAT r2 = fabs(D3DXVec3Dot(Sep, e2));
	FLOAT r3;
	if (e3)
	{
		r3 = fabs(D3DXVec3Dot(Sep, e3));
	}
	else
	{
		r3 = 0;
	}
	return r1 + r2 + r3;
}

bool Collider::IsHitOBBVsOBB(BoxCollider &obb1, BoxCollider &obb2)
{
	// 各方向ベクトルの確保
	// （N***:標準化方向ベクトル）
	D3DXVECTOR3 normalA1 = obb1.GetDirect(COLLIDER_AXIS_X), a1 = normalA1 * obb1.GetLen(COLLIDER_AXIS_X);
	D3DXVECTOR3 normalA2 = obb1.GetDirect(COLLIDER_AXIS_Y), a2 = normalA2 * obb1.GetLen(COLLIDER_AXIS_Y);
	D3DXVECTOR3 normalA3 = obb1.GetDirect(COLLIDER_AXIS_Z), a3 = normalA3 * obb1.GetLen(COLLIDER_AXIS_Z);
	D3DXVECTOR3 normalB1 = obb2.GetDirect(COLLIDER_AXIS_X), b1 = normalB1 * obb2.GetLen(COLLIDER_AXIS_X);
	D3DXVECTOR3 normalB2 = obb2.GetDirect(COLLIDER_AXIS_Y), b2 = normalB2 * obb2.GetLen(COLLIDER_AXIS_Y);
	D3DXVECTOR3 normalB3 = obb2.GetDirect(COLLIDER_AXIS_Z), b3 = normalB3 * obb2.GetLen(COLLIDER_AXIS_Z);
	D3DXVECTOR3 obb1Pos = obb1.GetCenterPos();
	D3DXVECTOR3 obb2Pos = obb2.GetCenterPos();
	D3DXVECTOR3 interval = obb1Pos - obb2Pos;

	// 分離軸 : Ae1
	float rA = D3DXVec3Length(&a1);
	float rB = LenSegOnSeparateAxis(&normalA1, &b1, &b2, &b3);
	float length = fabs(D3DXVec3Dot(&interval, &normalA1));
	if (length > rA + rB)
	{
		return false; // 衝突していない
	}

	 // 分離軸 : Ae2
	rA = D3DXVec3Length(&a2);
	rB = LenSegOnSeparateAxis(&normalA2, &b1, &b2, &b3);
	length = fabs(D3DXVec3Dot(&interval, &normalA2));
	if (length > rA + rB)
	{
		return false;
	}

	// 分離軸 : Ae3
	rA = D3DXVec3Length(&a3);
	rB = LenSegOnSeparateAxis(&normalA3, &b1, &b2, &b3);
	length = fabs(D3DXVec3Dot(&interval, &normalA3));
	if (length > rA + rB)
	{
		return false;
	}

	// 分離軸 : Be1
	rA = LenSegOnSeparateAxis(&normalB1, &a1, &a2, &a3);
	rB = D3DXVec3Length(&b1);
	length = fabs(D3DXVec3Dot(&interval, &normalB1));
	if (length > rA + rB)
	{
		return false;
	}

	// 分離軸 : Be2
	rA = LenSegOnSeparateAxis(&normalB2, &a1, &a2, &a3);
	rB = D3DXVec3Length(&b2);
	length = fabs(D3DXVec3Dot(&interval, &normalB2));
	if (length > rA + rB)
	{
		return false;
	}

	// 分離軸 : Be3
	rA = LenSegOnSeparateAxis(&normalB3, &a1, &a2, &a3);
	rB = D3DXVec3Length(&b3);
	length = fabs(D3DXVec3Dot(&interval, &normalB3));
	if (length > rA + rB)
	{
		return false;
	}

	// 分離軸 : C11
	D3DXVECTOR3 Cross;
	D3DXVec3Cross(&Cross, &normalA1, &normalB1);
	rA = LenSegOnSeparateAxis(&Cross, &a2, &a3);
	rB = LenSegOnSeparateAxis(&Cross, &b2, &b3);
	length = fabs(D3DXVec3Dot(&interval, &Cross));
	if (length > rA + rB)
	{
		return false;
	}

	// 分離軸 : C12
	D3DXVec3Cross(&Cross, &normalA1, &normalB2);
	rA = LenSegOnSeparateAxis(&Cross, &a2, &a3);
	rB = LenSegOnSeparateAxis(&Cross, &b1, &b3);
	length = fabs(D3DXVec3Dot(&interval, &Cross));
	if (length > rA + rB)
	{
		return false;
	}

	// 分離軸 : C13
	D3DXVec3Cross(&Cross, &normalA1, &normalB3);
	rA = LenSegOnSeparateAxis(&Cross, &a2, &a3);
	rB = LenSegOnSeparateAxis(&Cross, &b1, &b2);
	length = fabs(D3DXVec3Dot(&interval, &Cross));
	if (length > rA + rB)
	{
		return false;
	}

	// 分離軸 : C21
	D3DXVec3Cross(&Cross, &normalA2, &normalB1);
	rA = LenSegOnSeparateAxis(&Cross, &a1, &a3);
	rB = LenSegOnSeparateAxis(&Cross, &b2, &b3);
	length = fabs(D3DXVec3Dot(&interval, &Cross));
	if (length > rA + rB)
	{
		return false;
	}
	

	// 分離軸 : C22
	D3DXVec3Cross(&Cross, &normalA2, &normalB2);
	rA = LenSegOnSeparateAxis(&Cross, &a1, &a3);
	rB = LenSegOnSeparateAxis(&Cross, &b1, &b3);
	length = fabs(D3DXVec3Dot(&interval, &Cross));
	if (length > rA + rB)
	{
		return false;
	}

	// 分離軸 : C23
	D3DXVec3Cross(&Cross, &normalA2, &normalB3);
	rA = LenSegOnSeparateAxis(&Cross, &a1, &a3);
	rB = LenSegOnSeparateAxis(&Cross, &b1, &b2);
	length = fabs(D3DXVec3Dot(&interval, &Cross));
	if (length > rA + rB)
	{
		return false;
	}

	// 分離軸 : C31
	D3DXVec3Cross(&Cross, &normalA3, &normalB1);
	rA = LenSegOnSeparateAxis(&Cross, &a1, &a2);
	rB = LenSegOnSeparateAxis(&Cross, &b2, &b3);
	length = fabs(D3DXVec3Dot(&interval, &Cross));
	if (length > rA + rB)
	{
		return false;
	}

	// 分離軸 : C32
	D3DXVec3Cross(&Cross, &normalA3, &normalB2);
	rA = LenSegOnSeparateAxis(&Cross, &a1, &a2);
	rB = LenSegOnSeparateAxis(&Cross, &b1, &b3);
	length = fabs(D3DXVec3Dot(&interval, &Cross));
	if (length > rA + rB)
	{
		return false;
	}

	// 分離軸 : C33
	D3DXVec3Cross(&Cross, &normalA3, &normalB3);
	rA = LenSegOnSeparateAxis(&Cross, &a1, &a2);
	rB = LenSegOnSeparateAxis(&Cross, &b1, &b2);
	length = fabs(D3DXVec3Dot(&interval, &Cross));
	if (length > rA + rB)
	{
		return false;
	}

	// 分離平面が存在しないので「衝突している」
	return true;
}

bool Collider::IsHitOBBVsPlane(BoxCollider * obb, PlaneCollider * plane, float * length)
{
	
	//平面のx,z範囲内にOBBが存在するか調べる
	D3DXVECTOR3 obbPos = obb->GetCenterPos();
	D3DXVECTOR3 planePos = plane->GetCenterPos();
	D3DXVECTOR3 dirX = plane->GetDirect(COLLIDER_AXIS_X);
	D3DXVECTOR3 dirZ = plane->GetDirect(COLLIDER_AXIS_Z);
	//平面のx,z軸に対する距離を算出
	float distX = D3DXVec3Dot(&dirX, &(obbPos - planePos));
	float distZ = D3DXVec3Dot(&dirZ, &(obbPos - planePos));

	//平面のx範囲内か
	float len = obb->CalcProjectveLine(dirX);
	if (distX > len + plane->GetLen(COLLIDER_AXIS_X))
	{
		//範囲外なら衝突していない
		return false;
	}

	//平面のz範囲内か
	len = obb->CalcProjectveLine(dirZ);
	if (distZ > len + plane->GetLen(COLLIDER_AXIS_Z))
	{
		//範囲外なら衝突していない
		return false;
	}

	//平面の法線に対するOBBの射影線の長さを算出
	D3DXVECTOR3 planeNormal;							//平面の法線ベクトル
	planeNormal = plane->GetDirect(COLLIDER_AXIS_Y);
	len = obb->CalcProjectveLine(planeNormal);			//近接距離

	//平面とOBBの距離を算出
	float dist = D3DXVec3Dot(&(obbPos - planePos), &planeNormal);

	//戻し距離を算出
	if (length != nullptr)
	{
		if (dist > 0)
		{
			*length = len - fabs(dist);
		}
		else
		{
			*length = len + fabs(dist);
		}
	}

	//衝突判定
	if (fabs(dist) - len < 0.0f)
	{
		return true;		//衝突している
	}
	
	return false;			//衝突してない
}



