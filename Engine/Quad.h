#pragma once
#include "Global.h"
#include "Direct3D.h"

class Quad
{
protected:
	struct Vertex
	{
		D3DXVECTOR3 pos;	//位置
		D3DXVECTOR3 normal;	//法線
		D3DXVECTOR2 uv;		//UV座標
	};

	int vertex_;			//頂点数
	int polygon_;			//ポリゴン数

	LPDIRECT3DVERTEXBUFFER9 pVertexBuffer_;		//頂点バッファ(バーテックスバッファ)
	LPDIRECT3DINDEXBUFFER9 pIndexBuffer_;		//インデックスバッファ
	LPDIRECT3DTEXTURE9 pTexture_;				//テクスチャ
	D3DMATERIAL9 material_;						//マテリアル

public:
	Quad();
	~Quad();

	//準備
	//引数：pFilePath	テクスチャ用のファイルパス
	virtual void Load(const char *filename);

	//頂点情報の準備
	//引数：vertexList	頂点情報	length	頂点情報のサイズ
	void LoadVertex(Vertex *pVertex, UINT length);

	//テクスチャの準備
	//引数：pFilePath	テクスチャ用のファイルパス
	void LoadTexture(const char *filename);

	//インデックス情報の準備
	//引数：indexList	インデックス情報	length	インデックス情報のサイズ
	void LoadIndex(int *indexList, UINT length);

	//表示
	//引数：matrix	ワールド行列
	virtual void Draw(const D3DXMATRIX &matrix);
};