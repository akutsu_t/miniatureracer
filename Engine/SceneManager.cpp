#include "SceneManager.h"
#include "../PlayScene.h"
#include "../TitleScene.h"

//static変数の初期化
SCENE_ID SceneManager::currentSceneID_ = SCENE_ID_TITLE;
SCENE_ID SceneManager::nextSceneID_ = SCENE_ID_TITLE;
IGameObject* SceneManager::pCurrentScene_ = nullptr;

//コンストラクタ
SceneManager::SceneManager(IGameObject * parent)
	:IGameObject(parent, "SceneManager")
{
}

//デストラクタ
SceneManager::~SceneManager()
{
}

//初期化
void SceneManager::Initialize()
{
	//最初のシーンを生成
	pCurrentScene_ = CreateGameObject<TitleScene>(this);
}

//更新
void SceneManager::Update()
{
	if (currentSceneID_ != nextSceneID_)
	{
		//解放
		auto scene = childList_.begin();
		(*scene)->ReleaseSub();
		SAFE_DELETE(*scene);
		//リストをクリア
		childList_.clear();

		//次のシーンを生成
		switch (nextSceneID_)
		{
			case SCENE_ID_PLAY:
				pCurrentScene_ = CreateGameObject<PlayScene>(this);
				break;
			case SCENE_ID_TITLE:
				pCurrentScene_ = CreateGameObject<TitleScene>(this);
				break;
		}

		//カレントを変更
		currentSceneID_ = nextSceneID_;
	}
}

//描画
void SceneManager::Draw()
{
}

//解放
void SceneManager::Release()
{
}

//シーン切り替え
void SceneManager::ChangeScene(SCENE_ID next)
{
	nextSceneID_ = next;
}
