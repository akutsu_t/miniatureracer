#pragma once

#include "Engine/global.h"

class Stage;
class Judge;
class Player;

//プレイシーンを管理するクラス
class PlayScene : public IGameObject
{
	Stage*	pStage_;			//ステージ
	Judge*	pJudge_;			//審判
	Player* pPlayer_;			//プレイヤー

public:
	//コンストラクタ
	//引数：parent  親オブジェクト
	PlayScene(IGameObject* parent);

	//初期化
	void Initialize() override;

	//更新
	void Update() override;

	//描画
	void Draw() override;

	//解放
	void Release() override;

	//キャラクターを生成
	//引数：total 生成するキャラクターの総数
	void CreateCharacter(int total);
};