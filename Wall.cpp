#include "Wall.h"
#include "Engine/Model.h"

const D3DXVECTOR4 AMBIENT_COLOR = D3DXVECTOR4(0.25f, 0.25f, 0.8f, 0.25f);

//コンストラクタ
Wall::Wall(IGameObject * parent)
	:IGameObject(parent, "Wall")
{
}

//デストラクタ
Wall::~Wall()
{
}

//初期化
void Wall::Initialize()
{
	//エフェクト生成
	LPD3DXBUFFER err = 0;		//エラー用
	if (FAILED(D3DXCreateEffectFromFile(Direct3D::pDevice_, "WallShader.hlsl",
		NULL, NULL, D3DXSHADER_DEBUG, NULL, &pEffect_, &err)))
	{
		//失敗時	親ウィンドウ	表示内容			ウィンドウバー		ボタン
		MessageBox(NULL, (char*)err->GetBufferPointer(), "シェーダエラー", MB_OK);
	}

	//モデルデータのロード
	hModel_ = Model::Load("Data/Wall.fbx", pEffect_);
	assert(hModel_ >= 0);
}

//更新
void Wall::Update()
{
}

//描画
void Wall::Draw()
{
	//行列を設定
	Model::SetMatrix(hModel_, worldMatrix_);

	//シェーダー
	PassShaderInfo();

	//環境光を設定
	pEffect_->SetVector("AMBIENT_COLOR", &AMBIENT_COLOR);

	//pEffectを使って描画
	pEffect_->Begin(NULL, 0);

	//パス設定
	pEffect_->BeginPass(0);

	//描画
	Model::Draw(hModel_);

	//パス終了
	pEffect_->EndPass();
	pEffect_->End();
}

//解放
void Wall::Release()
{
}