#pragma once
#include <d3dx9.h>
#include <string>
#include "Engine/Global.h"

class Text
{
	//フォントオブジェクト
	LPD3DXFONT pFont_;

	//カラー
	DWORD color_;

public:
	//コンストラクタ
	Text(std::string fontName, int size);

	//デストラクタ
	~Text();

	//フォントカラーを設定
	void SetColor(DWORD r, DWORD g, DWORD b, DWORD alpha);

	//描画
	//引数：x x座標, y y座標, text 描画文字
	void Draw(int x, int y, std::string text);
};