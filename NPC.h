#pragma once
#include "Character.h"

class NPC : public Character
{
public:
	//コンストラクタ
	NPC(IGameObject* parent);

	//デストラクタ
	~NPC();

	//初期化
	void InitializeAddData() override;

	//描画
	void Draw() override;

	//解放
	void ReleaseAddData() override;

	//移動
	void Move() override;
};

