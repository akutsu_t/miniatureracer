#pragma once
#include "Engine/IGameObject.h"
#include <chrono>				//時間測定用

//画像
enum TIME_PICT {
	TIME_PICT_NUMBER_0,
	TIME_PICT_NUMBER_1,
	TIME_PICT_NUMBER_2,
	TIME_PICT_NUMBER_3,
	TIME_PICT_NUMBER_4,
	TIME_PICT_NUMBER_5,
	TIME_PICT_NUMBER_6,
	TIME_PICT_NUMBER_7,
	TIME_PICT_NUMBER_8,
	TIME_PICT_NUMBER_9,
	TIME_PICT_COLON,
	TIME_PICT_LAP,
	TIME_PICT_SLASH,
	TIME_PICT_WINDOW,
	TIME_PICT_MAX
};

//状態
enum TIME_STATE {
	TIME_STATE_BEFORE,
	TIME_STATE_MEASURE,
	TIME_STATE_END,
	TIME_STATE_MAX,
};

//TimeBoardを管理するクラス
class TimeBoard : public IGameObject
{
	int hPict_[TIME_PICT_MAX];							//画像番号
	std::string pictName_[TIME_PICT_MAX];				//画像名
	std::chrono::system_clock::time_point start_;		//開始時間
	double time_;										//経過時間
	TIME_STATE state_;									//現在の状態
	D3DXMATRIX windowMatrix_;							//ウィンドウの位置
	int minute_;										//分
	int second_;										//秒
	int microSecond_;									//1/100秒
	D3DXVECTOR2 numberSize_;							//数字サイズ
	int lap_;											//周回数
	int lapMax_;										//最大周回数
	bool isDraw_;										//描画するかどうか
public:
	//コンストラクタ
	TimeBoard(IGameObject* parent);

	//デストラクタ
	~TimeBoard();

	//初期化
	void Initialize() override;

	//更新
	void Update() override;

	//描画
	void Draw() override;

	//解放
	void Release() override;

	//時間を測定する
	//戻り値：経過時間(1/100秒)
	double MeasureTime();

	//計測開始
	void StartCount();

	//計測終了
	void EndCount();

	//時間を描画
	void DrawTime();

	//周回数を設定
	//引数：lap 周回数
	void SetLap(int lap);

	//最大周回数を設定
	//引数：lapMax 最大周回数
	void SetLapMax(int lapMax) { lapMax_ = lapMax; }

	//アクセス関数
	int GetMinute() { return minute_; }
	int GetSecond() { return second_; }
	int GetMicroSecond() { return microSecond_; }
	void SetIsDraw(bool isDraw) { isDraw_ = isDraw; }
};