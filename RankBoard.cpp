#include "RankBoard.h"
#include "Engine/Image.h"

//定数
const D3DXVECTOR3 RANK_WINDOW_POS = D3DXVECTOR3(0, 0, 0);
const D3DXVECTOR3 RANK_NUMBER_POS = D3DXVECTOR3(930, 625, 0);

//コンストラクタ
RankBoard::RankBoard(IGameObject * parent)
	:IGameObject(parent, "RankBoard"), rank_(0), isDraw_(true)
{
}

//デストラクタ
RankBoard::~RankBoard()
{
}

//初期化
void RankBoard::Initialize()
{
	//画像番号の初期化
	for (int i = 0; i < RANK_PICT_MAX; i++)
	{
		hPict_[i] = -1;
	}

	//画像ロード準備
	pictName_[RANK_PICT_NUMBER_0] = "0";
	pictName_[RANK_PICT_NUMBER_1] = "1st";
	pictName_[RANK_PICT_NUMBER_2] = "2nd";
	pictName_[RANK_PICT_NUMBER_3] = "3rd";
	pictName_[RANK_PICT_NUMBER_4] = "4th";
	pictName_[RANK_PICT_NUMBER_5] = "5th";
	pictName_[RANK_PICT_NUMBER_6] = "6th";
	pictName_[RANK_PICT_NUMBER_7] = "7th";
	pictName_[RANK_PICT_NUMBER_8] = "8th";
	pictName_[RANK_PICT_NUMBER_9] = "9th";
	pictName_[RANK_PICT_WINDOW] = "RankWindow";

	//画像データをロード
	for (int i = 0; i < RANK_PICT_MAX; i++)
	{
		hPict_[i] = Image::Load("Data/Image/Rank/" + pictName_[i] + ".png");
		assert(hPict_[i] >= 0);
	}

	//表示位置を設定
	position_ = RANK_WINDOW_POS;
	rankPos_ = position_ + RANK_NUMBER_POS;
}

//更新
void RankBoard::Update()
{
}

//描画
void RankBoard::Draw()
{
	//描画するかどうか
	if (isDraw_)
	{
		//ウィンドウ
		Image::SetPosition(hPict_[RANK_PICT_WINDOW], position_);
		Image::Draw(hPict_[RANK_PICT_WINDOW]);

		//順位
		Image::SetPosition(hPict_[rank_], rankPos_);
		Image::Draw(hPict_[rank_]);
	}
}

//解放
void RankBoard::Release()
{
}