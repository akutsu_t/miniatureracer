#include "Box.h"
#include "Text.h"
#include "Character.h"
#include "Engine/BoxCollider.h"
#include "Engine/Model.h"

//コンストラクタ
Box::Box(IGameObject * parent)
	:IGameObject(parent, "Box")
{
}

//デストラクタ
Box::~Box()
{
}

//初期化
void Box::Initialize()
{
	//箱型コライダーをつける
	SetBoxCollider(D3DXVECTOR3(0, 0, 0), D3DXVECTOR3(1.0f, 1.0f, 1.0f), worldMatrix_);
}

//更新
void Box::Update()
{
}

//描画
void Box::Draw()
{
}

//解放
void Box::Release()
{
}

void Box::Input()
{
}

void Box::OnCollision(IGameObject * pTarget, float length)
{
	//PlayerかNPCに当たったとき
	if (pTarget->GetName() == "Player" || pTarget->GetName() == "NPC")
	{
		//キャラクターがめり込みんだ分を押し戻すベクトルを求める
		D3DXVECTOR3 pushBackVec = CalcPushBackVec(pTarget);

#ifdef DEBUG
		//デバッグ用押し戻しベクトルの各数値表示
		OutputDebugString(("pushBackVec" + std::to_string(pushBackVec.x) + ", ").c_str());
		OutputDebugString((std::to_string(pushBackVec.y) + ", ").c_str());
		OutputDebugString((std::to_string(pushBackVec.z) + "\n").c_str());
#endif // DEBUG

		//めり込み分押し戻す
		pTarget->AddPosition(pushBackVec);

		//衝突後のキャラクターの速度を計算
		D3DXVec3Normalize(&pushBackVec, &pushBackVec);
		D3DXVECTOR3 velocity = ((Character*)pTarget)->GetVelocity();
		float vel = fabs(D3DXVec3Dot(&pushBackVec, &velocity));
		((Character*)pTarget)->SetVelocity(pushBackVec * vel + velocity);
	}
}
