#include "CarPhysic.h"
#include "Engine/Input.h"
#include "Engine/Global.h"

CarPhysic::CarPhysic() 
	: velocity_(0, 0, 0), position_(0, 0, 0), rotate_(0, 0, 0), rpm_(0), accel_(0), brake_(0),
	wheelRotate_(0, 0, 0), handleRotate_(0), externalForce_(0, 0, 0), gear_(GEAR_1)
{
}


CarPhysic::~CarPhysic()
{
}

//更新
void CarPhysic::Update(float time)
{
	//エンジンの更新
	UpdateEngine();

	//力を元に速度を計算
	CalcVelocity(time);

	//速度から回転角度を求める
	//旋回中のみ計算
	if (wheelRotate_ != 0)
	{
		//旋回半径
		float turnRadian = WHEEL_BASE / (float)sin(D3DXToRadian(wheelRotate_.y));
		//バック時
		if (gear_ == GEAR_BACK)
		{
			turnRadian = WHEEL_BASE / (float)sin(D3DXToRadian(-wheelRotate_.y));
		}
		//回転角度
		rotate_.y += (D3DXVec3Length(&velocity_) * time) /
			(2 * PI * turnRadian) * 360;
	}

	//位置を求める
	position_ += velocity_ * time;

	//タイヤの角度を更新
	UpdateWheelRotate(time);
	//各ペダルの状態を更新
	UpdatePedal();
	//ハンドルの状態を更新
	UpdateHandle();
}

//アクセルペダルを踏む
void CarPhysic::StepOnAccelPedal()
{
	//ペダルの踏み込み具合を増やす
	if (accel_ + PEDAL_STEP_SPEED < 1.0)
	{
		accel_ += PEDAL_STEP_SPEED;
	}
	else
	{
		accel_ = 1.0;
	}

}

//ブレーキペダルを踏む
void CarPhysic::StepOnBrakePedal()
{
	//ペダルの踏み込み具合を増やす
	if (brake_ + PEDAL_STEP_SPEED < 1.0)
	{
		brake_ += PEDAL_STEP_SPEED;
	}
	else
	{
		brake_ = 1.0;
	}

}

//各ペダルの更新
void CarPhysic::UpdatePedal()
{
	//アクセルペダル
	if (accel_ - PEDAL_REVERSE_SPEED > 0)
	{
		accel_ -= PEDAL_REVERSE_SPEED;
	}
	else
	{
		accel_ = 0;
	}

	//ブレーキペダル
	if (brake_ - PEDAL_REVERSE_SPEED > 0)
	{
		brake_ -= PEDAL_REVERSE_SPEED;
	}
	else
	{
		brake_ = 0;
	}
}

//エンジンの更新
void CarPhysic::UpdateEngine()
{
	//エンジンの回転数を更新
	//減少
	if ((rpm_ - RPM_DOWN_RATE) > 0)
	{
		rpm_ -= RPM_DOWN_RATE;
	}
	else
	{
		rpm_ = 0;
	}
	//アクセルに応じて増加
	rpm_ += (int)(RPM_UP_RATE * accel_);

	//ブレーキに応じて減少
	if (rpm_ - RPM_DOWN_RATE * GEAR_TABLE[gear_] * brake_ > 0)
	{
		rpm_ -= (int)(RPM_DOWN_RATE * GEAR_TABLE[gear_] * brake_);
	}
	else
	{
		rpm_ = 0;
	}

	if (gear_ != GEAR_BACK)
	{
		//エンジン回転数に応じたギアに変更
		gear_ = rpm_ / GEAR_TABLE_INTERVAL;
		if (gear_ > GEAR_3)
		{
			gear_ = GEAR_3;
		}
		else if (gear_ < GEAR_1)
		{
			gear_ = GEAR_1;
		}
	}
}

//ハンドルを更新
void CarPhysic::UpdateHandle()
{
	//右か左かを確認
	//右に回転している
	if (handleRotate_ > 0)
	{
		handleRotate_ -= HANDLE_REVERSE_SPEED;
		if (handleRotate_ < 0)
		{
			handleRotate_ = 0;
		}
	}
	//左に回転している
	else
	{
		handleRotate_ += HANDLE_REVERSE_SPEED;
		if (handleRotate_ > 0)
		{
			handleRotate_ = 0;
		}
	}

}

//タイヤの角度を更新
void CarPhysic::UpdateWheelRotate(float time)
{
	//ハンドルの角度に応じて回転
	wheelRotate_.y = handleRotate_ / HANDLE_GEAR_RATE;

	//最大角度超えないように修正
	if (wheelRotate_.y > WHEEL_ROTATE_MAX)
	{
		wheelRotate_.y = WHEEL_ROTATE_MAX;
	}

	//最小角度を下回らないように修正
	if (wheelRotate_.y < WHEEL_ROTATE_MIN)
	{
		wheelRotate_.y = WHEEL_ROTATE_MIN;
	}

	//タイヤの回転方向を算出
	int wheelTurnDir = 1;
	//後退中
	if (gear_ == GEAR_BACK)
	{
		//方向を反転
		wheelTurnDir = -wheelTurnDir;
	}
	//タイヤの回転を求める
	wheelRotate_.x += 360 * D3DXVec3Length(&velocity_) * time / (2 * PI * WHEEL_RADIUS) * wheelTurnDir;
	//整数部分を360周期に
	wheelRotate_.x = (wheelRotate_.x - (int)wheelRotate_.x) + (int)wheelRotate_.x % 360;
}

//力を元に速度を計算
void CarPhysic::CalcVelocity(float time)
{
	D3DXVECTOR3 force = D3DXVECTOR3(0, 0, 0);	//かかっている力

	//トラクション
	D3DXVECTOR3 tractionDir = D3DXVECTOR3(0, 0, 1.0f);
	D3DXMATRIX rotateYMatrix;
	D3DXMatrixRotationY(&rotateYMatrix, D3DXToRadian(rotate_.y));
	//バック時
	if (gear_ == GEAR_BACK)
	{
		//反転
		tractionDir = -tractionDir;
		D3DXMatrixRotationY(&rotateYMatrix, D3DXToRadian(rotate_.y));

	}
	//トラクションの方向ベクトルを作成
	D3DXVec3TransformCoord(&tractionDir, &tractionDir, &rotateYMatrix);
	//トラクションを計算
	D3DXVECTOR3 traction = tractionDir * (float)rpm_ * GEAR_TABLE[gear_];

	//前回の速度を力に変換
	force = velocity_ / time * MASS;
	force += traction;

	//外部からの力を加算
	force += externalForce_;
	//外部からの力を初期化
	externalForce_ = D3DXVECTOR3(0, 0, 0);

	//空気抵抗
	D3DXVECTOR3 air = -tractionDir * AIR_DENSITY * 0.5f * TOTAL_HEIGHT * TREAD_WIDTH
		* D3DXVec3Length(&velocity_) * D3DXVec3Length(&velocity_);
	force += air;

	//最大摩擦力を求める
	D3DXVECTOR3 frictionDir = D3DXVECTOR3(0, 0, 1);		//摩擦力の方向ベクトル
	D3DXMATRIX frictionMatrix;
	//右旋回時
	if (wheelRotate_.y >= 0)
	{
		D3DXMatrixRotationY(&frictionMatrix, D3DXToRadian(rotate_.y + wheelRotate_.y + 90));
	}
	//左旋回時
	else
	{
		D3DXMatrixRotationY(&frictionMatrix, D3DXToRadian(rotate_.y + wheelRotate_.y - 90));
	}

	//摩擦力を求める
	D3DXVec3TransformCoord(&frictionDir, &frictionDir, &frictionMatrix);

	//水平方向の力を分離
	D3DXVECTOR3 horiForce = D3DXVECTOR3(force.x, 0, force.z);
	force = D3DXVECTOR3(0, force.y, 0);
	//タイヤに対して横方向の力を算出
	float power = D3DXVec3Dot(&horiForce, &frictionDir);
	//横方向の力を分離
	horiForce -= power * frictionDir;
	//前進方向の摩擦を考慮
	if (D3DXVec3Length(&horiForce) - (FRICTION_RATE_F + brake_ * FRICTION_RATE) * MASS * GRAVITY > 0)
	{
		horiForce -= tractionDir * (FRICTION_RATE_F + brake_ * FRICTION_RATE) * MASS * GRAVITY;
	}
	else
	{
		horiForce = D3DXVECTOR3(0, 0, 0);
	}

	//その方向の力が摩擦力以上なら摩擦力分引いた力を加える
	if ((power - FRICTION_RATE * MASS * GRAVITY) > 0)
	{
		power -= FRICTION_RATE * MASS * GRAVITY;
		horiForce += power * frictionDir;
	}

	force += horiForce;


	//力を元に速度を求める
	velocity_ = force / MASS * time;
}

//力を加える
void CarPhysic::AddForce(D3DXVECTOR3 & force)
{
	externalForce_ += force;
}

//ハンドルを切る
void CarPhysic::TurnHandle(float rotate)
{
	//与えられえた角度分回転
	handleRotate_ += rotate;

	//最大角度超えないように修正
	if (handleRotate_ > WHEEL_ROTATE_MAX * HANDLE_GEAR_RATE)
	{
		handleRotate_ = WHEEL_ROTATE_MAX * HANDLE_GEAR_RATE;
	}

	//最小角度を下回らないように修正
	if (handleRotate_ < WHEEL_ROTATE_MIN * HANDLE_GEAR_RATE)
	{
		handleRotate_ = WHEEL_ROTATE_MIN * HANDLE_GEAR_RATE;
	}

	
}

//速度からエンジン回転数を計算
void CarPhysic::CalcRpm()
{
	//速度を元にエンジン回転数を計算
	D3DXVECTOR3 force = velocity_ * MASS;
	rpm_ = (int)(D3DXVec3Length(&force) / GEAR_TABLE[gear_]);
}

//前進
void CarPhysic::MoveFront()
{
	//速度が0なら前進ギアに切り替える
	if (D3DXVec3Length(&velocity_) == 0)
	{
		gear_ = GEAR_1;
	}

	//前進中
	if (gear_ != GEAR_BACK)
	{
		//アクセルペダルを踏む
		StepOnAccelPedal();
	}
	//後退中
	else
	{
		//ブレーキペダルを踏む
		StepOnBrakePedal();
	}
}

//後退
void CarPhysic::MoveBack()
{
	//速度が0なら後退ギアに切り替える
	if (D3DXVec3Length(&velocity_) == 0)
	{
		gear_ = GEAR_BACK;
	}

	//前進中
	if (gear_ != GEAR_BACK)
	{
		//アクセルペダルを踏む
		StepOnBrakePedal();
	}
	//後退中
	else
	{
		//ブレーキペダルを踏む
		StepOnAccelPedal();
	}
}
