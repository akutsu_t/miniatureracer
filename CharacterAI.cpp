#include "CharacterAI.h"
#include "Character.h"
#include "CarPhysic.h"
#include "Random.h"
#include "Stage.h"
#include "Engine/Global.h"

//定数
const int NEXT_POINT_RANDOM_MIN = 2;			//次ポイントの振れ幅最小値
const int NEXT_POINT_RANDOM_MAX = 5;			//次ポイントの振れ幅最大値
const int ROTATE_ALLOWABLE_RANGE = 5;			//次ポイントとの角度の許容範囲

//コンストラクタ
CharacterAI::CharacterAI(Character* pCharacter, CarPhysic* pPhysic) 
	: pCharacter_(pCharacter), pPhysic_(pPhysic), pStage_(nullptr), pRandom_(nullptr),
	yellowZone_(10.0f), redZone_(2.0f), turnHandleSpeed_(0.0f)
{
	//初期化
	nextCheckPos_ = D3DXVECTOR3(0, 0, 0);
	
	//乱数生成用クラスを生成
	pRandom_ = new Random();
	pRandom_->Initialize();

	//ハンドルの回転速度を取得
	turnHandleSpeed_ = pCharacter_->GetHandleSpeed();
}

//デストラクタ
CharacterAI::~CharacterAI()
{
	SAFE_DELETE(pRandom_);
}

//運転する
void CharacterAI::drive()
{
	//キャラクターの角度、位置、チェックポイントを取得
	D3DXVECTOR3 rotate = pCharacter_->GetRotate();
	D3DXVECTOR3 position = pCharacter_->GetPosition();
	int checkPoint = pCharacter_->GetCheckPoint();

	//前進・後退用単位ベクトル
	D3DXVECTOR3 frontDir = D3DXVECTOR3(0, 0, 1);
	//角度に合わせて移動ベクトルを変形
	D3DXMATRIX matX, matY, matZ, mat;


	//現在の正面方向の単位ベクトルを求める
	D3DXMatrixRotationX(&matX, D3DXToRadian(rotate.x));
	D3DXMatrixRotationY(&matY, D3DXToRadian(rotate.y));
	D3DXMatrixRotationZ(&matZ, D3DXToRadian(rotate.z));
	mat = matX * matY * matZ;
	D3DXVec3TransformCoord(&frontDir, &frontDir, &mat);

	D3DXVECTOR3 moveDir = nextCheckPos_ - position;
	//距離を求める
	float dist = D3DXVec3Length(&moveDir);
	//正規化
	D3DXVec3Normalize(&moveDir, &moveDir);

	//角度を求める
	float dot = D3DXVec3Dot(&frontDir, &moveDir);
	float angle = D3DXToDegree(acos(dot));

	//曲がる方向を調べる
	int turnDir = 0;				//曲がる方向
	D3DXMatrixRotationX(&matX, D3DXToRadian(-rotate.x));
	D3DXMatrixRotationY(&matY, D3DXToRadian(-rotate.y));
	D3DXMatrixRotationZ(&matZ, D3DXToRadian(-rotate.z));
	mat = matX * matY * matZ;
	D3DXVec3TransformCoord(&moveDir, &moveDir, &mat);
	//左方向
	if (moveDir.x < 0)
	{
		turnDir = -1;
	}
	else
	{
		turnDir = 1;
	}

	//角度に合わせて回転
	if (angle > ROTATE_ALLOWABLE_RANGE)
	{
		pPhysic_->TurnHandle(turnHandleSpeed_ * turnDir);
	}
	else if (angle <= -ROTATE_ALLOWABLE_RANGE)
	{
		pPhysic_->TurnHandle(-turnHandleSpeed_ * turnDir);
	}

	//目標地点との内積
	int next = checkPoint + 1;
	int checkMax = pStage_->GetCheckMax();
	//次のチェックポイントが次の周
	if (next >= checkMax)
	{
		//変換
		next -= checkMax;
	}
	//次チェックポイントの位置を取得
	D3DXVECTOR3 nextPoint = pStage_->GetBonePosition("checkPoint" + std::to_string(next));
	next += 1;
	//さらに次のチェックポイントが次の周
	if (next >= checkMax)
	{
		//変換
		next -= checkMax;
	}
	//二つ先のチェックポイントの位置を取得
	D3DXVECTOR3 next2Point = pStage_->GetBonePosition("checkPoint" + std::to_string(next));
	D3DXVECTOR3 checkPointDir = next2Point - nextPoint;
	D3DXVec3Normalize(&checkPointDir, &checkPointDir);
	float rate = 1 - fabs(D3DXVec3Dot(&moveDir, &checkPointDir));
	//現在の速度を取得
	D3DXVECTOR3 velVec = pPhysic_->GetVelocity();
	//大きさを求める
	float vel = D3DXVec3Length(&velVec);
	//速度と距離を比較して加速するか決める
	if (dist > vel * redZone_ * rate)
	{
		//加速（前進）
		pPhysic_->MoveFront();
	}
}

void CharacterAI::CalcNextPoint()
{
	//現在のチェックポイントを取得
	int checkPoint = pCharacter_->GetCheckPoint();

	int next = checkPoint + 1;
	int checkMax = pStage_->GetCheckMax();
	//次のチェックポイントが次の周
	if (next >= checkMax)
	{
		//変換
		next -= checkMax;
	}
	//In側の最接近地点
	D3DXVECTOR3 nextPointIn = pStage_->GetBonePosition("checkPoint" + std::to_string(next) + "_In");
	//Out側の最接近地点
	D3DXVECTOR3 nextPointOut = pStage_->GetBonePosition("checkPoint" + std::to_string(next) + "_Out");
	//In.Out間からランダムで次ポイント方向のベクトルを求める
	D3DXVECTOR3 nextPoint = (nextPointOut - nextPointIn)
		* (pRandom_->GetRandom(NEXT_POINT_RANDOM_MIN, NEXT_POINT_RANDOM_MAX) * 0.1f);
	nextCheckPos_ =  nextPoint + nextPointIn;
}

//ステージ情報を登録
void CharacterAI::SetStage(Stage * pStage)
{
	pStage_ = pStage;

	//次のチェックポイントまでのベクトルを取得
	CalcNextPoint();
}

