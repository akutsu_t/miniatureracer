#include "Judge.h"
#include "Player.h"
#include "Stage.h"
#include "Engine/Model.h"
#include "Engine/Image.h"
#include "Engine/Audio.h"

//定数
const int START_COUNT_MAX = 9;								//スタート合図までの最大カウント数
const int START_SIGNAL_COUNT = 3;							//スタート合図の表示時間
const int FINISH_COUNT_MAX = 5;								//ゴール後にリザルトを表示するまでの時間
const D3DXVECTOR3 SIGNAL_POS = D3DXVECTOR3(344, 336, 0);	//合図描画位置
const int LAP_MAX = 3;										//最大周回数


//コンストラクタ
Judge::Judge(IGameObject * parent) : IGameObject(parent, "Judge"), pStage_(nullptr),
isStart_(false), isEnd_(false), state_(JUDGE_STATE_READY), time_(0.0), second_(0),
lapMax_(LAP_MAX), pPlayer_(nullptr), isSignal_(false)
{
}

//デストラクタ
Judge::~Judge()
{
}

//初期化
void Judge::Initialize()
{
	//画像番号の初期化
	for (int i = 0; i < JUDGE_PICT_MAX; i++)
	{
		hPict_[i] = -1;
	}

	//画像ロード準備
	pictName_[JUDGE_PICT_NUMBER_0] = "0";
	pictName_[JUDGE_PICT_NUMBER_1] = "1";
	pictName_[JUDGE_PICT_NUMBER_2] = "2";
	pictName_[JUDGE_PICT_NUMBER_3] = "3";
	pictName_[JUDGE_PICT_GO] = "GO";
	pictName_[JUDGE_PICT_GOAL] = "GOAL";

	//画像データのロード
	for (int i = 0; i < JUDGE_PICT_MAX; i++)
	{
		hPict_[i] = Image::Load("Data/Image/Start/" + pictName_[i] + ".png");
		assert(hPict_[i] >= 0);
	}

	//基準となる時間を取得
	start_ = std::chrono::system_clock::now();
}

//更新
void Judge::Update()
{
	//順位判定
	JudgeRanking();

	//順位を各キャラクターに通知
	NoticeCharacterOfRank();

	//基準時間からの時間計測
	time_ = MeasureTime();
	second_ = (int)time_ / 100;

	switch (state_)
	{
		//待機中
		case JUDGE_STATE_WAIT:
			break;

		//スタートカウント
		case JUDGE_STATE_READY:
			//カウント表示開始時に音を再生
			if (!isSignal_ && START_COUNT_MAX - second_ <= START_SIGNAL_COUNT)
			{
				Audio::Play("signal");
				isSignal_ = true;
			}

			if (second_ >= START_COUNT_MAX)
			{
				//スタートに状態を変更
				state_ = JUDGE_STATE_START;
				//基準時間を再取得
				start_ = std::chrono::system_clock::now();
				//各キャラクターに開始合図
				SignalStart();
			}
			break;

		//開始合図表示中
		case JUDGE_STATE_START:
			if (second_ >= START_SIGNAL_COUNT)
			{
				//レース中に状態を変更
				state_ = JUDGE_STATE_RACE;
			}
			break;

		//レース中
		case JUDGE_STATE_RACE:
			//終了判定
			if (DoJudgeEnd())
			{
				//終了状態に
				state_ = JUDGE_STATE_FINISH;
				//終了後のカウント用に基準時間を更新
				start_ = std::chrono::system_clock::now();

			}
			break;

		//レース終了
		case JUDGE_STATE_FINISH:
			//リザルトへの移行時間経過
			if (second_ >= FINISH_COUNT_MAX)
			{
				//リザルト表示状態へ
				state_ = JUDGE_STATE_RESULT;
				pPlayer_->ShowResult();
			}
			break;

		//結果表示中
		case JUDGE_STATE_RESULT:
			break;

		default:
			break;
		}
}

//描画
void Judge::Draw()
{
	//状態をもとに表示内容を変更
	switch (state_)
	{
		//スタート前
		case JUDGE_STATE_READY:
			//開始まで3秒を切ったら表示開始
			if (START_COUNT_MAX - second_ <= START_SIGNAL_COUNT)
			{
				//スタートカウント
				Image::SetPosition(hPict_[START_COUNT_MAX - second_], SIGNAL_POS);
				Image::Draw(hPict_[START_COUNT_MAX - second_]);
			}
			break;

		//スタート時
		case JUDGE_STATE_START:
			//スタート合図
			Image::SetPosition(hPict_[JUDGE_PICT_GO], SIGNAL_POS);
			Image::Draw(hPict_[JUDGE_PICT_GO]);
			break;

		//レース中
		case JUDGE_STATE_RACE:
			break;
	
		//ゴール後
		case JUDGE_STATE_FINISH:
			//ゴール合図
			Image::SetPosition(hPict_[JUDGE_PICT_GOAL], SIGNAL_POS);
			Image::Draw(hPict_[JUDGE_PICT_GOAL]);
			break;

		//結果表示時
		case JUDGE_STATE_RESULT:
			break;

		default:
			break;
	}

}

//解放
void Judge::Release()
{
}

//任意のキャラクターをリストに登録する
void Judge::RegistCharacter(Character * pCharacter)
{
	//すでに追加されていないか調べる
	if (IsExistInRankList(pCharacter))
	{
		//追加せずに終了
		return;
	}

	//プレイヤーなら保存しておく
	if (pCharacter->GetName() == "Player")
	{
		pPlayer_ = (Player*)pCharacter;
	}

	//順位リストに追加
	rankList_.push_back(pCharacter);

	//最大周回数を通知
	pCharacter->SetLapMax(lapMax_);
}

//キャラクター登録完了
void Judge::CompleteRegistration()
{
	//待機状態ならスタートカウント状態へ
	if (state_ == JUDGE_STATE_WAIT)
	{
		state_ = JUDGE_STATE_READY;
		//基準時間を再取得
		start_ = std::chrono::system_clock::now();
	}
}

//任意のキャラクターをリストから削除
void Judge::RemoveCharacter(Character * pCharacter)
{
	for (auto it = rankList_.begin(); it != rankList_.end(); it++)
	{
		//指定されたキャラクターか調べる
		if (*it == pCharacter)
		{
			//削除して終了
			rankList_.erase(it);
			return;
		}
	}
}

//順位リストに任意のキャラクターが存在するか調べる
bool Judge::IsExistInRankList(Character * pCharacter)
{
	for (auto it = rankList_.begin(); it != rankList_.end(); it++)
	{
		if (*it == pCharacter)
		{
			//存在する
			return true;
		}
	}

	//存在しない
	return false;
}

//任意のキャラクターをゴールリストに追加
void Judge::AddToFinishList(Character * pCharacter)
{
	//追加
	finishList_.push_back(pCharacter);
}

//順位判定
void Judge::JudgeRanking()
{
	//キャラクターが1人以下
	if (rankList_.size() <= 1)
	{
		//ゴールしたかだけ判定
		auto it = rankList_.begin();
		if (it != rankList_.end() && (*it)->GetLap() > lapMax_)
		{
			Character* pCharacter = *it;
			//ゴールリストに追加し、キャラクターリストから削除
			AddToFinishList(pCharacter);
			RemoveCharacter(pCharacter);

			//ゴールしたことを通知
			pCharacter->EndGame();
		}

		return;
	}

	int checkPointMax = 0;				//キャラクターのチェックポイント最大値
	int rapMax = 0;						//キャラクターのラップ最大数
	Character* pCharacterMax = nullptr;	//最高順位キャラクターのポインタ格納用
	std::vector<Character*> result;		//結果格納用

	//リストのサイズが0になるまで
	while (!rankList_.empty())
	{
		//リスト内を先頭から見る
		for (auto it = rankList_.begin(); it != rankList_.end(); it++)
		{
			//暫定1位のキャラクターがいる
			if (pCharacterMax != nullptr)
			{
				//どちらが前にいるか判定
				if (ComparePosition(*it, pCharacterMax))
				{
					//最高順位のプレイヤーを入れ替え
					pCharacterMax = *it;
				}
			}
			//暫定1位のキャラクターがいない
			else
			{
				//現在のキャラクターを暫定1位に
				pCharacterMax = *it;
			}
		}

		//ゴールしたか
		if (pCharacterMax->GetLap() > lapMax_)
		{
			//ゴールリストに追加し、キャラクターリストから削除
			AddToFinishList(pCharacterMax);
			RemoveCharacter(pCharacterMax);
			pCharacterMax->EndGame();

			//ゴールしたのがプレーヤーならレース終了
			if (pCharacterMax == pPlayer_)
			{
				state_ = JUDGE_STATE_FINISH;
				//終了時の効果音を再生
				Audio::Play("goal");
				//終了後のカウント用に基準時間を更新
				start_ = std::chrono::system_clock::now();
			}
		}
		else
		{
			//最高順位のプレイヤーを登録
			result.push_back(pCharacterMax);
			RemoveCharacter(pCharacterMax);
		}

		//初期化
		pCharacterMax = nullptr;
	}

	//並べ替えの結果をrankListに保存
	rankList_.clear();
	while (!result.empty())
	{
		Character* pObj = result.at(result.size() - 1);
		result.pop_back();
		rankList_.push_front(pObj);
	}
}

//チェックポイント内の位置比較（チェックポイントが同じ場合）
bool Judge::ComparePosition(Character* pCharacter1, Character* pCharacter2)
{
	//周回数判定
	//各キャラクターの周回数を取得
	int lap1 = pCharacter1->GetLap();
	int lap2 = pCharacter2->GetLap();
	//周回数が同じか
	if (lap1 != lap2)
	{
		if (lap1 > lap2)
		{
			//キャラクター1が前にいる
			return true;
		}
		else
		{
			//キャラクター1が後ろにいる
			return false;
		}
	}

	//チェックポイント判定
	//各キャラクターのチェックポイントを取得
	int checkPoint1 = pCharacter1->GetCheckPoint();
	int checkPoint2 = pCharacter2->GetCheckPoint();
	//同じチェックポイント内にいるか
	if (checkPoint1 != checkPoint2)
	{
		//チェックポイントを比較
		if (checkPoint1 > checkPoint2)
		{
			//キャラクター1が前にいる
			return true;
		}
		else
		{
			//キャラクター1が後ろにいる
			return false;
		}
	}

	//位置判定
	//次のチェックポイントまでのベクトルを取得
	int hStageModel = pStage_->GetModelHandle();
	//現在と次のチェックポイント位置を取得
	D3DXVECTOR3 checkPos = Model::GetBonePosition(hStageModel, "checkPoint" + std::to_string(checkPoint1));
	D3DXVECTOR3 nextCheckPos = Model::GetBonePosition(hStageModel, "checkPoint" + std::to_string(checkPoint1 + 1));
	//チェックポイント間の軸を算出
	D3DXVECTOR3 axis = nextCheckPos - checkPos;
	D3DXVec3Normalize(&axis, &axis);
	//軸をもとに各プレイヤの位置を算出
	D3DXVECTOR3 character1Pos = pCharacter1->GetPosition() - checkPos;
	D3DXVECTOR3 character2Pos = pCharacter2->GetPosition() - checkPos;
	float character1Dist = D3DXVec3Dot(&axis, &character1Pos);
	float character2Dist = D3DXVec3Dot(&axis, &character2Pos);
	//キャラクターのチェックポイントからの距離を比較
	if (character1Dist > character2Dist)
	{
		//キャラクター1が前にいる
		return true;
	}

	//キャラクター1が後ろにいる
	return false;
}

//リスト内のキャラクターに現在の順位を通知
void Judge::NoticeCharacterOfRank()
{
	//現在の順位
	int rank = 1;
	//リスト内の全キャラクター
	for (auto it = rankList_.begin(); it != rankList_.end(); it++)
	{
		(*it)->SetRank(finishList_.size() + rank);
		rank++;
	}
}

//スタート合図を出す
void Judge::SignalStart()
{
	//全キャラクターにスタート合図を出す
	for (auto it = rankList_.begin(); it != rankList_.end(); it++)
	{
		(*it)->StartGame();
	}
}

//経過時間を測定
double Judge::MeasureTime()
{
	//現在の時間を取得
	std::chrono::system_clock::time_point end;
	end = std::chrono::system_clock::now();

	//経過時間を算出
	return static_cast<double>(std::chrono::duration_cast<std::chrono::microseconds>(end - start_).count() / 10000);
}

//レース終了か判定する
bool Judge::DoJudgeEnd()
{
	//順位リストが空でゴールリストが空ではない
	if (rankList_.empty() && !finishList_.empty())
	{
		return true;
	}
	else
	{
		return false;
	}
}
