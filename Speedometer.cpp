#include "Speedometer.h"
#include "Engine/Image.h"

//定数
const D3DXVECTOR3 SPEED_WINDOW_POS = D3DXVECTOR3(0, 0, 0);
const D3DXVECTOR3 SPEED_NUMBER_POS = D3DXVECTOR3(50, 630, 0);

//コンストラクタ
Speedometer::Speedometer(IGameObject * parent)
	:IGameObject(parent, "Speedometer"), speed_(0), onePlaceNum_(0), tenPlaceNum_(0),
	hundredPlaceNum_(0), isDraw_(true)
{
}

//デストラクタ
Speedometer::~Speedometer()
{
}

//初期化
void Speedometer::Initialize()
{
	//画像番号の初期化
	for (int i = 0; i < SPEED_PICT_MAX; i++)
	{
		hPict_[i] = -1;
	}

	//画像のロード準備
	pictName_[SPEED_PICT_NUMBER_0] = "0";
	pictName_[SPEED_PICT_NUMBER_1] = "1";
	pictName_[SPEED_PICT_NUMBER_2] = "2";
	pictName_[SPEED_PICT_NUMBER_3] = "3";
	pictName_[SPEED_PICT_NUMBER_4] = "4";
	pictName_[SPEED_PICT_NUMBER_5] = "5";
	pictName_[SPEED_PICT_NUMBER_6] = "6";
	pictName_[SPEED_PICT_NUMBER_7] = "7";
	pictName_[SPEED_PICT_NUMBER_8] = "8";
	pictName_[SPEED_PICT_NUMBER_9] = "9";
	pictName_[SPEED_PICT_WINDOW] = "SpeedWindow";

	//画像データをロード
	for (int i = 0; i < SPEED_PICT_MAX; i++)
	{
		hPict_[i] = Image::Load("Data/Image/Speed/" + pictName_[i] + ".png");
		assert(hPict_[i] >= 0);
	}

	//数字サイズを取得
	numberSize_ = Image::GetTextureSize(hPict_[SPEED_PICT_NUMBER_0]);

	//各画像の表示位置を決める
	//ウィンドウ
	position_ = SPEED_WINDOW_POS;
	//数値
	numberPos_ = position_ + SPEED_NUMBER_POS;
}

//更新
void Speedometer::Update()
{
	//速度から各位の表示画像番号を決める
	//百の位
	hundredPlaceNum_ = speed_ / 100 % 10;
	//十の位
	tenPlaceNum_ = speed_ / 10 % 10;
	//一の位
	onePlaceNum_ = speed_ % 10;
}

//描画
void Speedometer::Draw()
{
	//描画するかどうか
	if (isDraw_)
	{
		//ウィンドウ
		Image::SetPosition(hPict_[SPEED_PICT_WINDOW], position_);
		Image::Draw(hPict_[SPEED_PICT_WINDOW]);

		//速度
		DrawSpeed();
	}
}

//解放
void Speedometer::Release()
{
}

//速度を描画
void Speedometer::DrawSpeed()
{
	D3DXVECTOR3 position = numberPos_;		//描画位置

	//百の位
	//速度が100以上の場合のみ描画
	if (hundredPlaceNum_ > 0)
	{
		Image::SetPosition(hPict_[hundredPlaceNum_], position);
		Image::Draw(hPict_[hundredPlaceNum_]);
	}
	//表示位置を更新
	position.x += numberSize_.x;

	//十の位
	//速度が10以上の場合のみ描画
	if (hundredPlaceNum_ > 0 || tenPlaceNum_ > 0)
	{
		Image::SetPosition(hPict_[tenPlaceNum_], position);
		Image::Draw(hPict_[tenPlaceNum_]);
	}
	//表示位置を更新
	position.x += numberSize_.x;

	//一の位
	Image::SetPosition(hPict_[onePlaceNum_], position);
	Image::Draw(hPict_[onePlaceNum_]);
}
