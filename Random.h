#pragma once
#include <random>

class Random
{
	std::random_device random_;			//初期シード
	std::mt19937 mt_;				//疑似乱数

public:
	Random();
	~Random();

	//乱数生成準備
	void Initialize();

	//乱数を取得
	//引数：min 最小値, max 最大値
	int GetRandom(int min, int max);

	//解放
	void Release();
};

