#pragma once
#include "Engine/IGameObject.h"

//時間表示用画像
enum RESULT_TIME_PICT {
	RESULT_TIME_PICT_NUMBER_0,
	RESULT_TIME_PICT_NUMBER_1,
	RESULT_TIME_PICT_NUMBER_2,
	RESULT_TIME_PICT_NUMBER_3,
	RESULT_TIME_PICT_NUMBER_4,
	RESULT_TIME_PICT_NUMBER_5,
	RESULT_TIME_PICT_NUMBER_6,
	RESULT_TIME_PICT_NUMBER_7,
	RESULT_TIME_PICT_NUMBER_8,
	RESULT_TIME_PICT_NUMBER_9,
	RESULT_TIME_PICT_COLON,
	RESULT_TIME_PICT_MAX
};

//順位表示用画像
enum RESULT_RANK_PICT {
	RESULT_RANK_PICT_0,
	RESULT_RANK_PICT_1,
	RESULT_RANK_PICT_2,
	RESULT_RANK_PICT_3,
	RESULT_RANK_PICT_4,
	RESULT_RANK_PICT_5,
	RESULT_RANK_PICT_6,
	RESULT_RANK_PICT_MAX
};

//リザルト表示用画像
enum RESULT_PICT {
	RESULT_PICT_WINDOW_NONE,
	RESULT_PICT_WINDOW,
	RESULT_PICT_MAX
};

//結果を管理するクラス
class Result : public IGameObject
{
	int hTimePict_[RESULT_TIME_PICT_MAX];					//時間表示用画像番号
	std::string timePictName_[RESULT_TIME_PICT_MAX];		//時間表示用画像名
	int hRankPict_[RESULT_RANK_PICT_MAX];					//順位表示用画像番号
	std::string rankPictName_[RESULT_RANK_PICT_MAX];		//順位表示用画像名
	int hResultPict_[RESULT_PICT_MAX];						//リザルト表示用画像番号
	std::string resultPictName_[RESULT_PICT_MAX];			//リザルト表示画像名
	bool isDisplay_;										//表示するかどうか
	D3DXVECTOR2 numberSize_;								//数字サイズ

	//表示情報
	int minute_;								//分
	int second_;								//秒
	int microSecond_;							//1/100秒
	int rank_;									//順位
public:
	//コンストラクタ
	Result(IGameObject* parent);

	//デストラクタ
	~Result();

	//初期化
	void Initialize() override;

	//更新
	void Update() override;

	//描画
	void Draw() override;

	//解放
	void Release() override;

	//リザルト画面を表示
	void ShowResult();

	//時間を描画
	void DrawTime();

	//画像番号の初期化
	void InitHPict();

	//画像データをロード
	void LoadPict();

	//時間を設定
	void SetTime(int minute, int second, int microSecond);

	//順位を設定
	void SetRank(int rank) { rank_ = rank; }
};