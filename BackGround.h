#pragma once

#include "Engine/IGameObject.h"


//背景を管理するクラス
class BackGround : public IGameObject
{
public:
	//コンストラクタ
	BackGround(IGameObject* parent);


	//デストラクタ
	~BackGround();


	//初期化
	void Initialize() override;


	//更新
	void Update() override;


	//描画
	void Draw() override;


	//解放
	void Release() override;

};