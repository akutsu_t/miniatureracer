#include "BackGround.h"
#include "Engine/Model.h"

//コンストラクタ
BackGround::BackGround(IGameObject * parent)
	:IGameObject(parent, "BackGround")
{
}

//デストラクタ
BackGround::~BackGround()
{
}

//初期化
void BackGround::Initialize()
{
	//エフェクト生成
	LPD3DXBUFFER err = 0;		//エラー用
	if (FAILED(D3DXCreateEffectFromFile(Direct3D::pDevice_, "BackGroundShader.hlsl",
		NULL, NULL, D3DXSHADER_DEBUG, NULL, &pEffect_, &err)))
	{
		//失敗時	親ウィンドウ	表示内容			ウィンドウバー		ボタン
		MessageBox(NULL, (char*)err->GetBufferPointer(), "シェーダエラー", MB_OK);
	}

	//モデルデータのロード
	hModel_ = Model::Load("Data/BackGround.fbx", pEffect_);
	assert(hModel_ >= 0);
}

//更新
void BackGround::Update()
{
}

//描画
void BackGround::Draw()
{
	//行列を設定
	Model::SetMatrix(hModel_, worldMatrix_);

	//シェーダー
	PassShaderInfo();

	//pEffectを使って描画
	pEffect_->Begin(NULL, 0);

	//パス設定
	pEffect_->BeginPass(0);

	//描画
	Model::Draw(hModel_);

	//パス終了
	pEffect_->EndPass();
	pEffect_->End();
}

//解放
void BackGround::Release()
{
}