//グローバル変数(アプリ側から渡される情報)
float4x4	WVP;			//ワールド*ビュー*プロジェクション
float4x4	W;				//ワールド行列
float4x4	RS;				//拡大の逆行列と回転行列
float4		LIGHT_DIR;		//光の向き
float4		DIFFUSE_COLOR;	//拡散反射光
float4		AMBIENT_COLOR;	//環境光
float4		SPECULAR_COLOR;	//鏡面反射光（色）
float		SPECULAR_POWER;	//鏡面反射光（大きさ）
float4		CAMERA_POS;		//視点（カメラ位置）
texture		TEXTURE;		//テクスチャ
bool		EXIST_TEXTURE;	//テクスチャの有無

//サンプラー
sampler texSampler = sampler_state
{
	Texture = <TEXTURE>;			//テクスチャ情報が入っている変数名
	//アンチエイリアスをかけるための設定
	MinFilter = LINEAR;
	MagFilter = LINEAR;
	MipFilter = LINEAR;
	//そのまま並べる（デフォルト）
	AddressU = Wrap;
	AddressV = Wrap;
};

//頂点シェーダーの出力、ピクセルシェーダーの入力
struct VS_OUT
{
	float4 pos		: SV_POSITION;		//位置
	float4 normal	: NORMAL;			//法線
	float4 eye		: TEXCOORD1;		//視線(専用のセマンティクスがないときはTEXCOORD1〜)
	float2 uv		: TEXCOORD0;		//UV座標
};

//頂点シェーダー
//頂点座標（ローカル座標）をスクリーン座標に変換
//引数：pos	頂点座標（ローカル）, normal 法線, uv UV座標
//戻り値：頂点のスクリーン座標
VS_OUT VS(float4 pos : POSITION, float4 normal : NORMAL, float2 uv : TEXCOORD0)
{
	//出力データ
	VS_OUT outData;

	//位置（スクリーン座標に変換）頂点シェーダーで必ず行う
	outData.pos = mul(pos, WVP);

	//位置（ワールド座標）
	float4 worldPos = mul(pos, W);
	outData.eye = normalize(CAMERA_POS - worldPos);

	//法線
	//オブジェクトに合わせる（拡大、回転）
	normal = mul(normal, RS);
	//法線を正規化
	outData.normal = normalize(normal);

	//UV座標
	outData.uv = uv;

	return outData;
}

//ピクセルシェーダー
//引数：pos スクリーン座標
//戻り値：その座標の色情報
float4 PS(VS_OUT inData) : COLOR
{
	//法線を正規化
	inData.normal = normalize(inData.normal);

	//ライト（Directx側で正規化してそれを渡したほうがいい）
	//向き(Directxの設定とシェーダの設定は逆)
	float4 lightDir = LIGHT_DIR;
	//ライト方向を正規化
	lightDir = normalize(lightDir);

	//拡散反射光
	//明るさを設定
	float4 diffuse = 1;
	//透明度を設定
	diffuse.a = 1.0f;
	//色をつける(拡散反射光)
	//テクスチャあり
	if (EXIST_TEXTURE)
	{
		diffuse *= tex2D(texSampler, inData.uv);
	}
	//テクスチャなし
	else
	{
		diffuse *= DIFFUSE_COLOR;
	}

	//環境光
	float4 ambient = AMBIENT_COLOR;

	//鏡面反射光
	//正反射ベクトルを求める
	float4 R = reflect(lightDir, inData.normal);
	//鏡面反射光を求める
	float4 speculer = pow(dot(R, inData.eye), SPECULAR_POWER) * 2.0f * SPECULAR_COLOR;

	return ambient + diffuse + speculer;
}


technique
{
	//どの関数を使うか設定(バックミラーなどを作る場合は2回描画するためpassが2つ必要)
	pass
	{
		//頂点シェーダー		vs_3_0 でコンパイル(シェーダーは実行時にコンパイルされる)
		VertexShader = compile vs_3_0 VS();
		//ピクセルシェーダー
		PixelShader = compile ps_3_0 PS();
	}
}
