#include "TimeBoard.h"
#include "Engine/Image.h"

//定数
const D3DXVECTOR3 TIME_WINDOW_POS = D3DXVECTOR3(0, 0, 0);		//ウィンドウ位置
const D3DXVECTOR3 TIME_MINUTE_POS = D3DXVECTOR3(80, 30, 0);		//タイム表示位置
const D3DXVECTOR3 TIME_LAP_POS = D3DXVECTOR3(80, 70, 0);		//周回数表示位置

//コンストラクタ
TimeBoard::TimeBoard(IGameObject * parent)
	:IGameObject(parent, "TimeBoard"), time_(0.0f), state_(TIME_STATE_BEFORE), minute_(0),
	second_(0), microSecond_(0), lap_(0), lapMax_(0), isDraw_(true)
{
}

//デストラクタ
TimeBoard::~TimeBoard()
{
}

//初期化
void TimeBoard::Initialize()
{
	//画像番号の初期化
	for (int i = 0; i < TIME_PICT_MAX; i++)
	{
		hPict_[i] = -1;
	}

	//画像データのロード準備
	pictName_[TIME_PICT_NUMBER_0] = "0";
	pictName_[TIME_PICT_NUMBER_1] = "1";
	pictName_[TIME_PICT_NUMBER_2] = "2";
	pictName_[TIME_PICT_NUMBER_3] = "3";
	pictName_[TIME_PICT_NUMBER_4] = "4";
	pictName_[TIME_PICT_NUMBER_5] = "5";
	pictName_[TIME_PICT_NUMBER_6] = "6";
	pictName_[TIME_PICT_NUMBER_7] = "7";
	pictName_[TIME_PICT_NUMBER_8] = "8";
	pictName_[TIME_PICT_NUMBER_9] = "9";
	pictName_[TIME_PICT_COLON] = "colon";
	pictName_[TIME_PICT_LAP] = "LAP";
	pictName_[TIME_PICT_SLASH] = "slash";
	pictName_[TIME_PICT_WINDOW] = "TimeWindow";

	//画像データのロード
	for (int i = 0; i < TIME_PICT_MAX; i++)
	{
		hPict_[i] = Image::Load("Data/Image/Time/" + pictName_[i] + ".png");
		assert(hPict_[i] >= 0);
	}

	//数字サイズを取得
	numberSize_ = Image::GetTextureSize(hPict_[TIME_PICT_NUMBER_0]);

	//基準となる時間を取得
	start_ = std::chrono::system_clock::now();

	//位置を設定
	//ウィンドウ
	position_ = TIME_WINDOW_POS;
	D3DXMatrixTranslation(&windowMatrix_, position_.x, position_.y, position_.z);
	//タイム

}

//更新
void TimeBoard::Update()
{
	//計測中
	if (state_ == TIME_STATE_MEASURE)
	{
		//時間を取得
		time_ = MeasureTime();

		//経過時間を表示
		//各数値に変換
		minute_ = (int)time_ / 6000;
		second_ = (int)time_ % 6000 / 100;
		microSecond_ = (int)time_ % 100;
	}
}

//描画
void TimeBoard::Draw()
{
	//描画するかどうか
	if (isDraw_)
	{
		//ウィンドウ
		Image::SetPosition(hPict_[TIME_PICT_WINDOW], position_);
		Image::Draw(hPict_[TIME_PICT_WINDOW]);

		//時間
		DrawTime();

		//LAP
		D3DXVECTOR3 lapPos = position_ + TIME_LAP_POS;						//描画位置
		Image::SetPosition(hPict_[TIME_PICT_LAP], lapPos);
		Image::Draw(hPict_[TIME_PICT_LAP]);
		lapPos.x += Image::GetTextureSize(hPict_[TIME_PICT_LAP]).x;			//描画位置を更新

		//周回数
		Image::SetPosition(hPict_[lap_], lapPos);
		Image::Draw(hPict_[lap_]);
		lapPos.x += Image::GetTextureSize(hPict_[lap_]).x;					//描画位置を更新

		//スラッシュ
		Image::SetPosition(hPict_[TIME_PICT_SLASH], lapPos);
		Image::Draw(hPict_[TIME_PICT_SLASH]);
		lapPos.x += Image::GetTextureSize(hPict_[TIME_PICT_SLASH]).x;		//描画位置を更新

		//最大周回数
		Image::SetPosition(hPict_[lapMax_], lapPos);
		Image::Draw(hPict_[lapMax_]);
	}
}

//解放
void TimeBoard::Release()
{
}

//時間を計る
double TimeBoard::MeasureTime()
{
	//現在の時間を取得
	std::chrono::system_clock::time_point end;
	end = std::chrono::system_clock::now();

	//経過時間を算出
	return static_cast<double>(std::chrono::duration_cast<std::chrono::microseconds>(end - start_).count() / 10000);
}

//計測開始
void TimeBoard::StartCount()
{
	//タイム計測状態に移行
	state_ = TIME_STATE_MEASURE;
	//開始時間を現在の時間に
	start_ = std::chrono::system_clock::now();
}

//計測終了
void TimeBoard::EndCount()
{
	//タイム計測状態に移行
	state_ = TIME_STATE_END;
}

//時間を描画
void TimeBoard::DrawTime()
{
	int number;						//描画する数字
	D3DXVECTOR3 position;			//描画位置
	
	// 分
	// 十の位
	number = minute_ / 10;
	position = position_ + TIME_MINUTE_POS;
	Image::SetPosition(hPict_[number], position);
	Image::Draw(hPict_[number]);
	// 一の位
	number = minute_ % 10;
	position.x += numberSize_.x;
	Image::SetPosition(hPict_[number], position);
	Image::Draw(hPict_[number]);

	// :
	position.x += numberSize_.x;
	Image::SetPosition(hPict_[TIME_PICT_COLON], position);
	Image::Draw(hPict_[TIME_PICT_COLON]);

	// 秒
	// 十の位
	number = second_ / 10;
	position.x += numberSize_.x;
	Image::SetPosition(hPict_[number], position);
	Image::Draw(hPict_[number]);
	// 一の位
	number = second_ % 10;
	position.x += numberSize_.x;
	Image::SetPosition(hPict_[number], position);
	Image::Draw(hPict_[number]);

	// :
	position.x += numberSize_.x;
	Image::SetPosition(hPict_[TIME_PICT_COLON], position);
	Image::Draw(hPict_[TIME_PICT_COLON]);

	// 1/100秒
	//十の位
	number = microSecond_ / 10;
	position.x += numberSize_.x;
	Image::SetPosition(hPict_[number], position);
	Image::Draw(hPict_[number]);
	//一の位
	number = microSecond_ % 10;
	position.x += numberSize_.x;
	Image::SetPosition(hPict_[number], position);
	Image::Draw(hPict_[number]);
}

//周回数を設定
void TimeBoard::SetLap(int lap)
{
	//最大周回数を超える値なら最大周回数の値を設定
	if (lap > lapMax_)
	{
		lap_ = lapMax_;
	}
	else
	{
		lap_ = lap;
	}
}
