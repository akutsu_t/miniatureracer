#pragma once
#include "Engine/IGameObject.h"

class Text;
class Character;

//自機を管理するクラス
class Box : public IGameObject
{
public:
	//コンストラクタ
	Box(IGameObject* parent);

	//デストラクタ
	~Box();

	//初期化
	void Initialize() override;

	//更新
	void Update() override;

	//描画
	void Draw() override;

	//解放
	void Release() override;

	//入力
	void Input();

	//何かに当たった
	//引数：pTarget 当たった相手, length めり込んだ長さ
	void OnCollision(IGameObject *pTarget, float length) override;

	//めり込んだキャラクターを押し戻すベクトルを求める
	//D3DXVECTOR3& CalcPushBackVector(Character* pCharacter);
};