#pragma once

#include "Engine/IGameObject.h"

//画像
enum RANK_PICT {
	RANK_PICT_NUMBER_0,
	RANK_PICT_NUMBER_1,
	RANK_PICT_NUMBER_2,
	RANK_PICT_NUMBER_3,
	RANK_PICT_NUMBER_4,
	RANK_PICT_NUMBER_5,
	RANK_PICT_NUMBER_6,
	RANK_PICT_NUMBER_7,
	RANK_PICT_NUMBER_8,
	RANK_PICT_NUMBER_9,
	RANK_PICT_WINDOW,
	RANK_PICT_MAX
};

//RankBoardを管理するクラス
class RankBoard : public IGameObject
{
	int hPict_[RANK_PICT_MAX];				//画像番号
	std::string pictName_[RANK_PICT_MAX];	//画像名
	int rank_;								//順位
	D3DXVECTOR3 rankPos_;					//順位表示位置
	bool isDraw_;							//描画するかどうか

public:
	//コンストラクタ
	RankBoard(IGameObject* parent);

	//デストラクタ
	~RankBoard();

	//初期化
	void Initialize() override;

	//更新
	void Update() override;

	//描画
	void Draw() override;

	//解放
	void Release() override;

	//アクセス関数
	int GetRank() { return rank_; }
	void SetRank(int rank) { rank_ = rank; }
	void SetIsDraw(bool isDraw) { isDraw_ = isDraw; }
};