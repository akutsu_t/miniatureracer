#include "Wheel.h"
#include "Engine/Model.h"
#include "Character.h"

//コンストラクタ
Wheel::Wheel(IGameObject * parent) : IGameObject(parent, "Wheel")
{
}

//デストラクタ
Wheel::~Wheel()
{
}

//初期化
void Wheel::Initialize()
{
	//エフェクト生成
	LPD3DXBUFFER err = 0;		//エラー用
	if (FAILED(D3DXCreateEffectFromFile(Direct3D::pDevice_, "CommonShader.hlsl",
		NULL, NULL, D3DXSHADER_DEBUG, NULL, &pEffect_, &err)))
	{
		//失敗時	親ウィンドウ	表示内容			ウィンドウバー		ボタン
		MessageBox(NULL, (char*)err->GetBufferPointer(), "シェーダエラー", MB_OK);
		err->Release();
	}

	hModel_ = Model::Load("Data/wheel.fbx", pEffect_, true);
	assert(hModel_ >= 0);
}

//更新
void Wheel::Update()
{
	////速度を元に回転
	//float velocity = D3DXVec3Length(&((Character*)pParent_)->GetVelocity());
	//rotate_.x += 360 * velocity / (2 * PI * wheelRadius_);
	//rotate_.x = (rotate_.x - (int)rotate_.x) + ((int)rotate_.x % 360);
}

//描画
void Wheel::Draw()
{

	//行列を設定
	Model::SetMatrix(hModel_, worldMatrix_);

	//シェーダー
	PassShaderInfo();

	//pEffectを使って描画
	pEffect_->Begin(NULL, 0);

	//パス設定
	pEffect_->BeginPass(0);

	//描画
	Model::Draw(hModel_);

	//パス終了
	pEffect_->EndPass();
	pEffect_->End();
}

//解放
void Wheel::Release()
{
}

