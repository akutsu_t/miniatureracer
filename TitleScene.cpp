#include "TitleScene.h"
#include "Engine/Image.h"
#include "Engine/Input.h"
#include "Engine./Audio.h"

//コンストラクタ
TitleScene::TitleScene(IGameObject * parent)
	: IGameObject(parent, "TitleScene"), pict_(TITLE_PICT_TITLE)
{
	//初期化
	for (int i = 0; i < TITLE_PICT_MAX; i++)
	{
		hPict_[i] = -1;
	}
}

//初期化
void TitleScene::Initialize()
{
	//タイトル画像データロード
	hPict_[TITLE_PICT_TITLE] = Image::Load("Data/Image/Title.jpg");
	assert(hPict_[TITLE_PICT_TITLE] >= 0);
	//ロード画像データロード
	hPict_[TITLE_PICT_LOAD] = Image::Load("Data/Image/Loading.jpg");
	assert(hPict_[TITLE_PICT_LOAD] >= 0);

	//BGM再生
	Audio::Play("titleBgm");
}

//更新
void TitleScene::Update()
{
	//[Enter]キーが押された
	if (Input::IsKeyDown(DIK_RETURN))
	{
		//プレイシーンへ
		SceneManager::ChangeScene(SCENE_ID_PLAY);
		pict_ = TITLE_PICT_LOAD;
	}
}

//描画
void TitleScene::Draw()
{
	Image::SetMatrix(hPict_[pict_], worldMatrix_);
	Image::Draw(hPict_[pict_]);
}

//解放
void TitleScene::Release()
{
	//BGM停止
	Audio::Stop("titleBgm");
}