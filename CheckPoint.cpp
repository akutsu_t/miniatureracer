#include "CheckPoint.h"
#include "Character.h"

//静的メンバ変数の初期化
int CheckPoint::checkCount_ = 0;

//コンストラクタ
CheckPoint::CheckPoint(IGameObject * parent)
	:IGameObject(parent, "CheckPoint")
{
}

//デストラクタ
CheckPoint::~CheckPoint()
{
}

//初期化
void CheckPoint::Initialize()
{
	//自分の番号を記録
	checkNumber_ = checkCount_++;

	//箱型コライダーをつける
	SetBoxCollider(D3DXVECTOR3(0, 0, 0), D3DXVECTOR3(1.0f, 1.0f, 1.0f), worldMatrix_);
}

//更新
void CheckPoint::Update()
{
}

//描画
void CheckPoint::Draw()
{
}

//解放
void CheckPoint::Release()
{
	//解放の際にカウントを減らす
	checkCount_--;
}

//何かに当たった
void CheckPoint::OnCollision(IGameObject * pTarget, float length)
{
	//PlayerかNPCに当たった
	if (pTarget->GetName() == "Player" || pTarget->GetName() == "NPC")
	{
		//プレイヤーのチェックポイントを更新
		Character* pCharacter = (Character*)pTarget;
		int characterCheck = pCharacter->GetCheckPoint();
		pCharacter->CalcNextPoint();

		//このチェックポイントが最初でプレイヤーのチェックポイントが最後
		if (checkNumber_ == 0 && characterCheck == checkCount_ - 1)
		{
			//チェックポイントを更新し、ラップ数を1増やす
			pCharacter->SetCheckPoint(checkNumber_);
			pCharacter->IncreaseLap();
		}
		//このチェックポイントが次のチェックポイント
		else if (checkNumber_ == (characterCheck + 1))
		{
			//チェックポイントを更新
			pCharacter->SetCheckPoint(checkNumber_);
		}
	}
}

