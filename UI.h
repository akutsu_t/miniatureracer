#pragma once

#include "Engine/IGameObject.h"

//前方宣言
class TimeBoard;
class Speedometer;
class RankBoard;
class Player;
class Result;

//UIを管理するクラス
class UI : public IGameObject
{
	TimeBoard*		pTimeBoard_;				//タイム表示
	Speedometer*	pSpeedometer_;				//速度表示
	RankBoard*		pRankBoard_;				//順位表示
	Player*			pPlayer_;					//UIを所有しているプレイヤー
	Result*			pResult_;					//リザルト表示
	
public:
	//コンストラクタ
	UI(IGameObject* parent);

	//デストラクタ
	~UI();

	//初期化
	void Initialize() override;

	//更新
	void Update() override;

	//描画
	void Draw() override;

	//解放
	void Release() override;

	//アクセス関数
	void SetSpped(float speed);

	//タイム計測開始
	void StartCount();

	//タイム計測終了
	void EndCount();

	//リザルトを表示
	void ShowResult();

	
};