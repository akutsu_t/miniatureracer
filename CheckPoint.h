#pragma once
#include "Engine/IGameObject.h"

//地面を管理するクラス
class CheckPoint : public IGameObject
{
	static int checkCount_;		//チェックポイントの数
	int checkNumber_;			//チェックポイントの番号
public:
	//コンストラクタ
	CheckPoint(IGameObject* parent);

	//デストラクタ
	~CheckPoint();

	//初期化
	void Initialize() override;

	//更新
	void Update() override;

	//描画
	void Draw() override;

	//解放
	void Release() override;

	//何かに当たった
	//引数：pTarget 当たった相手, length めり込んだ長さ
	void OnCollision(IGameObject *pTarget, float length) override;
};