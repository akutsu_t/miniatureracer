#pragma once
#include "Engine/IGameObject.h"

//前方宣言
class CarPhysic;
class Wheel;
class CharacterAI;
class Stage;

//キャラクターの状態
enum CHARACTER_STATE {
	CHARACTER_STATE_START,			//スタート合図待ち
	CHARACTER_STATE_RACE,			//レース中
	CHARACTER_STATE_GOAL,			//ゴール後
	CHARACTER_STATE_MAX,
};

//タイヤ位置
enum WHEEL {
	WHEEL_FL,			//前方左
	WHEEL_FR,			//前方右
	WHEEL_RL,			//後方左
	WHEEL_RR,			//後方右
	WHEEL_MAX
};

//キャラクターを管理するクラス
class Character : public IGameObject
{
protected:
	int checkPoint_;		//現在の最高到達チェックポイント
	int lap_;				//現在の周回数
	int lapMax_;			//最大周回数
	int rank_;				//順位
	CHARACTER_STATE state_;	//レース中か

	CarPhysic* pPhysic_;
	Wheel* pWheels_[WHEEL_MAX];
	CharacterAI* pAI_;
	//定数
	static constexpr float TURN_HANDLE_SPEED = 15.0f;		//ハンドルを回す速度
public:
	//コンストラクタ
	Character(IGameObject* parent, std::string name);

	//デストラクタ
	~Character();

	//初期化
	void Initialize() override;

	//継承先の初期化
	virtual void InitializeAddData() {};

	//更新
	void Update() override;

	//描画
	void Draw() override;

	//解放
	void Release() override;

	//継承先の解放関数
	virtual void ReleaseAddData() {};

	//入力
	virtual void Move() {};

	//力を加算
	void AddForce(D3DXVECTOR3 force);

	//何かに当たった
	//引数：pTarget 当たった相手, length めり込んだ長さ
	void OnCollision(IGameObject *pTarget, float length) override;

	//周回数を1増やす
	void IncreaseLap();

	//キャラクターと衝突時の反射処理
	void ReflectVsCharacter(Character* pCharacter);

	//レース開始
	virtual void StartGame();

	//レース終了
	virtual void EndGame();

	//ステージ情報を登録
	//引数：pStage ステージオブジェクトのポインタ
	void SetStage(Stage* pStage);

	//次のチェックポイントまでのベクトルを計算する
	void CalcNextPoint();

	//
	void SetVelocity(D3DXVECTOR3 vel);

	//アクセス関数
	int GetCheckPoint() { return checkPoint_; }
	int GetLap() { return lap_; }
	int GetLapMax() { return lapMax_; }
	int GetRank() { return rank_; }
	D3DXVECTOR3 GetForce();
	D3DXVECTOR3& GetVelocity();
	float GetHandleSpeed() { return TURN_HANDLE_SPEED; }
	void SetCheckPoint(int checkPoint) { checkPoint_ = checkPoint; }
	void SetLap(int lap);
	void SetLapMax(int lapMax) { lapMax_ = lapMax; }
	void SetRank(int rank) { rank_ = rank; }

	void SetPosition(D3DXVECTOR3& position) override;
	void AddPosition(D3DXVECTOR3& addPosition) override;
};